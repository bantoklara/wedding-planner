This project is a Wedding planner full-stack web application. 
The application enables couples to manage various details of their wedding and search for services. 
The application also serves as an advertising platform for wedding service providers.

I used the following technologies to implement the full-stack application:

	Backend:
		- Java programming language
		- Spring Boot framework
		- MySql Database
		- WebSocket 
	Frontend:
		- JavaScript
		- ReactJS library
		- Material UI component library 
		
The application has 4 types of users:

	1. Unauthorized user: 
		- viewing the wedding services uploaded to the site
	2. Couples
		- maintaining the guest list and to-do list 
		- planning the seating order of guests around the table
		- searching for services 
		- connecting with service providers 
		- maintaining the feedback of the guests 
	3. Service providers 
		- uploading their services 
		- connecting with clients
	4. Guests 
		- sending their feedback regarding their presents at the wedding
		
