package org.wedding.backend.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import org.wedding.backend.model.Guest;
import org.wedding.backend.repository.GuestRepository;

import java.util.List;

@Component
public class AuthWithGeneratedCodeAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private GuestRepository repository;

    @Autowired
    private GuestInfoUserDetailsService userDetailsService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String code = authentication.getName();

        Guest guest = repository.findByLoginCode(code).orElse(null);
        if (guest != null)
            return new LoginCodeAuthenticationToken(code, null, List.of(new SimpleGrantedAuthority(guest.getRole().name())));

    return null;
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(LoginCodeAuthenticationToken.class);
    }
}
