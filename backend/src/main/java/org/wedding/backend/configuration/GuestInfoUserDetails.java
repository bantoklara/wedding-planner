package org.wedding.backend.configuration;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.wedding.backend.model.Guest;

import java.util.Collection;
import java.util.List;

public class GuestInfoUserDetails implements UserDetails {
    private String code;
    private List<GrantedAuthority> authorities;

    public GuestInfoUserDetails(Guest guest) {
        this.code = guest.getLoginCode();
        authorities = List.of(new SimpleGrantedAuthority(guest.getRole().name()));
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return code;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
