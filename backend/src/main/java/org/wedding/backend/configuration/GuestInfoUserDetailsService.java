package org.wedding.backend.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.wedding.backend.model.Guest;
import org.wedding.backend.repository.GuestRepository;

import java.util.Optional;

@Component
public class GuestInfoUserDetailsService implements UserDetailsService {
    @Autowired
    private GuestRepository repository;

    @Override
    public UserDetails loadUserByUsername(String code) throws UsernameNotFoundException {
        Optional<Guest> guest = repository.findByLoginCode(code);
        return guest.map(GuestInfoUserDetails::new).orElseThrow(() -> new UsernameNotFoundException("Guest not found"));
    }
}
