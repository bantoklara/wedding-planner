package org.wedding.backend.configuration;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class LoginCodeAuthenticationToken extends AbstractAuthenticationToken {
    private final Object principal;
    private final Object credentials;

    public LoginCodeAuthenticationToken(Object principal, Object credential) {
        super(null);
        this.principal = principal;
        this.credentials = credential;
        setAuthenticated(false);
    }

    public LoginCodeAuthenticationToken(Object principal, Object credential, Collection<? extends GrantedAuthority> authorities) {
        super(authorities);
        this.principal = principal;
        this.credentials = credential;
        setAuthenticated(true);
    }

    public static LoginCodeAuthenticationToken authenticated(Object principal, Object credentials,
                                                             Collection<? extends GrantedAuthority> authorities) {
        return new LoginCodeAuthenticationToken(principal, credentials, authorities);
    }

    @Override
    public Object getCredentials() {
        return credentials;
    }

    @Override
    public Object getPrincipal() {
        return principal;
    }
}
