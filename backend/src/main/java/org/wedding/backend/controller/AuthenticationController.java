package org.wedding.backend.controller;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wedding.backend.dto.incoming.AuthRequestDto;
import org.wedding.backend.dto.incoming.GuestAuthRequestDto;
import org.wedding.backend.dto.incoming.UserCreationDto;
import org.wedding.backend.dto.outgoing.AuthResponseDto;
import org.wedding.backend.dto.outgoing.UserReducedDto;
import org.wedding.backend.exception.UsernameExistsException;
import org.wedding.backend.mapper.UserMapper;
import org.wedding.backend.model.Notification;
import org.wedding.backend.model.User;
import org.wedding.backend.service.AuthenticationService;
import org.wedding.backend.service.GuestService;
import org.wedding.backend.service.NotificationService;
import org.wedding.backend.service.UserService;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
    @Autowired
    private AuthenticationService service;

    @Autowired
    private UserMapper mapper;

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private GuestService guestService;

    @PostMapping("/register")
    public ResponseEntity<User> register(@RequestBody @Valid UserCreationDto userCreationDto) throws UsernameExistsException {
        User user = mapper.userCreationDtoToUserModel(userCreationDto);
        User createdUser = service.register(user);
        return ResponseEntity.status(HttpStatus.CREATED).body(createdUser);
    }

    @PostMapping("/login")
    public ResponseEntity<UserReducedDto> login(@RequestBody AuthRequestDto authRequest, HttpServletResponse response) {
        User user  = userService.findByUsername(authRequest.getUsername());
        String token = service.login(user, authRequest);
        response.setHeader("Authorization", token);
        return ResponseEntity.ok(userMapper.userModelToUserReducedDto(user));
    }

    @PostMapping("/existsByUsername")
    public ResponseEntity<Boolean> existsByUsername(@RequestBody String username) {
        return ResponseEntity.ok(userService.existsByUsername(username));
    }

    @PostMapping("/login-as-guest")
    public AuthResponseDto loginAsGuest(@RequestBody GuestAuthRequestDto authRequestDto) {
        String token = guestService.login(authRequestDto.getLoginCode());
        return token != null
                ? AuthResponseDto.builder().token("CustomAuth" + token).build()
                : null;
    }
}
