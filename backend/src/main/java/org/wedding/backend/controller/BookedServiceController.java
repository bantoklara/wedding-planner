package org.wedding.backend.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.incoming.BookedServiceUpdatingDto;
import org.wedding.backend.dto.outgoing.BookedServiceDetailDto;
import org.wedding.backend.dto.outgoing.BookedServiceDto;
import org.wedding.backend.mapper.BookedServiceMapper;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.util.Status;
import org.wedding.backend.service.BookedServiceService;
import org.wedding.backend.service.NotificationService;

import java.awt.print.Book;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/booked-services")
public class BookedServiceController {
    @Autowired
    private BookedServiceService service;

    @Autowired
    private BookedServiceMapper mapper;

    @Autowired
    private NotificationService notificationService;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER')")
    public ResponseEntity<Map<String, Object>> findBookedServicesOfUser(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(value = "sortBy", required = false) String sortBy,
            @RequestParam(value = "order", required = false) String order,
            @RequestParam(value = "status", required = false) Status status,
            @RequestParam(value = "page", required = false) Integer page,
            @RequestParam(value = "perPage", required = false) Integer perPage) {
        Map<String, Object> response = service.findBookedServiceOfUser(userDetails.getUsername(), page, perPage, sortBy, order, status);
        response.put("bookedServices", mapper.bookedServiceModelsToBookedServiceDtos((List<BookedService>) response.get("bookedServices")));
        return ResponseEntity.ok(response);
    }

    @GetMapping("{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER')" +
            " and @securityService.hasUser(#id)")
    public ResponseEntity<BookedServiceDto> getById(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(
                mapper.bookedServiceModelToBookedServiceDto(service.findById(id))
        );
    }

    @GetMapping("/{id}/is-reserved")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<BookedServiceDto> isServiceReserved(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable("id") Long serviceId
    ) {
        BookedService bookedService = service.isReserved(userDetails.getUsername(), serviceId);

        return bookedService != null
                ? ResponseEntity.ok(mapper.bookedServiceModelToBookedServiceDto(bookedService))
                : ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<BookedServiceDto> create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody Long serviceId) {
        BookedService bookedService = service.create(userDetails.getUsername(), serviceId);
        notificationService.notifyServiceProviderAboutRequest(
                bookedService.getService(),
                bookedService.getWedding().getUser());

        return ResponseEntity.ok(
                mapper.bookedServiceModelToBookedServiceDto(
                        bookedService
                )
        );
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.hasUser(#id)")
    public ResponseEntity<BookedServiceDto> update(
            @PathVariable("id") Long id,
            @RequestBody @Valid BookedServiceUpdatingDto bookedServiceUpdatingDto) {
        BookedService bookedService = service.updateBookedService(id, mapper.bookedServiceUpdatingDtoToBookedServiceModel(bookedServiceUpdatingDto));

        notificationService.notifyCoupleABoutServiceResponse(
                bookedService.getWedding().getUser(),
                bookedService.getService(),
                true);

        return ResponseEntity.ok(mapper.bookedServiceModelToBookedServiceDto(bookedService)
        );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.hasUser(#id)")
    public ResponseEntity delete(
            @PathVariable("id") Long id
    ) {
        BookedService deletedBookedService = service.deleteBookedService(id);

        notificationService.notifyCoupleABoutServiceResponse(
                deletedBookedService.getWedding().getUser(),
                deletedBookedService.getService(),
                false);

        return ResponseEntity.ok().build();
    }
}
