package org.wedding.backend.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.configuration.UserInfoUserDetails;
import org.wedding.backend.dto.incoming.GuestCreationDto;
import org.wedding.backend.dto.incoming.GuestTableUpdateDto;
import org.wedding.backend.dto.outgoing.GuestDetailsDto;
import org.wedding.backend.dto.outgoing.GuestReducedDto;
import org.wedding.backend.helper.WriteExcel;
import org.wedding.backend.mapper.GuestMapper;
import org.wedding.backend.mapper.WeddingTableMapper;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.WeddingTable;
import org.wedding.backend.model.util.Role;
import org.wedding.backend.service.GuestService;
import org.wedding.backend.service.NotificationService;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/guests")
@Validated
@CrossOrigin("http://localhost:3000")
public class GuestController {

    @Autowired
    private GuestMapper mapper;

    @Autowired
    private GuestService service;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private WeddingTableMapper tableMapper;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<Map<String, Object>> getAllGuests(
            @AuthenticationPrincipal UserInfoUserDetails userDetails,
            @RequestParam(name = "name", required = false) String name,
            @RequestParam(name = "feedback", required = false) Integer feedback,
            @RequestParam(name = "hasNoTable", required = false) Boolean hasNoTable,
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "perPage", required = false) Integer perPage,
            @RequestParam(name = "sortBy", required = false) String sortBy,
            @RequestParam(name = "order", required = false) String order) {
        Map<String, Object> answer = service.filterGuests(
                userDetails.getUsername(),
                name, feedback, hasNoTable,
                page, perPage,
                sortBy, order
        );

        answer.put("guests", mapper.guestsToGuestDtos((List<Guest>) answer.get("guests")));
        answer.put("numberOfAllGuests", service.getNumberOfGuests(userDetails.getUsername()));
        answer.put("numberOfGuestWithFeedback1", service.getNumberOfGuestsByFeedback(userDetails.getUsername(), 1));
        answer.put("numberOfGuestWithFeedback2", service.getNumberOfGuestsByFeedback(userDetails.getUsername(), 2));
        answer.put("numberOfGuestWithFeedback3", service.getNumberOfGuestsByFeedback(userDetails.getUsername(), 3));
        return ResponseEntity.ok(answer);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')" +
            "and @securityService.guestBelongsToWedding(#id)")
    public GuestDetailsDto getGuestById(@PathVariable("id") Long id) {
        Guest guest = service.getGuestById(id);
        return (guest != null) ? mapper.guestToGuestDetailsDto(guest) : null;
    }

    @GetMapping("/my-invitation")
    @PreAuthorize("hasAnyAuthority('ROLE_WEDDING_GUEST')")
    public GuestDetailsDto findGuestByLoginCode(@AuthenticationPrincipal String code) {
        return mapper.guestToGuestDetailsDto(service.findGuestByLoginCode(code));
    }

    @GetMapping("/generate-excel")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<InputStreamResource> generateExcel(
            @RequestParam(name = "columns", required = false) List<String> columns,
            @RequestParam(name = "feedback", required = false) Integer feedback,
            @AuthenticationPrincipal UserDetails userDetails
    ) throws IOException {
        List<Guest> guests = (List<Guest>) service.filterGuests(
                userDetails.getUsername(),
                null, feedback,
                null,
                null,
                null, null, null).get("guests");
        InputStreamResource file = new InputStreamResource(WriteExcel.generateExcelFile(guests, columns));

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=vendeglista.xlsx")
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .body(file);
    }

    @GetMapping("/by-table")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<Map<Object, List<GuestReducedDto>>> getGuestsGroupedByTable(
            @AuthenticationPrincipal UserDetails userDetails) {
        Map<WeddingTable, List<Guest>> groups = service.findGuestsGroupedByWeddingTable(userDetails.getUsername());
        Map<Object, List<GuestReducedDto>> response = new HashMap<>();

        for (Map.Entry<WeddingTable, List<Guest>> group : groups.entrySet()) {
            response.put(tableMapper.weddingTableModelToWeddingTableDto(group.getKey()),
                    mapper.guestsToGuestDtos(group.getValue()));
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping
    @ResponseBody
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<List<GuestReducedDto>> createGuests(
            @AuthenticationPrincipal UserInfoUserDetails userDetails,
            @RequestBody List<@Valid GuestCreationDto> guests) {
        return ResponseEntity.ok(
                mapper.guestsToGuestDtos(
                        service.createGuests(userDetails.getUsername(), mapper.guestCreationDtosToGuestModels((guests))
                        )
                ));
    }

    @PutMapping("/wedding-tables")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')" +
            "and @securityService.guestsBelongToWedding(#guestTableUpdateDto)")
    @ResponseBody
    public List<GuestReducedDto> updateWeddingTables(
            @RequestBody List<GuestTableUpdateDto> guestTableUpdateDto
    ) {
        return mapper.guestsToGuestDtos(service.updateTableOfGuests(
                mapper.guestTableUpdateDtosToGuestModels(guestTableUpdateDto)
        ));
    }

    @PostMapping("/codes")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')")
    public ResponseEntity<List<GuestReducedDto>> generateGuestCodes(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(mapper.guestsToGuestDtos(service.generateLoginCodes(userDetails.getUsername())));
    }

    @PostMapping("/codes/{id}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.guestBelongsToWedding(#id)")
    public ResponseEntity<GuestDetailsDto> generateGuestCode(@PathVariable("id") Long id) {
        return ResponseEntity.ok(mapper.guestToGuestDetailsDto(service.generateLoginCode(id)));
    }

    @PatchMapping(value = "/{id}")
    @PreAuthorize("(hasAuthority('ROLE_COUPLE')" +
            "and @securityService.guestBelongsToWedding(#id))")
    @ResponseBody
    public GuestDetailsDto updateGuestPartially(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable("id") Long id,
            @RequestBody Map<String, Object> fields) {
        return mapper.guestToGuestDetailsDto(service.updateByCouple(id, fields));
    }

    @PatchMapping("/my-invitation")
    @PreAuthorize("hasAuthority('ROLE_WEDDING_GUEST')")
    public GuestDetailsDto updateInvitation(
            @AuthenticationPrincipal String code,
            @RequestBody Map<String, Object> fields) {
        Guest guest = service.updateByGuest(code, fields);
        notificationService.notifyCoupleAboutNewGuestFeedback(
                guest.getWedding().getUser(),
                guest.getId(),
                guest.getLastName() + " " + guest.getFirstName()
        );
        return mapper.guestToGuestDetailsDto(guest);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.guestBelongsToWedding(#id)")
    public void delete(
            @PathVariable("id") Long id) {
        service.delete(id);
    }
}
