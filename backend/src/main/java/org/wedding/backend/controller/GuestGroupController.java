package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wedding.backend.model.GuestGroup;
import org.wedding.backend.service.GuestGroupService;

import java.util.List;

@RestController
@RequestMapping("/api/guestGroups")
public class GuestGroupController {

    @Autowired
    private GuestGroupService service;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public List<GuestGroup> listGuestGroups() {
        return service.findAll();
    }
}
