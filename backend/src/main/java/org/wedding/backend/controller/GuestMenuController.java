package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.outgoing.GuestMenuReducedDto;
import org.wedding.backend.mapper.GuestMenuMapper;
import org.wedding.backend.service.GuestMenuService;

import java.util.List;

@RestController
@RequestMapping("/api/guestMenus")
public class GuestMenuController {
    @Autowired
    private GuestMenuService service;

    @Autowired
    private GuestMenuMapper mapper;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_WEDDING_GUEST')")
    public List<GuestMenuReducedDto> getAllGuestMenus() {
        return mapper.guestMenuModelsToGuestMenuReducedDtos(service.findAll());
    }

    @DeleteMapping("/{guestId}")
    @ResponseBody
    public Integer deleteNotUpdatedGuestMenus(@PathVariable("guestId") Long guestId, @RequestBody List<Long> idList) {
        return service.deleteNotUpdatedGuestMenus(guestId, idList);
    }
}
