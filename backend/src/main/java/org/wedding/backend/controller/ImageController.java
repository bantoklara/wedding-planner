package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.model.Image;
import org.wedding.backend.service.ImageService;

import java.io.IOException;

@RestController
@RequestMapping("/api/images")
public class ImageController {
    @Autowired
    private ImageService service;

    @PostMapping
    public Image upload(
            @RequestParam("description") String description,
            @RequestParam("file") MultipartFile file
    ) throws IOException {

        return null;
    }
}
