package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.wedding.backend.dto.outgoing.MenuDto;
import org.wedding.backend.mapper.MenuMapper;
import org.wedding.backend.service.MenuService;

import java.util.List;

@RestController
@RequestMapping("/api/menus")
public class MenuController {
    @Autowired
    private MenuService service;

    @Autowired
    private MenuMapper mapper;

    @GetMapping
    public List<MenuDto> getAllMenus() {
        return mapper.menuModelsToMenuDtos(service.findAll());
    }
}
