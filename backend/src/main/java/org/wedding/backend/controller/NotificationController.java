package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.outgoing.NotificationDto;
import org.wedding.backend.mapper.NotificationMapper;
import org.wedding.backend.service.NotificationService;
import org.wedding.backend.service.UserService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/notifications")
@CrossOrigin("http://localhost:3000")
public class NotificationController {

    @Autowired
    private NotificationService service;

    @Autowired
    private NotificationMapper mapper;

    @Autowired
    private UserService userService;

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping
    public ResponseEntity<List<NotificationDto>> getNotifications(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(value = "unread", required = false) Boolean unread) {
        return ResponseEntity.ok(
                mapper.notificationModelsToNotificationDtos(service.findAllAndSetAsRead(userDetails.getUsername(), unread)));
    }

    @GetMapping("/count")
    public ResponseEntity<Long> getNumberOfUnreadNotifications(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(service.getNumberOfUnreadNotifications(userDetails.getUsername()));
    }

    @MessageMapping("/mark-as-read")
    public void markNotificationsAsSeen(String username) {
        service.markUnreadNotificationsAsRead(username);
    }
}
