package org.wedding.backend.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.incoming.ScheduleCreationDto;
import org.wedding.backend.dto.outgoing.ScheduleDto;
import org.wedding.backend.exception.DateIntervalException;
import org.wedding.backend.mapper.ScheduleMapper;
import org.wedding.backend.model.Schedule;
import org.wedding.backend.service.ScheduleService;

import java.util.List;

@RestController
@RequestMapping("/api/schedules")
@CrossOrigin("http://localhost:3000")
public class ScheduleController {
    @Autowired
    private ScheduleService service;

    @Autowired
    private ScheduleMapper mapper;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')")
    public ResponseEntity<List<ScheduleDto>> getAllSchedulesOfUser(
            @AuthenticationPrincipal UserDetails userDetails
    ) {
        return ResponseEntity.ok(
                mapper.scheduleModelsToScheduleDtos(service.findByUsername(userDetails.getUsername()))
        );
    }

    @GetMapping("/{serviceId}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.serviceBelongsToProvider(#serviceId)")
    public ResponseEntity<List<ScheduleDto>> getAllSchedulesOfService(@PathVariable("serviceId") Long serviceId) {
        return ResponseEntity.ok(
                mapper.scheduleModelsToScheduleDtos(
                        service.findByServiceId(serviceId)
                )
        );
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')")
    public ResponseEntity<ScheduleDto> create(
            @RequestBody @Valid ScheduleCreationDto scheduleCreationDto
    ) throws DateIntervalException {
        return ResponseEntity.ok(
                mapper.scheduleModelToScheduleDto(
                        service.create(
                                scheduleCreationDto.getServiceId(),
                                scheduleCreationDto.getBookedServiceId(),
                                mapper.scheduleCreationDtoToScheduleModel(scheduleCreationDto))
                )
        );
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.scheduleBelongsToUser(#id)")
    public ResponseEntity<ScheduleDto> update(
            @PathVariable("id") Long id,
            @RequestBody @Valid ScheduleCreationDto scheduleCreationDto) throws DateIntervalException {
        return ResponseEntity.ok(mapper.scheduleModelToScheduleDto(
                service.update(
                        id,
                        scheduleCreationDto.getServiceId(),
                        scheduleCreationDto.getBookedServiceId(),
                        mapper.scheduleCreationDtoToScheduleModel(scheduleCreationDto))
        ));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.scheduleBelongsToUser(#id)")
    public ResponseEntity<Void> delete(@PathVariable("id") Long id) {
        service.delete(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
