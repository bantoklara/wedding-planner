package org.wedding.backend.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.dto.incoming.ServiceCreationDto;
import org.wedding.backend.dto.incoming.ServiceUpdateDto;
import org.wedding.backend.dto.outgoing.ServiceDetailsDto;
import org.wedding.backend.dto.outgoing.ServiceDto;
import org.wedding.backend.dto.outgoing.ServiceReducedDto;
import org.wedding.backend.exception.FileFormatException;
import org.wedding.backend.mapper.WeddingServiceMapper;
import org.wedding.backend.model.Service;
import org.wedding.backend.model.util.PriceCategory;
import org.wedding.backend.model.util.Role;
import org.wedding.backend.service.NotificationService;
import org.wedding.backend.service.ServiceService;
import org.wedding.backend.service.UserService;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/services")
@CrossOrigin("localhost:3000")
public class ServiceController {
    @Autowired
    private ServiceService service;

    @Autowired
    private UserService userService;

    @Autowired
    private WeddingServiceMapper mapper;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private ObjectMapper objectMapper;

    //    CREATE

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')")
    public ResponseEntity<ServiceDetailsDto> create(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam("service") String serviceDto,
            @RequestParam(value = "descriptions", required = false) String descriptions,
            @RequestParam("coverImage") MultipartFile coverImage,
            @RequestParam(value = "images", required = false) List<MultipartFile> images
    ) throws IOException, FileFormatException {
        ServiceCreationDto serviceCreationDto = objectMapper.readValue(serviceDto, ServiceCreationDto.class);
        List<String> descriptionList = descriptions != null ? objectMapper.readValue(descriptions, List.class) : null;
        Service createdService = service.create(
                userDetails.getUsername(),
                mapper.weddingServiceCreationDtoToWeddingServiceModel(serviceCreationDto),
                descriptionList,
                coverImage,
                images
        );

        notificationService.notifyAdminAboutNewServiceRequest(userService.findByUsername("admin"), createdService);

        return ResponseEntity.ok(mapper.weddingServiceModelToWeddingServiceDto(createdService));
    }

    //    READ

    @GetMapping
    public ResponseEntity<List<ServiceDto>> filter(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestParam(value = "myServices", required = false) Boolean myServices,
            @RequestParam(value = "enabled", required = false) Boolean enabled,
            @RequestParam(value = "categoryId", required = false) Long categoryId,
            @RequestParam(value = "county", required = false) String county,
            @RequestParam(value = "priceCategory", required = false) PriceCategory priceCategory,
            @RequestParam(value = "maximalSpaceCapacity", required = false) Integer maximalSpaceCapacity,
            @RequestParam(value = "start", required = false) LocalDateTime start,
            @RequestParam(value = "end", required = false) LocalDateTime end,
            @RequestParam(value = "page", required = false) Integer page
    ) {
        return ResponseEntity.ok(mapper.weddingServiceModelsToWeddingServiceDtos(service.filter(
                userDetails != null ? userDetails.getUsername() : null,
                myServices, enabled, categoryId, county, priceCategory, maximalSpaceCapacity,
                start, end,
                page
        )));
    }

    @GetMapping("/my-services")
    public ResponseEntity<List<ServiceReducedDto>> getAllServicesOfUser(@AuthenticationPrincipal UserDetails userDetails) {
        return ResponseEntity.ok(
                mapper.weddingServiceModelsToWeddingServiceReducedDtos(
                        service.findServicesOfUser(userDetails.getUsername())));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ServiceDetailsDto> getById(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable("id") Long id) throws IOException {
        return ResponseEntity.ok(mapper.weddingServiceModelToWeddingServiceDto(
                service.findByUsernameAndId(
                        userDetails != null ? userDetails.getUsername() : null,
                        id)));
    }

    //    UPDATE

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_SERVICE_PROVIDER')" +
            "and @securityService.serviceBelongsToProvider(#id)")
    public ResponseEntity<ServiceDetailsDto> update(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable("id") Long id,
            @RequestParam("service") String serviceDto,
            @RequestParam(value = "descriptions", required = false) String descriptions,
            @RequestParam(value = "coverImage", required = false) MultipartFile coverImage,
            @RequestParam(value = "images", required = false) List<MultipartFile> images
    ) throws IOException, FileFormatException {
        ServiceUpdateDto weddingServiceUpdateDto = objectMapper.readValue(serviceDto, ServiceUpdateDto.class);
        List<String> descriptionList = descriptions != null ? objectMapper.readValue(descriptions, List.class) : null;
        return ResponseEntity.ok(
                mapper.weddingServiceModelToWeddingServiceDto(
                        service.update(
                                userDetails.getUsername(),
                                id,
                                mapper.weddingServiceUpdateDtoToWeddingServiceModel(weddingServiceUpdateDto),
                                descriptionList,
                                coverImage,
                                images
                        )
                )
        );
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_ADMIN')")
    public ResponseEntity enableService(@PathVariable("id") Long id) {
        Service enabledService = service.enableService(id);

        notificationService.notifyServiceProviderAboutAdminFeedback(enabledService, true);
        return ResponseEntity.ok().build();
    }

    //    DELETE
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')" +
            "or (hasAuthority('ROLE_SERVICE_PROVIDER') and @securityService.serviceBelongsToProvider(#id))")
    public ResponseEntity delete(
            @AuthenticationPrincipal UserDetails userDetails,
            @PathVariable("id") Long id) {
        Service deletedService = service.delete(userDetails.getUsername(), id);

        if (userService.findByUsername(userDetails.getUsername()).getRole().equals(Role.ROLE_ADMIN)) {
            notificationService.notifyServiceProviderAboutAdminFeedback(deletedService, false);
        }
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
