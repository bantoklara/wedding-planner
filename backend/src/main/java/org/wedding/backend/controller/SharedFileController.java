package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.dto.outgoing.SharedFileDto;
import org.wedding.backend.mapper.SharedFileMapper;
import org.wedding.backend.model.SharedFile;
import org.wedding.backend.service.SharedFileService;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/booked-services/{bookedServiceId}/shared-files")
public class SharedFileController {

    @Autowired
    private SharedFileService service;

    @Autowired
    private SharedFileMapper mapper;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER')" +
            " and @securityService.hasUser(#bookedServiceId)")
    public ResponseEntity<Map<String, Object>> getSharedFiles(
            @PathVariable("bookedServiceId") Long bookedServiceId,
            @RequestParam(name = "page", required = false) Integer page,
            @RequestParam(name = "perPage", required = false) Integer perPage,
            @RequestParam(name = "sortBy", required = false) String sortBy,
            @RequestParam(name = "order", required = false) String order) {
        Map<String, Object> response = service.getSharedFiles(bookedServiceId, page, perPage, sortBy, order);
        response.put("sharedFiles", mapper.sharedFileModelsToSharedFileDtos((List<SharedFile>) response.get("sharedFiles")));

        return ResponseEntity.ok(response);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER')" +
            "and @securityService.hasUser(#bookedServiceId)")
    public ResponseEntity<SharedFileDto> create(
            @PathVariable("bookedServiceId") Long bookedServiceId,
            @RequestParam("file") MultipartFile file
    ) throws IOException {
        return ResponseEntity.ok(
                mapper.sharedFileModelToSharedFileDto(
                        service.create(bookedServiceId, file))
        );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER')" +
            "and @securityService.hasUser(#bookedServiceId)")
    public ResponseEntity delete(
            @PathVariable("bookedServiceId") Long bookedServiceId,
            @PathVariable("id") Long id
    ) {
        service.delete(id);
        return ResponseEntity.ok().build();
    }

}
