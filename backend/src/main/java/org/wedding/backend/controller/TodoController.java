package org.wedding.backend.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.configuration.UserInfoUserDetails;
import org.wedding.backend.dto.incoming.TodoCreationDto;
import org.wedding.backend.dto.incoming.TodoUpdatingDto;
import org.wedding.backend.dto.outgoing.TodoReducedDto;
import org.wedding.backend.mapper.TodoMapper;
import org.wedding.backend.service.TodoService;
import org.wedding.backend.service.UserService;
import org.wedding.backend.service.WeddingService;

import java.util.List;

@RestController
@RequestMapping("/api/todos")
public class TodoController {
    @Autowired
    private TodoService service;

    @Autowired
    private TodoMapper mapper;

    @Autowired
    private WeddingService weddingService;

    @Autowired
    private UserService userService;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_COUPLE')")
    public List<TodoReducedDto> getAllTodos(@AuthenticationPrincipal UserInfoUserDetails userDetails) {
        Long weddingId = getWeddingIdByUsername(userDetails.getUsername());
        return mapper.todoModelsToTodoReducedDtos(weddingService.findAllTodos(weddingId));
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_COUPLE')")
    public TodoReducedDto createWeddingTodo(
            @AuthenticationPrincipal UserInfoUserDetails userDetails,
            @Valid @RequestBody TodoCreationDto todoCreationDto) {
        Long weddingId = getWeddingIdByUsername(userDetails.getUsername());
        return mapper.todoModelToTodoReducedDto(
                weddingService.createWeddingTodo(
                        weddingId,
                        mapper.todoCreationDtoToTodoModel(todoCreationDto)));
    }

    @PutMapping("/{todoId}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.todoBelongsToUser(#todoId)")
    public TodoReducedDto updateTodo(
            @PathVariable("todoId") Long todoId,
            @RequestBody TodoUpdatingDto todoUpdatingDto
    ) {
        return mapper.todoModelToTodoReducedDto(
                service.update(todoId, mapper.todoUpdatingDtoToTodoModel(todoUpdatingDto)));
    }

    @PatchMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.todoBelongsToUser(#id)")
    public Integer updateTodoState(
            @PathVariable("id") Long id,
            @RequestBody Boolean done) {
        return service.updateTodoState(id, done);

    }

    @DeleteMapping("/{todoId}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.todoBelongsToUser(#todoId)")
    public ResponseEntity deleteTodoFromWedding(
            @PathVariable("todoId") Long todoId
    ) {
        service.delete(todoId);
        return ResponseEntity.ok().build();
    }

    private Long getWeddingIdByUsername(String username) {
        Long userId = userService.findUserIdByUsername(username);
        return weddingService.findWeddingIdByUserId(userId);
    }
}
