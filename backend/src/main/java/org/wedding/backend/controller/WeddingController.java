package org.wedding.backend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.outgoing.WeddingDto;
import org.wedding.backend.dto.outgoing.WeddingStatisticsDto;
import org.wedding.backend.mapper.WeddingMapper;
import org.wedding.backend.mapper.WeddingTableMapper;
import org.wedding.backend.model.Wedding;
import org.wedding.backend.service.UserService;
import org.wedding.backend.service.WeddingService;

@RestController
@RequestMapping("api/wedding")
public class WeddingController {
    @Autowired
    private WeddingService service;

    @Autowired
    private WeddingMapper mapper;

    @Autowired
    private WeddingTableMapper weddingTableMapper;

    @Autowired
    private UserService userService;

    @GetMapping("/{weddingId}")
    @PreAuthorize("hasAnyAuthority('ROLE_COUPLE')")
    public ResponseEntity<WeddingStatisticsDto> getWeddingById(@PathVariable("weddingId") Long weddingId) {
        return ResponseEntity.ok(service.getStatisticsAboutWedding(weddingId));
    }
}
