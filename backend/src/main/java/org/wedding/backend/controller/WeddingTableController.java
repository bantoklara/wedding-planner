package org.wedding.backend.controller;

import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.wedding.backend.dto.incoming.WeddingTableCreationDto;
import org.wedding.backend.dto.outgoing.WeddingTableDto;
import org.wedding.backend.mapper.WeddingTableMapper;
import org.wedding.backend.service.UserService;
import org.wedding.backend.service.WeddingService;
import org.wedding.backend.service.WeddingTableService;

import java.util.List;

@RestController
@RequestMapping("/api/wedding-tables")
public class WeddingTableController {
    @Autowired
    private WeddingTableService service;

    @Autowired
    private WeddingTableMapper mapper;

    @Autowired
    private WeddingService weddingService;

    @Autowired
    private UserService userService;

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_COUPLE')")
    public ResponseEntity<List<WeddingTableDto>> findByWeddingId(@AuthenticationPrincipal UserDetails userDetails) {
        Long weddingId = getWeddingIdByUsername(userDetails.getUsername());
        return ResponseEntity.ok(
                mapper.weddingTableModelsToWeddingTableDtos(service.findByWeddingId(weddingId))
        );
    }

    @PostMapping
    @PreAuthorize("hasAuthority('ROLE_COUPLE')")
    public ResponseEntity<WeddingTableDto> createWeddingTable(
            @AuthenticationPrincipal UserDetails userDetails,
            @RequestBody @Valid WeddingTableCreationDto weddingTableCreationDto
    ) {
        Long id = getWeddingIdByUsername(userDetails.getUsername());
        return ResponseEntity.ok().body(mapper.weddingTableModelToWeddingTableDto(
                weddingService.createWeddingTable(
                        id,
                        mapper.weddingTableCreationDtoToWeddingTableModel(weddingTableCreationDto)
                )));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.tableBelongsToWedding(#id)")
    public ResponseEntity<WeddingTableDto> update(
            @PathVariable("id") Long id,
            @RequestBody @Valid WeddingTableCreationDto weddingTableCreationDto) {
        return ResponseEntity.ok(
                mapper.weddingTableModelToWeddingTableDto(
                        service.update(
                                id,
                                mapper.weddingTableCreationDtoToWeddingTableModel(weddingTableCreationDto))
                )
        );
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_COUPLE')" +
            "and @securityService.tableBelongsToWedding(#id)")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        return service.delete(id) ? ResponseEntity.ok().build() : ResponseEntity.notFound().build();
    }

    private Long getWeddingIdByUsername(String username) {
        Long userId = userService.findUserIdByUsername(username);
        return weddingService.findWeddingIdByUserId(userId);
    }
}
