package org.wedding.backend.controller.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;

public class JsonObjectInFormData {

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
}
