package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class BookedServiceCreationDto {
    @NotNull
    private Long weddingId;

    @NotNull
    private Long serviceId;
}
