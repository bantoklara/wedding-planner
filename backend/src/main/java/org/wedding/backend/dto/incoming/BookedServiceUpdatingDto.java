package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.wedding.backend.model.util.Status;

@Data
public class BookedServiceUpdatingDto {
    @NotNull
    private Status status;

    @NotNull
    @Min(0)
    private Integer price;
}
