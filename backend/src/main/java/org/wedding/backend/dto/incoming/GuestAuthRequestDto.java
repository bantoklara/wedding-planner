package org.wedding.backend.dto.incoming;

import lombok.Data;

@Data
public class GuestAuthRequestDto {
    String loginCode;
}
