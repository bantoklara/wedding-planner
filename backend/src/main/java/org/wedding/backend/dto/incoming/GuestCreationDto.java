package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.*;
import lombok.Data;
import org.wedding.backend.model.GuestGroup;

import java.time.LocalDateTime;

@Data
public class GuestCreationDto {
    @NotNull(message = "Firstname must not be null")
    @Size(max = 30, message = "First name must be maximum 30 character long.")
    @Pattern(regexp = "^[\\p{L} -]+$", message = "First name must contain only letters, spaces and \'-\' characters")
    private String firstName;

    @NotNull(message = "Firstname must not be null")
    @Size(max = 30, message = "Last name must be maximum 30 character long.")
    @Pattern(regexp = "^[\\p{L} -]+$", message = "Last name must contain only letters, spaces and \'-\' characters")
    private String lastName;

    @Min(value = 1, message = "Value is out of range")
    @Max(value = 4, message = "Values is out of range")
    private String invitedWithGuest;

    private GuestGroup guestGroup;

    private LocalDateTime lastFeedbackDate;
}
