package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.Data;
import org.wedding.backend.model.compositeKey.GuestMenuPK;

@Data
public class GuestMenuCreationDto {
    @NotNull
    private GuestMenuPK id;

    @NotNull
    @PositiveOrZero
    private Integer amount;
}
