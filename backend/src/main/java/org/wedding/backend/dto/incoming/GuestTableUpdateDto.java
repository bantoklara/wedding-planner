package org.wedding.backend.dto.incoming;

import lombok.Data;
import org.wedding.backend.model.WeddingTable;

@Data
public class GuestTableUpdateDto {
    private Long id;
    private WeddingTable weddingTable;
}
