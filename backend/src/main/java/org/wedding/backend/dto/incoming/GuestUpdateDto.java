package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.*;
import lombok.Data;
import org.wedding.backend.model.GuestGroup;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class GuestUpdateDto {
    @NotNull
    @Size(min = 3, max = 30)
    private String firstName;

    @NotNull
    @Size(min = 3, max = 30)
    private String lastName;

    @Min(1)
    @Max(4)
    @NotNull
    private String invitedWithGuest;

    @NotNull
    @PositiveOrZero
    private Integer numberOfGuests;

    @NotNull
    @Min(1)
    @Max(3)
    private Integer feedback;

    @Size(max = 200)
    private String additionalMessage;

    private GuestGroup guestGroup;

    private List<GuestMenuCreationDto> guestMenus;

    @FutureOrPresent
    private LocalDateTime lastFeedbackDate;
}
