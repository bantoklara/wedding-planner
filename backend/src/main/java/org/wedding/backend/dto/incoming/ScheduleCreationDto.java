package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScheduleCreationDto {
    @NotNull
    LocalDateTime start;

    @NotNull
    LocalDateTime end;

    @NotNull
    String title;

    @NotNull
    Long serviceId;

    Long bookedServiceId;
}
