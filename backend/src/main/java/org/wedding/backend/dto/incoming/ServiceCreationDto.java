package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.wedding.backend.model.Category;
import org.wedding.backend.model.util.PriceCategory;

@Data
public class ServiceCreationDto {
    @NotNull
    @Size(min = 3, max = 20)
    private String name;

    @NotNull
    private String description;

    private PriceCategory priceCategory;
    private String counties;
    private Integer maximalSpaceCapacity;

    @NotNull
    private Category category;
}
