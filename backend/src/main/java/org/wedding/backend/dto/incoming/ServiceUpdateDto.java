package org.wedding.backend.dto.incoming;

import lombok.Data;
import org.wedding.backend.model.Image;

import java.util.List;

@Data
public class ServiceUpdateDto extends ServiceCreationDto {
    List<Image> images;
}
