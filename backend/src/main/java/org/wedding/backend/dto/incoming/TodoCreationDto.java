package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.wedding.backend.model.Category;

@Data
public class TodoCreationDto {
    @NotNull(message = "Description must not be null.")
    @Size(max = 255, message = "Description must be maximum 255 character long.")
    private String description;

    @NotNull(message = "Deadline must not be null.")
    private Integer deadline;

    @NotNull(message = "Category must not be null.")
    private Category category;
}
