package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TodoUpdatingDto extends TodoCreationDto {
    @NotNull
    private Boolean done;
}
