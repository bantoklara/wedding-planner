package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.wedding.backend.model.util.Role;

@Data
public class UserCreationDto {
    @NotNull
    @Size(max = 30, message = "Field can have maximum 30 characters")
    private String firstName;

    @NotNull
    @Size(max = 30, message = "Field can have maximum 30 characters")
    @Pattern(regexp = "^[\\p{L} -]*$", message = "Field can contain only letters, space and - character")
    private String lastName;

    @NotNull
    @Size(max = 15, message = "Field can have maximum 15 characters")
    @Pattern(regexp = "^[\\p{L}0-9_]*$", message = "Field can contain only letters, numbers and underscore")
    private String username;

    @NotNull
    @Pattern(regexp = "^(?=.{8,}$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$", message = "Password must contain uppercase and lowercase letters and numbers")
    private String password;

    @NotNull
    @Size(max = 100, message = "Field can have maximum 30 characters")
    @Pattern(regexp = "^.+@.+\\..+$", message = "Field does not contain a valid email.")
    private String email;

    @NotNull
    @Pattern(regexp = "^[0-9]{10}$", message = "Field does not contain a valid phone number")
    private String phoneNumber;

    @NotNull
    private Role role;

    private WeddingCreationDto wedding;
}
