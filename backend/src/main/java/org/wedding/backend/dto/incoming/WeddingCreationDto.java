package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Data;

import java.time.LocalDate;

@Data
public class WeddingCreationDto {
    @NotNull
    String nameOfBride;

    @NotNull
    String nameOfFiance;

    LocalDate dateOfFeedbackDeadline;
    LocalDate dateOfWedding;
}
