package org.wedding.backend.dto.incoming;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class WeddingTableCreationDto {
    @NotNull(message = "Name must not be null.")
    private String name;

    @NotNull(message = "Number of seats must not be null.")
    @Min(2)
    @Max(16)
    private Integer numberOfSeats;
}
