package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.util.List;

@Data
public class BookedServiceDetailDto extends BookedServiceDto {
    Integer price;
    List<SharedFileDto> sharedFiles;
}

