package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.util.Status;

@Data
public class BookedServiceDto extends BookedServiceReducedDto {
    private Status status;
    private Integer price;
    private ServiceDto service;
}
