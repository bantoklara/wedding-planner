package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.util.Status;

@Data
public class BookedServiceReducedDto {
    private Long id;
    private WeddingReducedDto wedding;
}
