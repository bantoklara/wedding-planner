package org.wedding.backend.dto.outgoing;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ExcelFileDto {
    byte[] excelInBytes;
}
