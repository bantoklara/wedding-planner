package org.wedding.backend.dto.outgoing;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.wedding.backend.model.GuestGroup;

import java.time.LocalDateTime;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class GuestDetailsDto extends GuestReducedDto {
    private String additionalMessage;
    private LocalDateTime lastFeedbackDate;
    private GuestGroup guestGroup;
    private List<GuestMenuReducedDto> guestMenus;
}
