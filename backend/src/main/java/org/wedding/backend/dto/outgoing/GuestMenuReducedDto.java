package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.compositeKey.GuestMenuPK;

@Data
public class GuestMenuReducedDto {
    private GuestMenuPK id;
    private Integer amount;
}
