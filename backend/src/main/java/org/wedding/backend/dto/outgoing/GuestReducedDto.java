package org.wedding.backend.dto.outgoing;

import lombok.Data;

@Data
public class GuestReducedDto {
    private Long id;
    private String firstName;
    private String lastName;
    private Integer numberOfGuests;
    private Integer feedback;
    private Integer invitedWithGuest;
}
