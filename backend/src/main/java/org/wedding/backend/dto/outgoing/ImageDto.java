package org.wedding.backend.dto.outgoing;

import lombok.Data;

@Data
public class ImageDto {
    Long id;
    String url;
    String description;
}
