package org.wedding.backend.dto.outgoing;

import lombok.Data;

@Data
public class MenuDto {
    private Long id;
    private String name;
}
