package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class NotificationDto {
    private Long id;
    private String message;
    private String link;
    private LocalDateTime date;
}
