package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ScheduleDto {
    Long id;
    LocalDateTime start;
    LocalDateTime end;
    String title;
    ServiceReducedDto service;
    BookedServiceReducedDto bookedService;
}
