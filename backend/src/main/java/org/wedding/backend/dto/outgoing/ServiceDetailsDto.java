package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.Category;
import org.wedding.backend.model.util.PriceCategory;

import java.util.List;

@Data
public class ServiceDetailsDto {
    Long id;
    String name;
    String description;
    PriceCategory priceCategory;
    String counties;
    Integer maximalSpaceCapacity;
    Boolean enabled;
    ImageDto cover;
    Category category;
    private List<ImageDto> images;
    private UserReducedDto user;
}
