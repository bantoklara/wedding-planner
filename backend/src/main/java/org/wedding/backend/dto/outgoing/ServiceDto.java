package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.Category;
import org.wedding.backend.model.util.PriceCategory;

@Data
public class ServiceDto {
    Long id;
    String name;
    String description;
    String counties;
    PriceCategory priceCategory;
    Integer maximalSpaceCapacity;
    Boolean enabled;
    ImageDto cover;
    Category category;
    UserReducedDto user;
}
