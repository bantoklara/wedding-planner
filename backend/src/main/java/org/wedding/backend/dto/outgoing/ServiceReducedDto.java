package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.util.List;

@Data
public class ServiceReducedDto {
    Long id;
    String name;
    List<BookedServiceReducedDto> bookedServices;
}
