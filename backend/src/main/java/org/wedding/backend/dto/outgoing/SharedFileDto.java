package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class SharedFileDto {
    Long id;
    String name;
    String type;
    String url;
    LocalDateTime uploadDate;
}
