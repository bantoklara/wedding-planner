package org.wedding.backend.dto.outgoing;

import lombok.Data;
import org.wedding.backend.model.Category;

@Data
public class TodoReducedDto {
    private Long id;
    private Integer deadline;
    private String description;
    private Category category;
    private Boolean done;
}
