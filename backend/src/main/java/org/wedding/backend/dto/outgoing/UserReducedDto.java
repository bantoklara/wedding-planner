package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class UserReducedDto {
    Long id;
    String lastName;
    String firstName;
    String username;
    String email;
    String phoneNumber;
}
