package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class WeddingDto extends WeddingReducedDto {
    private Long id;
    private String nameOfBride;
    private String nameOfFiance;
    private UserReducedDto user;
    private LocalDate dateOfWedding;
}
