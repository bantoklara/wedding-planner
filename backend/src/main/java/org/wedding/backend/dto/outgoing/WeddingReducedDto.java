package org.wedding.backend.dto.outgoing;

import lombok.Data;

import java.time.LocalDate;

@Data
public class WeddingReducedDto {
    private Long id;
    private String nameOfBride;
    private String nameOfFiance;
    private UserReducedDto user;
    private LocalDate dateOfWedding;
}
