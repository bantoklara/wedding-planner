package org.wedding.backend.dto.outgoing;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;

@Data
@Builder
public class WeddingStatisticsDto {
    String nameOfBride;
    String nameOfFiance;
    LocalDate dateOfWedding;

    Integer numberOfInvitations;
    Integer numberOfInvitationsWithPositiveFeedback;
    Integer numberOfInvitationsWithNegativeFeedback;
    Integer numberOfInvitationsWithPendingFeedback;

    Integer numberOfInviteesWithPositiveFeedback;

    Integer numberOfInviteesWithSeat;
    Integer numberOfInviteesWithoutSeat;

    Integer numberOfCompletedTodos;
    Integer numberOfUncompletedTodos;

    Integer numberOfAcceptedBookedServices;
    Integer numberOfPendingBookedServices;
}
