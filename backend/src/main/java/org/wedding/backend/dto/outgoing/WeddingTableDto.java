package org.wedding.backend.dto.outgoing;

import lombok.Data;

@Data
public class WeddingTableDto {
    Long id;
    String name;
    Integer numberOfSeats;
}
