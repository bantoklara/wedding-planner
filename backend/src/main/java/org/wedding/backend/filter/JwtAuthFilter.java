package org.wedding.backend.filter;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.wedding.backend.configuration.GuestInfoUserDetailsService;
import org.wedding.backend.configuration.LoginCodeAuthenticationToken;
import org.wedding.backend.configuration.UserInfoUserDetailsService;
import org.wedding.backend.service.JwtService;

import java.io.IOException;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

    private static String DAOAUTH = "Bearer ";
    private static String CUSTOMAUTH = "Bearer CustomAuth";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserInfoUserDetailsService userDetailsService;

    @Autowired
    private GuestInfoUserDetailsService guestInfoUserDetailsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException, ExpiredJwtException {
        String authHeader = request.getHeader("Authorization");
        String token = null;

        try {
            if (authHeader != null) {
                if (authHeader.startsWith(CUSTOMAUTH)) {
                    token = authHeader.substring(CUSTOMAUTH.length());
                    String code = jwtService.extractUsername(token);
                    setSecurityContextHolderWithLoginCodeAuthToken(code, token, request);
                } else {
                    token = authHeader.substring(7);
                    String username = jwtService.extractUsername(token);
                    setSecurityContextHolderWithUsernameAndPasswordAuthToken(username, token, request);
                }
            }
            filterChain.doFilter(request, response);
        } catch (ExpiredJwtException | MalformedJwtException ex) {
            response.setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getWriter().write(ex.getMessage());
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        }
    }

    private void setSecurityContextHolderWithUsernameAndPasswordAuthToken(String username, String token, HttpServletRequest request) {
        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            if (jwtService.validateToken(token, userDetails)) {
                UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
    }

    private void setSecurityContextHolderWithLoginCodeAuthToken(String code, String token, HttpServletRequest request) {
        if (code != null && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = guestInfoUserDetailsService.loadUserByUsername(code);
            if (jwtService.validateToken(token, userDetails)) {
                LoginCodeAuthenticationToken authToken = new LoginCodeAuthenticationToken(code, null, userDetails.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
    }

}
