package org.wedding.backend.helper;

// Reads a template todo-list from .xlsx file when a wedding is created
// All wedding has a template todo-list

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.ResourceUtils;
import org.wedding.backend.model.Category;
import org.wedding.backend.model.Todo;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReadExcel {
    private static String[] HEADERS = {"deadline", "description", "category_id"};
    private static String SHEET = "Todos";

    public static List<Todo> excelToTodos() throws FileNotFoundException {
        File file = ResourceUtils.getFile("classpath:static/base-todos.xlsx");
        InputStream is = new FileInputStream(file);
        try {
            Workbook workbook = new XSSFWorkbook(is);

            Sheet sheet = workbook.getSheet(SHEET);
            Iterator<Row> rows = sheet.iterator();

            List<Todo> todos = new ArrayList<Todo>();

            int rowNumber = 0;
            while (rows.hasNext()) {
                Row currentRow = rows.next();

                if (rowNumber == 0) {
                    rowNumber++;
                    continue;
                }

                Iterator<Cell> cellsInRow = currentRow.iterator();
                Todo todo = new Todo();
                todo.setDone(false);
                int cellIndex = 0;
                while(cellsInRow.hasNext()) {
                    Cell currentCell = cellsInRow.next();

                    switch (cellIndex) {
                        case 0:
                            todo.setDeadline((int) currentCell.getNumericCellValue());
                            break;
                        case 1:
                            todo.setDescription(currentCell.getStringCellValue());
                            break;
                        case 2:
                            Category category = new Category();
                            category.setId((long) currentCell.getNumericCellValue());
                            todo.setCategory(category);
                            break;
                        default:
                            break;
                    }
                    cellIndex++;
                }
                todos.add(todo);
            }
            workbook.close();
            return todos;

        } catch (IOException e) {
            throw new RuntimeException("Fail to parse Excel file: " + e.getMessage());
        }
    }

}
