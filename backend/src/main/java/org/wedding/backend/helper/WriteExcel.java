package org.wedding.backend.helper;

import jakarta.servlet.ServletOutputStream;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.util.ReflectionUtils;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.GuestMenu;
import org.wedding.backend.model.Menu;
import org.wedding.backend.model.WeddingTable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WriteExcel {

    private static void writeHeader(
            XSSFWorkbook workbook,
            XSSFSheet sheet,
            List<String> columns,
            Map<String, String> headerInHungarian
    ) {
        Row row = sheet.createRow(0);
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        style.setFont(font);

        for (int i = 0; i < columns.size(); i++) {
            Object value = new Object();
            String column = columns.get(i);
            Field field = ReflectionUtils.findField(Guest.class, column);
            createCell(sheet, row, i, headerInHungarian.get(column), style);
        }
    }

    private static void write(List<Guest> guests, XSSFWorkbook workbook, XSSFSheet sheet, List<String> columns) {
        int rowCount = 1;
        CellStyle style = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setFontHeight(14);
        style.setFont(font);

        for (Guest guest : guests) {
            Row row = sheet.createRow(rowCount++);
            for (int i = 0; i < columns.size(); i++) {
                String column = columns.get(i);
                Object value = new Object();
                switch (column) {
                    case "menus" -> {
                        String allMenus = "";
                        for (GuestMenu menu : guest.getGuestMenus()) {
                            allMenus += menu.getMenu().getName() + ": " + menu.getAmount() + "\n";
                        }
                        value = allMenus;
                    }
                    case "weddingTable" -> {
                        WeddingTable table = guest.getWeddingTable();
                        value = table != null ? table.getName() : null;
                    }
                    default -> {
                        Field field = ReflectionUtils.findField(Guest.class, column);
                        if (field != null) {
                            field.setAccessible(true);
                            value = ReflectionUtils.getField(field, guest);
                        } else value = null;
                    }
                }
                createCell(sheet, row, i, value != null ? value : "-", style);
            }
        }
    }

    private static void createCell(XSSFSheet sheet, Row row, Integer columnCount, Object value, CellStyle style) {
        sheet.autoSizeColumn(columnCount);
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Long) {
            cell.setCellValue((Long) value);
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        } else {
            cell.setCellValue((Boolean) value);
        }
        cell.setCellStyle(style);
    }

    public static ByteArrayInputStream generateExcelFile(
            List<Guest> guests,
            List<String> columns
    ) throws IOException {
        Map<String, String> headersInHungarian = new HashMap<>();
        headersInHungarian.put("lastName", "Vezetéknév");
        headersInHungarian.put("firstName", "Keresztnév");
        headersInHungarian.put("feedback", "Visszajelzés");
        headersInHungarian.put("numberOfGuests", "Személyek száma");
        headersInHungarian.put("additionalMessage", "További üzenet");
        headersInHungarian.put("loginCode", "Bejelentkező kód");
        headersInHungarian.put("menus", "Menük");
        headersInHungarian.put("weddingTable", "Asztal");

        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Vendégek");
        writeHeader(workbook, sheet, columns, headersInHungarian);
        write(guests, workbook, sheet, columns);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);
        return new ByteArrayInputStream(out.toByteArray());
    }
}
