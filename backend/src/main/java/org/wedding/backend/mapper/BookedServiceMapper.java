package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.BookedServiceCreationDto;
import org.wedding.backend.dto.incoming.BookedServiceUpdatingDto;
import org.wedding.backend.dto.outgoing.BookedServiceDetailDto;
import org.wedding.backend.dto.outgoing.BookedServiceDto;
import org.wedding.backend.dto.outgoing.BookedServiceReducedDto;
import org.wedding.backend.model.BookedService;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookedServiceMapper {
    BookedService bookedServiceCreationDtoToBookedServiceModel(BookedServiceCreationDto bookedServiceCreationDto);

    BookedService bookedServiceUpdatingDtoToBookedServiceModel(BookedServiceUpdatingDto bookedServiceUpdatingDto);

    BookedServiceReducedDto bookedServiceModelToBookedServiceReducedDto(BookedService bookedService);

    BookedServiceDto bookedServiceModelToBookedServiceDto(BookedService bookedService);

    BookedServiceDetailDto bookedServiceModelToBookedServiceDetailDto(BookedService bookedService);

    @IterableMapping(elementTargetType = BookedServiceDto.class)
    List<BookedServiceDto> bookedServiceModelsToBookedServiceDtos(List<BookedService> bookedServices);

    @IterableMapping(elementTargetType = BookedServiceReducedDto.class)
    List<BookedServiceReducedDto> bookedServiceModelsToBookedServiceReducedDtos(List<BookedService> bookedService);
}
