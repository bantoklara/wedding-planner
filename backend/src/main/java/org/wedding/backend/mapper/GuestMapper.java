package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.GuestCreationDto;
import org.wedding.backend.dto.incoming.GuestTableUpdateDto;
import org.wedding.backend.dto.incoming.GuestUpdateDto;
import org.wedding.backend.dto.outgoing.GuestDetailsDto;
import org.wedding.backend.dto.outgoing.GuestReducedDto;
import org.wedding.backend.model.Guest;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GuestMapper {
    GuestReducedDto guestToGuestReducedDto(Guest guest);

    GuestDetailsDto guestToGuestDetailsDto(Guest guest);

    @IterableMapping(elementTargetType = GuestReducedDto.class)
    List<GuestReducedDto> guestsToGuestDtos(List<Guest> weddingGuests);

    Guest guestTableUpdateDtoToGuestModel(GuestTableUpdateDto guestTableUpdateDto);

    @IterableMapping(elementTargetType = Guest.class)
    List<Guest> guestTableUpdateDtosToGuestModels(List<GuestTableUpdateDto> guestTableUpdateDtos);

    Guest guestCreationDtoToGuest(GuestCreationDto weddingGuestCreationDto);

    @IterableMapping(elementTargetType = Guest.class)
    List<Guest> guestCreationDtosToGuestModels(List<GuestCreationDto> weddingGuestCreationDto);

    Guest guestDetailedUpdateDtoToGuest(GuestUpdateDto guestUpdateDto);
}
