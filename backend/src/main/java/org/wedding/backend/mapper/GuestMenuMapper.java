package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.GuestMenuCreationDto;
import org.wedding.backend.dto.outgoing.GuestMenuReducedDto;
import org.wedding.backend.model.GuestMenu;

import java.util.List;

@Mapper(componentModel = "spring")
public interface GuestMenuMapper {
    GuestMenu guestMenuReducedDtoToGuestMenuModel(GuestMenuReducedDto guestMenuReducedDto);

    @IterableMapping(elementTargetType = GuestMenu.class)
    List<GuestMenu> guestMenuReducedDtosToGuestMenuModels(List<GuestMenuReducedDto> guestMenuReducedDto);

    GuestMenuReducedDto guestMenuModelToGuestMenuReducedDto(GuestMenu guestMenu);

    @IterableMapping(elementTargetType = GuestMenuReducedDto.class)
    List<GuestMenuReducedDto> guestMenuModelsToGuestMenuReducedDtos(List<GuestMenu> guestMenu);

    GuestMenu guestMenuCreationDtopToguestMenuModel(GuestMenuCreationDto guestMenuCreationDto);

    @IterableMapping(elementTargetType = GuestMenu.class)
    List<GuestMenu> guestMenuCreationDtospToguestMenuModels(List<GuestMenuCreationDto> guestMenuCreationDtos);
}
