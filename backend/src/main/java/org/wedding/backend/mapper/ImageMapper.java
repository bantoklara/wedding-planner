package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.outgoing.ImageDto;
import org.wedding.backend.model.Image;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImageMapper {
    ImageDto imageModelToImageDto(Image image);

    @IterableMapping(elementTargetType = ImageDto.class)
    List<ImageDto> imageModelsToImageDtos(List<Image> image);
}
