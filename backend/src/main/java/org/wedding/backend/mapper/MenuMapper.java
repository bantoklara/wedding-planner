package org.wedding.backend.mapper;

import org.mapstruct.Mapper;
import org.wedding.backend.dto.outgoing.MenuDto;
import org.wedding.backend.model.Menu;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MenuMapper {
    MenuDto menuModelToMenuDto(Menu menu);

    List<MenuDto> menuModelsToMenuDtos(List<Menu> menus);
}
