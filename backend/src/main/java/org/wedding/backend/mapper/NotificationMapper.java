package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.outgoing.NotificationDto;
import org.wedding.backend.model.Notification;

import java.util.List;

@Mapper(componentModel = "spring")
public interface NotificationMapper {
    @IterableMapping(elementTargetType = NotificationDto.class)
    List<NotificationDto> notificationModelsToNotificationDtos(List<Notification> notifications);

}
