package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.ScheduleCreationDto;
import org.wedding.backend.dto.outgoing.ScheduleDto;
import org.wedding.backend.model.Schedule;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ScheduleMapper {
    Schedule scheduleCreationDtoToScheduleModel(ScheduleCreationDto scheduleCreationDto);

    ScheduleDto scheduleModelToScheduleDto(Schedule schedule);

    @IterableMapping(elementTargetType = ScheduleDto.class)
    List<ScheduleDto> scheduleModelsToScheduleDtos(List<Schedule> schedule);
}
