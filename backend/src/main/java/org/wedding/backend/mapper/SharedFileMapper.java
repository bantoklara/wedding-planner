package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.outgoing.SharedFileDto;
import org.wedding.backend.model.SharedFile;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SharedFileMapper {
    SharedFileDto sharedFileModelToSharedFileDto(SharedFile sharedFile);

    @IterableMapping(elementTargetType = SharedFileDto.class)
    List<SharedFileDto> sharedFileModelsToSharedFileDtos(List<SharedFile> sharedFile);

}
