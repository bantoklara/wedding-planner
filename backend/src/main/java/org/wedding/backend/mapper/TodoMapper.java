package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.TodoCreationDto;
import org.wedding.backend.dto.incoming.TodoUpdatingDto;
import org.wedding.backend.dto.outgoing.TodoReducedDto;
import org.wedding.backend.model.Todo;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TodoMapper {
    Todo todoCreationDtoToTodoModel(TodoCreationDto todoCreationDto);
    Todo todoUpdatingDtoToTodoModel(TodoUpdatingDto todoUpdatingDto);

    TodoReducedDto todoModelToTodoReducedDto(Todo todo);

    @IterableMapping(elementTargetType = TodoReducedDto.class)
    List<TodoReducedDto> todoModelsToTodoReducedDtos(List<Todo> todo);

}
