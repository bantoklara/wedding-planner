package org.wedding.backend.mapper;

import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.UserCreationDto;
import org.wedding.backend.dto.outgoing.UserReducedDto;
import org.wedding.backend.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserReducedDto userModelToUserReducedDto(User user);
    User userCreationDtoToUserModel(UserCreationDto userCreationDto);
}
