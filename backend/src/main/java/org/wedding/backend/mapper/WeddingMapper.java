package org.wedding.backend.mapper;

import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.WeddingCreationDto;
import org.wedding.backend.dto.outgoing.WeddingDto;
import org.wedding.backend.model.Wedding;

@Mapper(componentModel = "spring")
public interface WeddingMapper {
    Wedding weddingCreationDtoToWeddingModel(WeddingCreationDto weddingCreationDto);

    WeddingDto weddingModelToWeddingDto(Wedding wedding);
}
