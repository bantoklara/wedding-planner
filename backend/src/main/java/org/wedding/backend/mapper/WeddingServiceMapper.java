package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.ServiceCreationDto;
import org.wedding.backend.dto.incoming.ServiceUpdateDto;
import org.wedding.backend.dto.outgoing.ServiceDetailsDto;
import org.wedding.backend.dto.outgoing.ServiceDto;
import org.wedding.backend.dto.outgoing.ServiceReducedDto;
import org.wedding.backend.model.Service;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WeddingServiceMapper {
    ServiceDetailsDto weddingServiceModelToWeddingServiceDto(Service service);

    @IterableMapping(elementTargetType = ServiceDetailsDto.class)
    List<ServiceDetailsDto> weddingServiceModelsToWeddingServiceDetailsDtos(List<Service> service);

    Service weddingServiceCreationDtoToWeddingServiceModel(ServiceCreationDto serviceCreationDto);

    Service weddingServiceUpdateDtoToWeddingServiceModel(ServiceUpdateDto weddingServiceUpdateDto);

    ServiceDto weddingServiceModelToWeddingServiceReducedDto(Service service);

    @IterableMapping(elementTargetType = ServiceDto.class)
    List<ServiceDto> weddingServiceModelsToWeddingServiceDtos(List<Service> service);

    @IterableMapping(elementTargetType = ServiceReducedDto.class)
    List<ServiceReducedDto> weddingServiceModelsToWeddingServiceReducedDtos(List<Service> service);

}
