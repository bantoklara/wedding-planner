package org.wedding.backend.mapper;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.wedding.backend.dto.incoming.WeddingTableCreationDto;
import org.wedding.backend.dto.outgoing.WeddingTableDto;
import org.wedding.backend.model.WeddingTable;

import java.util.List;

@Mapper(componentModel = "spring")
public interface WeddingTableMapper {
    WeddingTable weddingTableCreationDtoToWeddingTableModel(WeddingTableCreationDto weddingTableCreationDto);
    WeddingTableDto weddingTableModelToWeddingTableDto(WeddingTable weddingTable);

    @IterableMapping(elementTargetType = WeddingTableDto.class)
    List<WeddingTableDto> weddingTableModelsToWeddingTableDtos(List<WeddingTable> weddingTable);
}
