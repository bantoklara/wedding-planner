package org.wedding.backend.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.wedding.backend.model.util.Status;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BookedService extends BaseEntity {
    private Integer price;

    @Enumerated(EnumType.STRING)
    private Status status;

    //    Relationships
    @ManyToOne
    @JoinColumn(name = "wedding_id")
    private Wedding wedding;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Service service;

    @OneToMany(mappedBy = "bookedService", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    private List<SharedFile> sharedFiles;
}
