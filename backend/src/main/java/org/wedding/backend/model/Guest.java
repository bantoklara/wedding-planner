package org.wedding.backend.model;

import jakarta.persistence.*;
import lombok.*;
import org.wedding.backend.model.util.Role;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Guest extends BaseEntity {
    @Column(length = 10)
    private String firstName;
    @Column(length = 10)
    private String lastName;
    @Column(unique = true, length = 8)
    private String loginCode;
    private Integer numberOfGuests;
    private Integer feedback;
    private String additionalMessage;
    private Integer invitedWithGuest;
    private LocalDateTime lastFeedbackDate;

    @Enumerated(EnumType.STRING)
    private Role role;

    //    Relationships
    @ManyToOne
    private Wedding wedding;

    @ManyToOne
    private GuestGroup guestGroup;

    @ManyToOne
    private WeddingTable weddingTable;

    @OneToMany(mappedBy = "guest", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<GuestMenu> guestMenus;
}
