package org.wedding.backend.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.wedding.backend.model.compositeKey.GuestMenuPK;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode()
public class GuestMenu {
    @EmbeddedId
    private GuestMenuPK id;

    @ManyToOne
    @JoinColumn(name="guest_id")
    @MapsId("guestId")
    private Guest guest;

    @ManyToOne
    @JoinColumn(name="menu_id")
    @MapsId("menuId")
    private Menu menu;

    private Integer amount;
}
