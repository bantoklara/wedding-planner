package org.wedding.backend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Menu extends BaseEntity {
    private String name;

    @OneToMany(mappedBy = "menu")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<GuestMenu> guestMenus;
}
