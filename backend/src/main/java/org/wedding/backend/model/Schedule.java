package org.wedding.backend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.*;

import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Schedule extends BaseEntity {
    private LocalDateTime start;
    private LocalDateTime end;
    private String title;

    //    Relationships
    @ManyToOne
    @JoinColumn(name = "service_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Service service;

    @ManyToOne
    @JoinColumn(name = "booked_service_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private BookedService bookedService;
}
