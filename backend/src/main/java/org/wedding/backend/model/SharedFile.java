package org.wedding.backend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SharedFile extends BaseEntity {
    private String name;
    private String url;
    private String type;
    private LocalDateTime uploadDate;

    //    Relationships
    @ManyToOne
    @JoinColumn(name = "booked_service_id")
    private BookedService bookedService;
}
