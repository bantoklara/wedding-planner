package org.wedding.backend.model;

import jakarta.persistence.*;
import lombok.*;
import org.wedding.backend.model.util.Role;

import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class User extends BaseEntity {
    @Column(length = 30)
    private String firstName;

    @Column(length = 30)
    private String lastName;

    @Column(unique = true, length = 15)
    private String username;

    private String password;

    @Column(length = 20)
    private String email;

    @Column(length = 10)
    private String phoneNumber;

    @Enumerated(EnumType.STRING)
    private Role role;

    //    Relationships
    @OneToOne(mappedBy = "user", fetch = FetchType.LAZY)
    private Wedding wedding;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Notification> notifications;

}
