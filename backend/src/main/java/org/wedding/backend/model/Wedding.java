package org.wedding.backend.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Wedding extends BaseEntity {
    private String nameOfBride;
    private String nameOfFiance;
    private LocalDate dateOfWedding;
    private LocalDate dateOfFeedbackDeadline;

    //    Relationships
    @OneToOne
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @JoinColumn(name = "wedding_id")
    private List<Guest> weddingGuests;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "wedding_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Todo> todos;

    @OneToMany(cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(name = "wedding_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<WeddingTable> tables;

    @OneToMany(mappedBy = "wedding", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<BookedService> bookedServices;
}
