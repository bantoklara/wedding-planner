package org.wedding.backend.model;

import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.List;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class WeddingTable extends BaseEntity {
    private String name;
    private Integer numberOfSeats;

    @ManyToOne
    private Wedding wedding;

    @OneToMany
    @JoinColumn(name = "wedding_table_id")
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private List<Guest> guests;
}
