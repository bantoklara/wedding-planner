package org.wedding.backend.model.compositeKey;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Embeddable
public class GuestMenuPK implements Serializable {
    @Column(name="guest_id")
    Long guestId;

    @Column(name="menu_id")
    Long menuId;
}
