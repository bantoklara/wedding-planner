package org.wedding.backend.model.util;

public enum PriceCategory {
    CHEAP,
    MIDDLE,
    EXPENSIVE
}
