package org.wedding.backend.model.util;

public enum Role {

    ROLE_COUPLE,
    ROLE_SERVICE_PROVIDER,
    ROLE_ADMIN,
    ROLE_WEDDING_GUEST
}
