package org.wedding.backend.model.util;

public enum Status {
    ACCEPTED,
    PENDING
}
