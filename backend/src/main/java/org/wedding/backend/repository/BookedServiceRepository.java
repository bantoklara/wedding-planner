package org.wedding.backend.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.BookedService;

import java.util.List;

@Repository
public interface BookedServiceRepository extends JpaRepository<BookedService, Long>, JpaSpecificationExecutor<BookedService> {
    BookedService findByWeddingIdAndServiceId(Long weddingId, Long serviceId);
}
