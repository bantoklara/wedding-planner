package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Category;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
