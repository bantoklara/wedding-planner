package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.GuestGroup;

@Repository
public interface GuestGroupRepository  extends JpaRepository<GuestGroup, Long> {
}
