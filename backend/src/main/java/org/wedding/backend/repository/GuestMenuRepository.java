package org.wedding.backend.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.GuestMenu;
import org.wedding.backend.model.compositeKey.GuestMenuPK;

import java.util.List;

@Repository
public interface GuestMenuRepository extends JpaRepository<GuestMenu, GuestMenuPK> {
    @Query("delete from GuestMenu where id.guestId=:guestId and id.menuId not in :idList")
    @Modifying
    @Transactional
    int deleteNotUpdatedGuestMenus(@Param("guestId") Long guestId, @Param("idList") List<Long> idList);
}
