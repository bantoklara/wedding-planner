package org.wedding.backend.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Guest;

import java.util.List;
import java.util.Optional;

@Repository
public interface GuestRepository extends JpaRepository<Guest, Long>, JpaSpecificationExecutor<Guest> {
    Optional<Guest> findByLoginCode(String code);

    List<Guest> findByWeddingId(Long weddingId);

    @Query("update Guest set feedback=:feedback where id=:guestId")
    @Modifying
    @Transactional
    int setGuestFeedback(
            @Param("guestId") Long guestId,
            @Param("feedback") Integer feedback);

    boolean existsGuestByLoginCode(String loginCode);

    Integer countByWeddingId(Long weddingId);

    int countByFeedbackAndWeddingId(Integer feedback, Long weddingId);
}
