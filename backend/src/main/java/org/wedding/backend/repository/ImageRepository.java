package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Image;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {
    List<Image> findByServiceId(Long weddingServiceId);

    @Query(value = "select * from image where wedding_service_id=:weddingServiceId and cover_image=true", nativeQuery = true)
    Image findCoverImageOfService(@Param("weddingServiceId") Long weddingServiceId);

    @Query(value = "select * from image where wedding_service_id=:serviceId and id not in :list", nativeQuery = true)
    List<Image> findImagesToDelete(@Param("serviceId") Long serviceId, @Param("list") List<Long> ids);
}
