package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Menu;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {
}
