package org.wedding.backend.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Notification;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {
    List<Notification> findByUserIdOrderByDateDesc(Long userId);

    @Query(value = "update notification set unread=false where user_id=:userId and unread=:isUnread", nativeQuery = true)
    @Transactional
    @Modifying
    void updateReadStateByUserId(@Param("userId") Long userId, @Param("isUnread") Boolean isUnread);

    Long countByUnreadAndUserId(Boolean unread, Long userId);
}
