package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Service;

import java.util.List;

@Repository
public interface ServiceRepository extends JpaRepository<Service, Long>, JpaSpecificationExecutor<Service> {
    @Query(value = "select user_id from service where id=:id", nativeQuery = true)
    Long findUserId(Long id);

    List<Service> findByUserId(Long userId);
}
