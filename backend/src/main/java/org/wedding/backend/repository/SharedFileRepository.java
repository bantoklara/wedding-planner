package org.wedding.backend.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.SharedFile;

import java.util.List;
import java.util.Map;

@Repository
public interface SharedFileRepository extends JpaRepository<SharedFile, Long> {
    List<SharedFile> findByBookedServiceId(Long bookedServiceId, Pageable pageable);

    int countByBookedServiceId(Long bookedServiceId);
}
