package org.wedding.backend.repository;

import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Todo;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
    @Query(value = "select wedding_id from todo where id=:id", nativeQuery = true)
    Long findWeddingIdById(Long id);

    @Query(value = "update todo set done=:done where id=:id", nativeQuery = true)
    @Modifying
    @Transactional
    Integer setTodoState(@Param("done") Boolean done, @Param("id") Long id);
}