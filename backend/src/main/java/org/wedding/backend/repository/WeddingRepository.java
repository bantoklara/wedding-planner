package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Wedding;
import org.wedding.backend.model.WeddingTable;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface WeddingRepository extends JpaRepository<Wedding, Long> {
    Optional<Wedding> findByUserId(Long id);

    @Query(value = "select id from wedding where user_id=:userId", nativeQuery = true)
    Long findIdByUserId(Long userId);

    LocalDate findDateOfWeddingByUserId(Long userId);
}
