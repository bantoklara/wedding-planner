package org.wedding.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.WeddingTable;

import java.util.List;

@Repository
public interface WeddingTableRepository extends JpaRepository<WeddingTable, Long> {
    List<WeddingTable> findByWeddingId(Long weddingId);

    @Query("SELECT wedding.id FROM WeddingTable WHERE id=:id")
    Long findWeddingIdById(Long id);

    @Query("SELECT t FROM WeddingTable t WHERE t.wedding.id=:weddingId")
    List<WeddingTable> findIdByWeddingIdGroupById(Long weddingId);
}
