package org.wedding.backend.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.*;
import org.wedding.backend.model.util.Status;

public class BookedServiceSpecification {

    public static Specification<BookedService> hasCoupleId(Long coupleId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(BookedService_.WEDDING).get(Wedding_.USER).get(User_.ID), coupleId);
    }

    public static Specification<BookedService> hasProviderId(Long providerId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(BookedService_.SERVICE).get(Service_.USER).get(User_.ID), providerId);
    }

    public static Specification<BookedService> hasStatus(Status status) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(BookedService_.STATUS), status);
    }
}
