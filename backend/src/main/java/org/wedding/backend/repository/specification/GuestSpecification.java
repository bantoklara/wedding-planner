package org.wedding.backend.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.Guest_;
import org.wedding.backend.model.WeddingTable_;
import org.wedding.backend.model.Wedding_;

public class GuestSpecification {
    public static Specification<Guest> belongsTo(Long weddingId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Guest_.WEDDING).get(Wedding_.ID), weddingId);
    }

    public static Specification<Guest> nameLike(String name) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.like(
                        criteriaBuilder.concat(root.get(Guest_.LAST_NAME), criteriaBuilder.concat(" ", root.get(Guest_.FIRST_NAME))),
                        "%" + name + "%");
    }

    public static Specification<Guest> feedbackIs(Integer feedback) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Guest_.FEEDBACK), feedback);
    }

    public static Specification<Guest> hasNoTable() {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.isNull(root.get(Guest_.WEDDING_TABLE).get(WeddingTable_.ID));
    }

    public static Specification<Guest> seatsAtTable(Long tableId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Guest_.WEDDING_TABLE).get(WeddingTable_.ID), tableId);
    }
}
