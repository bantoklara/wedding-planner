package org.wedding.backend.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.Notification;
import org.wedding.backend.model.Notification_;
import org.wedding.backend.model.User_;

import java.time.LocalDateTime;

public class NotificationSpecification {
    public static Specification<Notification> belongsToUserWithId(Long userId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Notification_.USER).get(User_.ID), userId);
    }

    public static Specification<Notification> isUnread() {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Notification_.UNREAD), true);
    }
}
