package org.wedding.backend.repository.specification;

import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.Schedule;
import org.wedding.backend.model.Schedule_;
import org.wedding.backend.model.Service_;
import org.wedding.backend.model.User_;

public class ScheduleSpecification {
    public static Specification<Schedule> belongsToUserWithId(Long userId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Schedule_.SERVICE).get(Service_.USER).get(User_.ID), userId);
    }
}
