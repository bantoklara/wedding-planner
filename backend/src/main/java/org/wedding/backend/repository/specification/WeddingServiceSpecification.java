package org.wedding.backend.repository.specification;

import jakarta.persistence.criteria.Expression;
import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.Category_;
import org.wedding.backend.model.Service;
import org.wedding.backend.model.Service_;
import org.wedding.backend.model.User_;
import org.wedding.backend.model.util.PriceCategory;

import java.util.List;

public class WeddingServiceSpecification {
    public static Specification<Service> myServices(Long ownerId) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Service_.USER).get(User_.ID), ownerId);
    }

    public static Specification<Service> isEnabled(Boolean enabled) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Service_.ENABLED), enabled);
    }

    public static Specification<Service> countyLike(String county) {
        return ((root, query, criteriaBuilder) ->
                criteriaBuilder.like(root.get(Service_.COUNTIES), "%" + county + "%"));
    }

    public static Specification<Service> hasCategoryId(Long id) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Service_.CATEGORY).get(Category_.ID), id);
    }

    public static Specification<Service> hasPriceCategory(PriceCategory priceCategory) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get(Service_.PRICE_CATEGORY), priceCategory);
    }

    public static Specification<Service> hasEnoughPlace(Integer requiredSpaceCapacity) {
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.lessThanOrEqualTo(root.get(Service_.MAXIMAL_SPACE_CAPACITY), requiredSpaceCapacity);
    }

    public static Specification<Service> idBetween(List<Long> ids) {
        return (root, query, criteriaBuilder) -> {
            Expression<Long> idExpression = root.get(Service_.ID);
            return idExpression.in(ids);
        };
    }
}
