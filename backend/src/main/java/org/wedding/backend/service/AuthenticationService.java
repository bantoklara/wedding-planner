package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.wedding.backend.dto.incoming.AuthRequestDto;
import org.wedding.backend.exception.UsernameExistsException;
import org.wedding.backend.model.User;
import org.wedding.backend.model.util.Role;

@Service
public class AuthenticationService {
    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public String login(User user, AuthRequestDto authRequest) {
        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        if (authentication.isAuthenticated()) {
            if (user != null)
                return jwtService.generateToken(
                        authRequest.getUsername(),
                        user);
        }
        return null;
    }

    public User register(User user) throws UsernameExistsException {
        if (userService.existsByUsername(user.getUsername()))
            throw new UsernameExistsException("Username already exists");

        return userService.create(user);
    }

    public Boolean isAdmin(String username) {
        return userService.findByUsername(username).getRole().equals(Role.ROLE_ADMIN);
    }

    public Long getUserIdByUsername(String username) {
        return userService.findUserIdByUsername(username);
    }

    public User getUserByUsername(String username) {
        return userService.findByUsername(username);
    }
}
