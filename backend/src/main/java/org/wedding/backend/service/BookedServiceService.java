package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.Service;
import org.wedding.backend.model.User;
import org.wedding.backend.model.Wedding;
import org.wedding.backend.model.util.Role;
import org.wedding.backend.model.util.Status;
import org.wedding.backend.repository.BookedServiceRepository;

import java.util.*;

import static org.springframework.data.jpa.domain.Specification.where;
import static org.wedding.backend.repository.specification.BookedServiceSpecification.*;

@org.springframework.stereotype.Service
public class BookedServiceService {
    @Autowired
    private BookedServiceRepository repository;

    @Autowired
    private WeddingService weddingService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    AuthenticationService authenticationService;

//    CREATE

    public BookedService create(String username, Long serviceId) {
        BookedService bookedService = new BookedService();
        Wedding wedding = authenticationService.getUserByUsername(username).getWedding();
        Service service = serviceService.findByUsernameAndId(username, serviceId);

        if (wedding == null || service == null || repository.findByWeddingIdAndServiceId(wedding.getId(), service.getId()) != null)
            return null;

        bookedService.setWedding(wedding);
        bookedService.setService(service);
        bookedService.setStatus(Status.PENDING);
        return repository.save(bookedService);
    }

//    READ

    public BookedService findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public BookedService isReserved(String username, Long serviceId) {
        Wedding wedding = authenticationService.getUserByUsername(username).getWedding();
        return repository.findByWeddingIdAndServiceId(wedding.getId(), serviceId);
    }

    public Map<String, Object> findBookedServiceOfUser(
            String username,
            Integer page, Integer perPage,
            String sortBy,
            String order,
            Status status
    ) {
        User user = authenticationService.getUserByUsername(username);

//        Filtering
        Specification<BookedService> spec = where(user.getRole().equals(Role.ROLE_COUPLE)
                ? hasCoupleId(user.getId())
                : hasProviderId(user.getId()));

        spec = spec.and(hasStatus(status != null ? status : Status.ACCEPTED));

//        Sorting
        Sort.Direction direction = order != null && order.equals("DESC") ? Sort.Direction.DESC : Sort.Direction.ASC;
        Sort sorts = getSortingAttributes(sortBy != null ? sortBy : "", direction);

//        Paging
        Pageable pageable = page != null && perPage != null && page >= 0 && perPage > 0
                ? PageRequest.of(page, perPage, sorts)
                : PageRequest.of(0, 10, sorts);

        Map<String, Object> answer = new HashMap<>();
        answer.put("size", repository.count(spec));
        answer.put("bookedServices", repository.findAll(spec, pageable).getContent());

        return answer;
    }

    //    UPDATE
    public BookedService updateBookedService(
            Long id,
            BookedService bookedService
    ) {
        BookedService bookedServiceFromDb = repository.findById(id).orElse(null);

        if (bookedServiceFromDb == null)
            return null;

        bookedService.setId(bookedServiceFromDb.getId());
        bookedService.setWedding(bookedServiceFromDb.getWedding());
        bookedService.setService(bookedServiceFromDb.getService());

        return repository.save(bookedService);
    }

    //    DELETE
    public BookedService deleteBookedService(Long id) {
        BookedService bookedService = repository.findById(id).orElse(null);
        if (bookedService == null)
            return null;
        repository.deleteById(id);
        return bookedService;
    }

    private Sort getSortingAttributes(String key, Sort.Direction direction) {
        switch (key) {
            case "coupleName" -> {
                return Sort.by(direction, "wedding.user.lastName", "wedding.user.firstName");
            }
            case "providerName" -> {
                return Sort.by(direction, "service.user.lastName", "service.user.firstName");
            }
            case "dateOfWedding" -> {
                return Sort.by(direction, "wedding.dateOfWedding");
            }
            case "category" -> {
                return Sort.by(direction, "service.category.name");
            }
            default -> {
                return Sort.by(direction, "service.name");
            }
        }
    }
}
