package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.GuestGroup;
import org.wedding.backend.repository.GuestGroupRepository;

import java.util.List;

@Service
public class GuestGroupService {

    @Autowired
    private GuestGroupRepository repository;

    public GuestGroup save(GuestGroup entity) {
        return null;
    }

    public List<GuestGroup> findAll() {
        return repository.findAll();
    }

    public GuestGroup findById(long id) {
        return repository.findById(id).orElse(null);
    }

    public boolean existsById(long id) {
        return repository.existsById(id);
    }
}
