package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.GuestMenu;
import org.wedding.backend.model.compositeKey.GuestMenuPK;
import org.wedding.backend.repository.GuestMenuRepository;

import java.util.List;

@Service
public class GuestMenuService {
    @Autowired
    private GuestMenuRepository repository;

    public GuestMenu saveOrUpdate(GuestMenu entity) {
        return repository.save(entity);
    }

    public List<GuestMenu> findAll() {
        return repository.findAll();
    }

    public GuestMenu findById(GuestMenuPK id) {
        return repository.findById(id).orElse(null);
    }

    public void delete(GuestMenuPK id) {
        repository.deleteById(id);
    }

    public boolean existsById(GuestMenuPK id) {
        return repository.existsById(id);
    }

    public Integer deleteNotUpdatedGuestMenus(Long guestId, List<Long> menuId) {
        if (menuId.isEmpty())
            menuId.add(-1L);
        return repository.deleteNotUpdatedGuestMenus(guestId, menuId);
    }
}
