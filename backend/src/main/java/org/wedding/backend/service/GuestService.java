package org.wedding.backend.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;
import org.wedding.backend.configuration.LoginCodeAuthenticationToken;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.GuestMenu;
import org.wedding.backend.model.User;
import org.wedding.backend.model.WeddingTable;
import org.wedding.backend.model.compositeKey.GuestMenuPK;
import org.wedding.backend.repository.GuestRepository;

import java.io.IOException;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;
import static org.wedding.backend.repository.specification.GuestSpecification.*;

@Service
public class GuestService {

    @Autowired
    private GuestRepository repository;

    @Autowired
    private GuestGroupService guestGroupService;

    @Autowired
    private GuestMenuService guestMenuService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private WeddingService weddingService;

    @Autowired
    private WeddingTableService weddingTableService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private ObjectMapper objectMapper;

    public List<Guest> createGuests(String username, List<Guest> guests) {
        User user = authenticationService.getUserByUsername(username);
        for (Guest guest : guests) {
            guest.setWedding(user.getWedding());
            guest.setGuestGroup(guest.getGuestGroup() != null ? guestGroupService.findById(guest.getGuestGroup().getId()) : null);
            guest.setFeedback(3);
        }
        return repository.saveAll(guests);
    }

    public Map<WeddingTable, List<Guest>> findGuestsGroupedByWeddingTable(String username) {
        User user = authenticationService.getUserByUsername(username);
        Long weddingId = user != null ? user.getWedding().getId() : null;

        if (weddingId == null)
            return null;

        List<WeddingTable> tableList = weddingTableService.getListOfTableIds(weddingId);

        Map<WeddingTable, List<Guest>> response = new HashMap<>();
        for (WeddingTable table : tableList) {
            Specification<Guest> spec = where(belongsTo(weddingId)).and(feedbackIs(1)).and(seatsAtTable(table.getId()));
            response.put(table, repository.findAll(spec));
        }
        Specification<Guest> spec = where(belongsTo(weddingId)).and(feedbackIs(1)).and(hasNoTable());
        response.put(new WeddingTable(), repository.findAll(spec));

        return response;
    }

    public Guest getGuestById(long id) {
        return repository.findById(id).orElse(null);
    }

    public Guest findGuestByLoginCode(String loginCode) {
        return repository.findByLoginCode(loginCode).orElse(null);
    }

    public Guest updateGuest(long id, Guest guestToUpdate) {
        Guest guest = repository.findById(id).orElse(null);
        if (guest != null) {
            List<Long> menuIdList = new ArrayList<Long>();
            guestToUpdate.setId(id);
            guestToUpdate.setWedding(guest.getWedding());
            guestToUpdate.setLoginCode(guest.getLoginCode());
            if (guestToUpdate.getGuestGroup().getId() == null) {
                guestToUpdate.setGuestGroup(null);
            }
//            guestToUpdate.setGuestGroup(guestGroupService.findById(guestToUpdate.getGuestGroup().getId()));
            guestToUpdate.getGuestMenus().forEach((guestMenu -> {
                guestMenu.setGuest(guestToUpdate);
                guestMenu.setMenu(menuService.findById(guestMenu.getId().getMenuId()));
                menuIdList.add(guestMenu.getId().getMenuId());
            }));
            guestMenuService.deleteNotUpdatedGuestMenus(guestToUpdate.getId(), menuIdList);
            return repository.save(guestToUpdate);
        }
        return null;
    }

    public void delete(long id) {
        if (existsById(id)) {
            repository.deleteById(id);
        }
    }

    public boolean existsById(long id) {
        return repository.existsById(id);
    }

    public Guest updateByCouple(Long id, Map<String, Object> fields) {
        Guest guest = repository.findById(id).orElse(null);
        return updatePartially(guest, fields);
    }

    public Guest updateByGuest(String loginCode, Map<String, Object> fields) {
        Guest guest = repository.findByLoginCode(loginCode).orElse(null);
        return updatePartially(guest, fields);
    }

    private Guest updatePartially(Guest guest, Map<String, Object> fields) {
        if (guest != null) {
            fields.forEach((key, value) -> {
                switch (key) {
                    case ("guestMenus") -> {
                        List<GuestMenu> guestMenus = new ArrayList<GuestMenu>();
                        if (value.getClass().equals(ArrayList.class)) {
                            // [{menuId, amount}]
                            ((ArrayList<?>) value).forEach((array) -> {
                                // array: {menuId, amount}
                                guestMenus.add(buildGuestMenu((LinkedHashMap<String, Object>) array, guest));
                            });
                        }
                        guestMenuService.deleteNotUpdatedGuestMenus(guest.getId(), guestMenus.stream().map((guestMenu) -> guestMenu.getId().getMenuId()).collect(Collectors.toList()));
                        guest.setGuestMenus(guestMenus);
                    }
                    case "guestGroupId" -> guest.setGuestGroup(guestGroupService.findById(Long.valueOf((Integer) value)));

                    case "weddingTableId" -> guest.setWeddingTable(weddingTableService.findById(Long.valueOf((Integer) value)));
                    case "numberOfGuests" -> {
                        if (guest.getWeddingTable() != null) {
                            WeddingTable table = guest.getWeddingTable();
                            int occupiedSeats = table.getGuests().stream()
                                    .map(Guest::getNumberOfGuests)
                                    .reduce(0, Integer::sum);
                            if (table.getNumberOfSeats() - occupiedSeats < (Integer) value) {
                                guest.setWeddingTable(null);
                            }
                        }
                        guest.setNumberOfGuests((Integer) value);
                    }
                    default -> {
                        Field field = ReflectionUtils.findField(Guest.class, key);
                        if (field != null) {
                            field.setAccessible(true);
                            ReflectionUtils.setField(field, guest, value);
                        }
                    }
                }
            });
            guest.setLastFeedbackDate(LocalDateTime.now());
            return repository.save(guest);
        }
        return null;
    }

    public List<Guest> generateLoginCodes(String username) {
        Long userId = authenticationService.getUserIdByUsername(username);
        Long weddingId = weddingService.findWeddingIdByUserId(userId);
        List<Guest> guests = repository.findByWeddingId(weddingId);

        for (Guest guest : guests) {
            guest.setLoginCode(generate(guest.getId()));
        }
        return repository.saveAll(guests);
    }

    public Guest generateLoginCode(Long id) {
        Guest guest = repository.findById(id).orElse(null);
        if (guest == null) return null;

        guest.setLoginCode(generate(id));
        return repository.save(guest);
    }

    private String generate(Long id) {
        String idAsString = id.toString();
        String idd = idAsString.length() < 4 ? "0".repeat(4 - idAsString.length()) + idAsString : idAsString.length() > 4 ? idAsString.substring(idAsString.length() - 4) : idAsString;
        String randomString = generateRandomStringOfNLength(4);
        while (repository.existsGuestByLoginCode(idd + randomString)) {
            randomString = generateRandomStringOfNLength(4);
        }
        return idd + randomString;
    }

    private String generateRandomStringOfNLength(Integer n) {
        return new Random().ints(65, 90 + 1).limit(n).collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append).toString();
    }

    private GuestMenu buildGuestMenu(LinkedHashMap<String, Object> fields, Guest guest) {
        GuestMenu guestMenu = new GuestMenu();
        if (fields.getClass().equals(LinkedHashMap.class)) {
            LinkedHashMap<String, Object> arrFields = (LinkedHashMap<String, Object>) fields;
            arrFields.forEach((key, value) -> {
                if (key.equals("menuId")) {
                    guestMenu.setId(new GuestMenuPK(guest.getId(), Long.valueOf((Integer) value)));
                } else {
                    Field field1 = ReflectionUtils.findField(GuestMenu.class, key);
                    field1.setAccessible(true);
                    ReflectionUtils.setField(field1, guestMenu, value);
                }
            });
        }
        guestMenu.setGuest(guest);
        guestMenu.setMenu(menuService.findById(guestMenu.getId().getMenuId()));
        return guestMenu;
    }

    public Integer setGuestFeedback(Long guestId, Integer feedback) {
        return repository.setGuestFeedback(guestId, feedback);
    }

    public Integer getNumberOfGuests(String username) {
        return repository.countByWeddingId(authenticationService.getUserByUsername(username).getWedding().getId());
    }

    public Integer getNumberOfGuestsByFeedback(String username, Integer feedback) {
        Long weddingId = authenticationService.getUserByUsername(username).getWedding().getId();
        Integer count = repository.countByFeedbackAndWeddingId(
                feedback,
                weddingId
        );
        return count;
    }

    public Map<String, Object> filterGuests(
            String username,
            String name,
            Integer feedback,
            Boolean hasNoTable,
            Integer page,
            Integer perPage,
            String sortBy,
            String order) {
        User user = authenticationService.getUserByUsername(username);
        if (user == null) return null;

//        Filters
        Specification<Guest> spec = where(belongsTo(user.getWedding().getId()));
        if (name != null) spec = spec.and(nameLike(name));
        if (feedback != null) spec = spec.and(feedbackIs(feedback));
        if (hasNoTable != null) spec = spec.and(hasNoTable());

//        Pagination
        Sort.Direction direction = order != null && order.equals("DESC") ? Sort.Direction.DESC : Sort.Direction.ASC;
        Sort sort = getSortingAttribute(sortBy != null ? sortBy : "", direction);

        Pageable pageable = page != null && perPage != null && page >= 0 && perPage > 0
                ? PageRequest.of(page, perPage, sort)
                : PageRequest.of(0, 10, sort);

        List<Guest> guests = repository.findAll(spec, pageable).getContent();
        Map<String, Object> answer = new HashMap<>();
        answer.put("numberOfFilteredGuests", repository.count(spec));
        answer.put("guests", guests);
        return answer;
    }

    public String login(String code) {
        Authentication authentication = authenticationManager.authenticate(new LoginCodeAuthenticationToken(code, null));
        if (authentication.isAuthenticated()) {
            Optional<Guest> guest = repository.findByLoginCode(code);
            if (guest.isPresent()) {
                return jwtService.generateToken(code, guest.get());
            }
        }
        return null;
    }

    public List<Guest> updateTableOfGuests(List<Guest> guests) {
        List<Guest> updatedGuests = new ArrayList<>();
        for (Guest guest : guests) {
            Guest updatedGuest = repository.findById(guest.getId()).orElse(null);
            if (updatedGuest != null) {
                updatedGuest.setWeddingTable(weddingTableService.findById(guest.getWeddingTable().getId()));
                updatedGuests.add(updatedGuest);
            }
        }

        return repository.saveAll(updatedGuests);
    }

    private Sort getSortingAttribute(String key, Sort.Direction direction) {
        switch (key) {
            case "feedback" -> {
                return Sort.by(direction, "feedback");
            }
            case "numberOfGuests" -> {
                return Sort.by(direction, "numberOfGuests");
            }
            default -> {
                return Sort.by(direction, "lastName", "firstName", "invitedWithGuest");
            }
        }
    }
}
