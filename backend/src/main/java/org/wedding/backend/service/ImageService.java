package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.model.Image;
import org.wedding.backend.repository.ImageRepository;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
public class ImageService {
    @Autowired
    private ImageRepository repository;

    private static final String UPLOAD_DIR = "static/public/photos";

    public Image create(String description, MultipartFile file) throws IOException {
        return repository.save(buildImage(description, file));
    }

    public List<Image> createAll(List<String> descriptions, List<MultipartFile> images) throws IOException {
        List<Image> createdImages = new ArrayList<>();
        for (int i = 0; i < descriptions.size(); i++) {
            createdImages.add(buildImage(descriptions.get(i), images.get(i)));
        }
        return repository.saveAll(createdImages);
    }

    public Image findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Image> findAllImageOfService(Long serviceId) {
        return repository.findByServiceId(serviceId);
    }

    public Image getCoverImageOfService(Long weddingServiceId) {
        return repository.findCoverImageOfService(weddingServiceId);
    }

    public void deleteImageById(Long id) throws IOException {
        Image imageToDelete = repository.findById(id).orElse(null);

        if (imageToDelete != null) {
            deleteImageFromDir(imageToDelete.getUrl().split("photos/")[1]);
            repository.delete(imageToDelete);
        }
    }

    public void deleteImagesByIdNotInList(Long serviceId, List<Long> ids) throws IOException {
        if (ids.isEmpty())
            ids.add(-1L);
        List<Image> imagesToDelete = repository.findImagesToDelete(serviceId, ids);

//        delete images files from directory
        for (Image image : imagesToDelete) {
            deleteImageFromDir(image.getUrl().split("photos/")[1]);
        }

//        delete images from database
        repository.deleteAll(imagesToDelete);
    }

    private void deleteImageFromDir(String name) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resourceUrl = classLoader.getResource(UPLOAD_DIR);
        assert resourceUrl != null;
        Path path = Paths.get(new File(resourceUrl.getFile()).getAbsolutePath(), name);
        if (Files.exists(path)) {
            Files.delete(path);
        }
    }

    private Image buildImage(String description, MultipartFile file) throws IOException {
        Image image = new Image();
        String generatedName = generateUniqueFileName(".png");
        ClassLoader classLoader = getClass().getClassLoader();
        URL resourceUrl = classLoader.getResource(UPLOAD_DIR);
        Path filenameAndPath = Paths.get(new File(resourceUrl.getFile()).getAbsolutePath(), generatedName);
        Files.write(filenameAndPath, file.getBytes());
        image.setUrl("http://localhost:8080/public/photos/" + generatedName);
        image.setDescription(description);
        return image;
    }

    private String generateUniqueFileName(String ext) {
        return String.format("%s%s", System.currentTimeMillis(), new Random().nextInt(10000) + ext);
    }
}
