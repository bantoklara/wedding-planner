package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.Menu;
import org.wedding.backend.repository.MenuRepository;

import java.util.List;

@Service
public class MenuService {

    @Autowired
    private MenuRepository repository;

    public Menu save(Menu entity) {
        return repository.save(entity);
    }

    public List<Menu> findAll() {
        return repository.findAll();
    }

    public Menu findById(long id) {
        return repository.findById(id)
                .orElse(null);
    }

    public Menu update(long id, Menu entity) {
        return null;
    }

    public void delete(long id) {

    }

    public boolean existsById(long id) {
        return repository.existsById(id);
    }
}
