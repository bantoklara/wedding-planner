package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.Notification;
import org.wedding.backend.model.User;
import org.wedding.backend.repository.NotificationRepository;

import java.time.LocalDateTime;
import java.util.List;

import static org.springframework.data.jpa.domain.Specification.not;
import static org.springframework.data.jpa.domain.Specification.where;
import static org.wedding.backend.repository.specification.NotificationSpecification.belongsToUserWithId;
import static org.wedding.backend.repository.specification.NotificationSpecification.isUnread;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository repository;

    @Autowired
    private UserService userService;

    @Autowired
    private SimpMessagingTemplate template;

    public List<Notification> findAllAndSetAsRead(String username, Boolean unread) {
        Long userId = userService.findUserIdByUsername(username);

        Specification<Notification> spec = where(belongsToUserWithId(userId));
        if (unread != null && unread)
            spec = spec.and(isUnread());

        List<Notification> notifications = repository.findAll(spec, Sort.by(Sort.Direction.DESC, "date"));
        repository.updateReadStateByUserId(userId, true);

        return notifications;
    }

    public void markUnreadNotificationsAsRead(String username) {
        repository.updateReadStateByUserId(userService.findUserIdByUsername(username), true);
    }

    public Long getNumberOfUnreadNotifications(String username) {
        Long userId = userService.findUserIdByUsername(username);
        return repository.countByUnreadAndUserId(true, userId);
    }

    public void notifyCoupleAboutNewGuestFeedback(User couple, Long guestId, String guestName) {
        Notification notification = buildNotification(
                couple,
                guestName + " visszajelzett az eksüvőre.",
                "/guests/" + guestId
        );
        notification.setUser(null);
        template.convertAndSendToUser(
                couple.getUsername(),
                "/notifications/couple/feedback",
                notification);
    }

    public void notifyCoupleABoutServiceResponse(User couple, org.wedding.backend.model.Service service, Boolean accepted) {
        Notification notification = buildNotification(
                couple,
                "A(z) " + service.getName() + " nevű szolgáltatás" + (accepted ? "elfogadta" : "elutasította") + " a szolgáltatási kérelmét",
                "/reservations"
        );
        notification.setUser(null);

        template.convertAndSendToUser(
                couple.getUsername(), "/notifications/couple/service",
                notification);
    }

    public void notifyServiceProviderAboutRequest(org.wedding.backend.model.Service service, User couple) {
        Notification notification = buildNotification(
                service.getUser(),
                couple.getLastName() + " " + couple.getFirstName() + " nevű felhasználó igénybe szeretné venni a(z) " + service.getName() + " nevű szolgáltatását.",
                "/reservations"
        );
        notification.setUser(null);

        template.convertAndSendToUser(
                service.getUser().getUsername(), "/notifications/provider/request",
                notification);
    }

    public void notifyServiceProviderAboutAdminFeedback(org.wedding.backend.model.Service service, Boolean accepted) {
        Notification notification = buildNotification(
                service.getUser(),
                "Az adminisztrátor" + (accepted ? "jóváhagyta " : "törölte ") + "a(z) " + service.getName() + " nevű szolgáltatást.",
                "/services/"
        );
        notification.setUser(null);

        template.convertAndSendToUser(
                service.getUser().getUsername(), "/notifications/provider/service-feedback",
                notification);
    }

    public void notifyAdminAboutNewServiceRequest(User admin, org.wedding.backend.model.Service service) {
        Notification notification = buildNotification(
                admin,
                service.getUser().getLastName() + " " + service.getUser().getFirstName() + " nevű szolgáltató új szolgáltatást szeretne feltölteni.",
                "/services/" + service.getId()
        );
        notification.setUser(null);

        template.convertAndSendToUser(
                admin.getUsername(), "/notifications/admin/request",
                notification);
    }

    private Notification buildNotification(User couple, String message, String link) {
        Notification notification = new Notification();
        notification.setMessage(message);
        notification.setLink(link);
        notification.setDate(LocalDateTime.now());
        notification.setUser(couple);
        notification.setUnread(true);
        return repository.save(notification);
    }
}
