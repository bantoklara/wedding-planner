package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.exception.DateIntervalException;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.Schedule;
import org.wedding.backend.repository.ScheduleRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.wedding.backend.repository.specification.ScheduleSpecification.belongsToUserWithId;

@Service
public class ScheduleService {
    @Autowired
    private ScheduleRepository repository;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private BookedServiceService bookedServiceService;

    @Autowired
    private AuthenticationService authenticationService;

    public Schedule create(Long serviceId, Long bookedServiceId, Schedule schedule) throws DateIntervalException {
        org.wedding.backend.model.Service service = serviceService.findById(serviceId);

        if (schedule.getEnd().compareTo(schedule.getStart()) < 0)
            throw new DateIntervalException("Start date must be before end date.");

        if (service == null)
            return null;

        BookedService bookedService = bookedServiceId != null
                ? bookedServiceService.findById(bookedServiceId)
                : null;

        List<Schedule> schedulesOfService = repository.findByServiceId(serviceId);

        for (Schedule schedule1 : schedulesOfService) {
            if (datesOverlap(schedule1.getStart(), schedule1.getEnd(), schedule.getStart(), schedule.getEnd())) {
                throw new DateIntervalException("Service is already taken in this period");
            }
        }

        schedule.setService(service);
        schedule.setBookedService(bookedService);
        return repository.save(schedule);
    }

    public Schedule findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Schedule> findByServiceId(Long serviceId) {
        return repository.findByServiceId(serviceId);
    }

    public List<Schedule> findByUsername(String username) {
        return repository.findAll(belongsToUserWithId(authenticationService.getUserIdByUsername(username)));
    }

    public List<Long> findOccupiedServices(LocalDateTime start, LocalDateTime end) {
        List<Schedule> schedules = repository.findAll();
        List<Long> serviceId = new ArrayList<>();

        for (Schedule schedule : schedules) {
            if (datesOverlap(schedule.getStart(), schedule.getEnd(), start, end)) {
                serviceId.add(schedule.getService().getId());
            }
        }
        return serviceId;
    }

    public Schedule update(Long id, Long serviceId, Long bookedServiceId, Schedule schedule) throws DateIntervalException {
        if (!repository.existsById(id))
            return null;

        if (schedule.getEnd().compareTo(schedule.getStart()) < 0)
            throw new DateIntervalException("Start date must be before end date.");

        org.wedding.backend.model.Service service = serviceService.findById(serviceId);
        if (service == null)
            return null;

        BookedService bookedService = bookedServiceId != null
                ? bookedServiceService.findById(bookedServiceId)
                : null;

        List<Schedule> schedulesOfService = repository.findByServiceId(serviceId);

        for (Schedule schedule1 : schedulesOfService) {
            if (!schedule1.getId().equals(id) &&
                    datesOverlap(schedule1.getStart(), schedule1.getEnd(), schedule.getStart(), schedule.getEnd())) {
                throw new DateIntervalException("Service is already taken in this period");
            }
        }

        schedule.setId(id);
        schedule.setService(service);
        schedule.setBookedService(bookedService);
        return repository.save(schedule);
    }

    public void delete(Long id) {
        Schedule schedule = repository.findById(id).orElse(null);
        if (schedule == null)
            return;
        repository.delete(schedule);
    }

    private Boolean datesOverlap(LocalDateTime s1, LocalDateTime e1, LocalDateTime s2, LocalDateTime e2) {
        System.out.println(s1.compareTo(s2) + "&&" + e1.compareTo(s2));
        System.out.println(s1.compareTo(e2) + "&&" + e1.compareTo(e2));

        return ((s1.compareTo(s2) < 0 && e1.compareTo(s2) > 0) || // 1: 9, 10, 11, 12
                (s1.compareTo(e2) < 0 && e1.compareTo(e2) > 0) || // 2: 7
                (s2.compareTo(s1) < 0 && e2.compareTo(s1) > 0) || // 3: 5
                (s2.compareTo(e1) < 0 && e2.compareTo(e1) > 0) || // 4: 6
                (s1.compareTo(s2) == 0 && e1.compareTo(e2) == 0) // 5: 8
        );
    }

}
