package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.wedding.backend.dto.incoming.GuestTableUpdateDto;
import org.wedding.backend.dto.incoming.ServiceCreationDto;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.Schedule;
import org.wedding.backend.model.User;
import org.wedding.backend.model.util.Role;

import java.util.List;

@Service("securityService")
public class SecurityService {
    Authentication authentication;
    @Autowired
    private UserService userService;

    @Autowired
    private BookedServiceService bookedServiceService;

    @Autowired
    private ServiceService serviceService;

    @Autowired
    private GuestService guestService;

    @Autowired
    private TodoService todoService;

    @Autowired
    private WeddingTableService tableService;

    @Autowired
    private ScheduleService scheduleService;

    public boolean hasUser(Long bookedServiceId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();

        User user = userService.findByUsername(authentication.getName());
        BookedService bookedService = bookedServiceService.findById(bookedServiceId);

        if (bookedService == null)
            return false;

        return user.getRole().equals(Role.ROLE_COUPLE)
                ? bookedService.getWedding().getId().equals(user.getWedding().getId())
                : bookedService.getService().getUser().getId().equals(user.getId());
    }

    public boolean guestBelongsToWedding(Long guestId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        Guest guest = guestService.getGuestById(guestId);

        return user.getWedding() != null && guest != null && user.getWedding().getId().equals(guest.getWedding().getId());
    }

    public boolean guestsBelongToWedding(List<GuestTableUpdateDto> guests) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());

        for (GuestTableUpdateDto guest : guests) {
            Guest guestFromDb = guestService.getGuestById(guest.getId());
            if (guestFromDb != null && !user.getWedding().getId().equals(guestFromDb.getWedding().getId()))
                return false;
        }
        return true;
    }

    public boolean serviceBelongsToProvider(Long serviceId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());

        return serviceService.findUserIdByServiceId(serviceId).equals(user.getId());
    }

    public boolean todoBelongsToUser(Long todoId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());

        return todoService.findWeddingIdById(todoId).equals(user.getWedding().getId());
    }

    public boolean tableBelongsToWedding(Long tableId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        return tableService.findWeddingIdById(tableId).equals(user.getWedding().getId());
    }

    public boolean scheduleBelongsToUser(Long scheduleId) {
        authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        Schedule schedule = scheduleService.findById(scheduleId);

        return schedule != null && schedule.getService().getUser().equals(user);
    }
}
