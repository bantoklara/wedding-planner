package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.exception.FileFormatException;
import org.wedding.backend.model.*;
import org.wedding.backend.model.Service;
import org.wedding.backend.model.util.PriceCategory;
import org.wedding.backend.model.util.Role;
import org.wedding.backend.model.util.Status;
import org.wedding.backend.repository.ServiceRepository;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.data.jpa.domain.Specification.where;
import static org.wedding.backend.repository.specification.WeddingServiceSpecification.*;

@org.springframework.stereotype.Service
public class ServiceService {
    @Autowired
    private ServiceRepository repository;

    @Autowired
    private ImageService imageService;

    @Autowired
    private AuthenticationService authenticationService;

    @Autowired
    private CategoryService categoryService;

    //    CREATE

    public Service create(
            String username,
            Service service,
            List<String> descriptions,
            MultipartFile coverImage,
            List<MultipartFile> images
    ) throws IOException, FileFormatException {
        User owner = authenticationService.getUserByUsername(username);
        if (owner == null)
            return null;
//        Check if file formats are correct
        if (coverImage.getContentType() == null || !coverImage.getContentType().startsWith("image/"))
            throw new FileFormatException("File must be an image.");
        if (images != null) {
            for (MultipartFile image : images) {
                if (image.getContentType() == null || !image.getContentType().startsWith("image/")) {
                    throw new FileFormatException("File must be an image.");
                }
            }
        }
        service.setUser(owner);
        service.setEnabled(false);
        service.setCategory(categoryService.findById(service.getCategory().getId()));
        if (coverImage != null) {
            service.setCover(imageService.create("cover image", coverImage));
        }

        if (descriptions != null && images != null) {
            service.setImages(imageService.createAll(descriptions, images));
        }

        return repository.save(service);
    }

    public List<Service> findServicesOfUser(String username) {
        Long id = authenticationService.getUserIdByUsername(username);
        List<Service> services = repository.findAll(myServices(id).and(isEnabled(true)), Sort.by(Sort.Direction.ASC, "name"));

        for (Service service : services) {
            List<BookedService> acceptedBookedServices = service.getBookedServices()
                    .stream()
                    .filter(bs -> bs.getStatus().equals(Status.ACCEPTED))
                    .toList();
            service.setBookedServices(acceptedBookedServices);
        }
        return services;
    }

    //    READ
    public Service findByUsernameAndId(String username, Long id) {
        Service service = repository.findById(id).orElse(null);
        if (service == null)
            return null;
        if (!service.isEnabled()) {
            User user = username != null ? authenticationService.getUserByUsername(username) : null;
            if (user == null || (
                    !user.getRole().equals(Role.ROLE_ADMIN)
                            && !(user.getRole().equals(Role.ROLE_SERVICE_PROVIDER) && hasAuthority(username, id))
            )
            ) {
                return null;
            }
        }
        return service;
    }

    public Service findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Service> findUsersService(String username) {
        return repository.findByUserId(authenticationService.getUserIdByUsername(username));
    }

    public Long findUserIdByServiceId(Long id) {
        return repository.findUserId(id);
    }

    public List<Service> filter(
            String username,
            Boolean myServices, Boolean enabled,
            Long categoryId, String county, PriceCategory priceCategory, Integer numberOfSpace,
            LocalDateTime start, LocalDateTime end,
            Integer page
    ) {
        User user = username != null ? authenticationService.getUserByUsername(username) : null;

        Specification<Service> spec;

        if (user != null) {
            switch (user.getRole()) {
                case ROLE_ADMIN -> {
                    spec = where(enabled != null && !enabled ? isEnabled(false) : isEnabled(true));
                }
                case ROLE_SERVICE_PROVIDER -> {
                    if (myServices != null && myServices) {
                        spec = where(myServices(user.getId())
                                .and(enabled != null && !enabled ? isEnabled(false) : isEnabled(true)));
                    } else spec = where(isEnabled(true));
                }
                default -> {
                    spec = where(isEnabled(true));
                }
            }
        } else spec = where(isEnabled(true));

        if (categoryId != null) {
            spec = spec.and(hasCategoryId(categoryId));
        }

        if (priceCategory != null) {
            spec = spec.and(hasPriceCategory(priceCategory));
        }
        if (county != null) {
            spec = spec.and(countyLike(county));
        }
        if (numberOfSpace != null) {
            spec = spec.and(hasEnoughPlace(numberOfSpace));
        }

        if (start != null && end != null) {
            spec = spec.and(idBetween(findIdsOfFreeServices(start, end)));
        }

        Pageable pageable = PageRequest.of(page != null && page > 0 ? page : 0, 10);
        return repository.findAll(spec, pageable).getContent();
    }

    private List<Long> findIdsOfFreeServices(LocalDateTime start, LocalDateTime end) {
        List<Service> services = repository.findAll(isEnabled(true));
        List<Long> serviceIds = new ArrayList<>();

        for (Service service : services) {
            boolean hasScheduleAtInterval = false;
            for (Schedule schedule : service.getSchedules()) {
                if (datesOverlap(schedule.getStart(), schedule.getEnd(), start, end)) {
                    hasScheduleAtInterval = true;
                    break;
                }
            }
            if (!hasScheduleAtInterval) {
                serviceIds.add(service.getId());
            }
        }
        return serviceIds;
    }

    public Service findByIdIfEnabled(Long id) {
        Service service = repository.findById(id).orElse(null);
        if (service == null || !service.isEnabled()) {
            return null;
        }
        return service;
    }

    //    UPDATE
    public Service update(
            String username,
            Long id,
            Service service,
            List<String> descriptions,
            MultipartFile coverImage,
            List<MultipartFile> images) throws IOException, FileFormatException {
        Service serviceFromDb = repository.findById(id).orElse(null);

        if (serviceFromDb == null) {
            return null;
        }
        Long oldCoverImageId = serviceFromDb.getCover().getId();
        service.setId(id);
        service.setUser(authenticationService.getUserByUsername(username));
        service.setEnabled(serviceFromDb.isEnabled());
//        Check if file formats are correct
        if (coverImage != null && (coverImage.getContentType() == null || !coverImage.getContentType().startsWith("image/"))) {
            throw new FileFormatException("File mut be an image.");
        }
        if (images != null) {
            for (MultipartFile image : images) {
                if (image.getContentType() == null || !image.getContentType().startsWith("image/")) {
                    throw new FileFormatException("File must be an image");
                }
            }
        }
//        Check if cover image was modified
        if (coverImage != null) {
            if (coverImage.getContentType() == null || !coverImage.getContentType().startsWith("image/")) {
                throw new FileFormatException("File mut be an image.");
            }
            service.setCover(imageService.create("cover image", coverImage));
        } else {
            service.setCover(serviceFromDb.getCover());
        }

//        Delete images if this is the case
        if (service.getImages() != null &&
                serviceFromDb.getImages() != null &&
                service.getImages().size() != serviceFromDb.getImages().size()) {
            List<Long> ids = service.getImages().isEmpty() ? List.of(-1L) : service.getImages().stream().map(BaseEntity::getId).toList();
            imageService.deleteImagesByIdNotInList(id, ids);
        }

//        Check if there are new images
        List<Image> serviceImages = service.getImages();
        if (descriptions != null && images != null) {
            serviceImages.addAll(imageService.createAll(descriptions, images));
        }
        service.setImages(serviceImages);

        Service updatedService = repository.save(service);
        if (coverImage != null) {
            imageService.deleteImageById(oldCoverImageId);
        }

        return updatedService;
    }

    public Service enableService(Long id) {
        Service service = repository.findById(id).orElse(null);
        if (service != null && !service.isEnabled()) {
            service.setEnabled(true);
            return repository.save(service);
        }
        return null;
    }

    //    DELETE
    public Service delete(String username, Long id) {
        Service service = repository.findById(id).orElse(null);
        if (service == null)
            return null;
        repository.deleteById(id);
        return service;
    }

    //    UTILS
    public Boolean hasAuthority(String username, Long id) {
        return authenticationService.getUserIdByUsername(username).equals(repository.findUserId(id));
    }

    private Boolean datesOverlap(LocalDateTime s1, LocalDateTime e1, LocalDateTime s2, LocalDateTime e2) {
        return ((s1.compareTo(s2) < 0 && e1.compareTo(s2) > 0) || // 1: 9, 10, 11, 12
                (s1.compareTo(e2) < 0 && e1.compareTo(e2) > 0) || // 2: 7
                (s2.compareTo(s1) < 0 && e2.compareTo(s1) > 0) || // 3: 5
                (s2.compareTo(e1) < 0 && e2.compareTo(e1) > 0) || // 4: 6
                (s1.compareTo(s2) == 0 && e1.compareTo(e2) == 0) // 5: 8
        );
    }
}
