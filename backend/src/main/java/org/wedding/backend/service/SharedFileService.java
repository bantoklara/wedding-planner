package org.wedding.backend.service;

import org.apache.commons.compress.utils.FileNameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.wedding.backend.model.BookedService;
import org.wedding.backend.model.SharedFile;
import org.wedding.backend.repository.SharedFileRepository;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@Service
public class SharedFileService {
    @Autowired
    private SharedFileRepository repository;

    @Autowired
    private BookedServiceService bookedServiceService;

    private static final String UPLOAD_DIR = "static/public/documents";

    public Map<String, Object> getSharedFiles(Long bookedServiceId, Integer page, Integer perPage, String sortBy, String order) {
        Sort.Direction direction = order != null && order.equals("DESC") ? Sort.Direction.DESC : Sort.Direction.ASC;
        Sort sort = Sort.by(direction, sortBy != null ? sortBy : "name");
        Pageable pageable = page != null && perPage != null && page > 0 && perPage > 0
                ? PageRequest.of(page, perPage, sort)
                : PageRequest.of(0, 10, sort);
        Map<String, Object> response = new HashMap<>();
        response.put("size", repository.countByBookedServiceId(bookedServiceId));
        response.put("sharedFiles", repository.findByBookedServiceId(bookedServiceId, pageable));
        return response;
    }

    //    CREATE
    public SharedFile create(Long bookedServiceId, MultipartFile file) throws IOException {
        BookedService bookedService = bookedServiceService.findById(bookedServiceId);

        if (bookedService == null)
            return null;
        SharedFile sharedFile = buildFile(file);
        sharedFile.setBookedService(bookedService);
        return repository.save(sharedFile);
    }

    //    DELETE
    public void delete(Long id) {
        repository.deleteById(id);
    }

    private SharedFile buildFile(MultipartFile file) throws IOException {
        SharedFile sharedFile = new SharedFile();
        String extension = FileNameUtils.getExtension(file.getOriginalFilename());
        String generatedName = generateUniqueFileName(extension);
        ClassLoader classLoader = getClass().getClassLoader();
        URL resourceUrl = classLoader.getResource(UPLOAD_DIR);
        Path filenameAndPath = Paths.get(new File(resourceUrl.getFile()).getAbsolutePath(), generatedName);
        Files.write(filenameAndPath, file.getBytes());

        sharedFile.setName(file.getOriginalFilename());
        sharedFile.setType(extension);
        sharedFile.setUrl("http://localhost:8080/public/documents/" + generatedName);
        sharedFile.setUploadDate(LocalDateTime.now());

        return sharedFile;
    }

    private String generateUniqueFileName(String ext) {
        return String.format("%s%s", System.currentTimeMillis(), new Random().nextInt(10000) + "." + ext);
    }
}
