package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.helper.ReadExcel;
import org.wedding.backend.model.Category;
import org.wedding.backend.model.Todo;
import org.wedding.backend.repository.TodoRepository;

import java.io.IOException;
import java.util.List;

@Service
public class TodoService {
    @Autowired
    private TodoRepository repository;

    @Autowired
    private CategoryService categoryService;

    public Todo findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<Todo> createTemplateTodos() {
        try {
            List<Todo> todos = ReadExcel.excelToTodos();
            return repository.saveAll(todos);
        } catch (IOException e) {
            throw new RuntimeException("Fail to store excel data: " + e.getMessage());
        }
    }

    public Long findWeddingIdById(Long id) {
        return repository.findWeddingIdById(id);
    }

    public Todo create(Todo todo) {
        Category category = categoryService.findById(todo.getCategory().getId());
        if (category != null)
            todo.setCategory(category);
        todo.setDone(false);
        return repository.save(todo);
    }

    public Todo update(Long id, Todo todo) {
        Todo todoToUpdate = repository.findById(id).orElse(null);
        if (todoToUpdate != null) {
            todo.setId(id);
            todo.setWedding(todoToUpdate.getWedding());
            return repository.save(todo);
        }
        return null;
    }

    public Integer updateTodoState(Long id, Boolean done) {
        return repository.setTodoState(done, id);
    }

    public void delete(Long id) {
        Todo todo = repository.findById(id).orElse(null);
        if (todo != null) {
            repository.delete(todo);
        }
    }
}
