package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.User;
import org.wedding.backend.model.Wedding;
import org.wedding.backend.model.util.Role;
import org.wedding.backend.repository.UserRepository;

@Service
public class UserService {
    @Autowired
    private UserRepository repository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private WeddingService weddingService;

    public User create(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        Wedding wedding = user.getRole().equals(Role.ROLE_COUPLE) ? user.getWedding() : null;
        user.setWedding(null);

        User createdUser = repository.save(user);

        if (wedding != null) {
            wedding.setUser(user);
            weddingService.create(wedding);
        }

        return createdUser;
    }

    public Boolean existsByUsername(String username) {
        return repository.existsUserByUsername(username);
    }

    public User findById(Long userId) {
        return repository.findById(userId).orElse(null);
    }

    public User findByUsername(String username) {
        return repository.findByUsername(username).orElse(null);
    }

    public Long findUserIdByUsername(String username) {
        return repository.findIdByUsername(username);
    }
}
