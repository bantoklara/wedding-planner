package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.dto.outgoing.WeddingStatisticsDto;
import org.wedding.backend.model.*;
import org.wedding.backend.model.util.Status;
import org.wedding.backend.repository.WeddingRepository;

import java.util.List;

@Service
public class WeddingService {
    @Autowired
    private WeddingRepository repository;

    @Autowired
    private TodoService todoService;

    @Autowired
    private WeddingTableService weddingTableService;

    public Wedding findById(Long weddingId) {
        return repository.findById(weddingId).orElse(null);
    }

    public Wedding create(Wedding wedding) {
        List<Todo> todos = todoService.createTemplateTodos();
        wedding.setTodos(todos);
        return repository.save(wedding);
    }

    public Todo createWeddingTodo(Long weddingId, Todo todo) {
        Wedding wedding = repository.findById(weddingId).orElse(null);
        if (wedding != null) {
            Todo createdTodo = todoService.create(todo);
            List<Todo> todos = wedding.getTodos();
            todos.add(createdTodo);
            wedding.setTodos(todos);
            repository.save(wedding);
            return createdTodo;
        }
        return null;
    }

    public WeddingStatisticsDto getStatisticsAboutWedding(Long id) {
        Wedding wedding = repository.findById(id).orElse(null);

        if (wedding == null)
            return null;

        List<Guest> guests = wedding.getWeddingGuests();
        Integer numberOfInvitationsWithPositiveFeedback = Math.toIntExact(guests.stream().filter(guest -> guest.getFeedback().equals(1)).count());
        Integer numberOfInvitationsWithNegativeFeedback = Math.toIntExact(guests.stream().filter(guest -> guest.getFeedback().equals(2)).count());
        Integer numberOfInvitationsWithPendingFeedback = guests.size() - numberOfInvitationsWithPositiveFeedback - numberOfInvitationsWithNegativeFeedback;
        Integer numberOfInviteesWithPositiveFeedback =
                guests.stream().filter(guest -> guest.getFeedback().equals(1))
                        .mapToInt(Guest::getNumberOfGuests)
                        .sum();

        Integer numberOfInviteesWithSeat = guests.stream()
                .filter(guest -> guest.getFeedback().equals(1) && guest.getWeddingTable() != null)
                .mapToInt(Guest::getNumberOfGuests)
                .sum();

        Integer numberOfInviteesWithoutSeat = numberOfInviteesWithPositiveFeedback - numberOfInviteesWithSeat;

        List<Todo> todos = wedding.getTodos();

        int numberOfCompletedTodos = Math.toIntExact(todos.stream()
                .filter(todo -> todo.getDone() != null && todo.getDone())
                .count());

        Integer numberOfUncompletedTodos = todos.size() - numberOfCompletedTodos;

        List<BookedService> bookedServices = wedding.getBookedServices();

        int numberOfAcceptedBookedServices = Math.toIntExact(bookedServices.stream()
                .filter(bookedService -> bookedService.getStatus().equals(Status.ACCEPTED))
                .count());

        Integer numberOfPendingBookedServices = bookedServices.size() - numberOfAcceptedBookedServices;

        return WeddingStatisticsDto.builder()
                .nameOfBride(wedding.getNameOfBride())
                .nameOfFiance(wedding.getNameOfFiance())
                .dateOfWedding(wedding.getDateOfWedding())
                .numberOfInvitations(guests.size())
                .numberOfInvitationsWithPositiveFeedback(numberOfInvitationsWithPositiveFeedback)
                .numberOfInvitationsWithNegativeFeedback(numberOfInvitationsWithNegativeFeedback)
                .numberOfInvitationsWithPendingFeedback(numberOfInvitationsWithPendingFeedback)
                .numberOfInviteesWithPositiveFeedback(numberOfInviteesWithPositiveFeedback)
                .numberOfInviteesWithSeat(numberOfInviteesWithSeat)
                .numberOfInviteesWithoutSeat(numberOfInviteesWithoutSeat)
                .numberOfCompletedTodos(numberOfCompletedTodos)
                .numberOfUncompletedTodos(numberOfUncompletedTodos)
                .numberOfAcceptedBookedServices(numberOfAcceptedBookedServices)
                .numberOfPendingBookedServices(numberOfPendingBookedServices)
                .build();
    }

    public void delete(Long id) {
        repository.findById(id).ifPresent(wedding -> repository.delete(wedding));
    }

    public Long findWeddingIdByUserId(Long userId) {
        return repository.findIdByUserId(userId);
    }

    //    TODOS
    public List<Todo> findAllTodos(Long weddingId) {
        Wedding wedding = repository.findById(weddingId).orElse(null);
        return (wedding != null) ? wedding.getTodos() : null;
    }

    //    WEDDING TABLES
    public WeddingTable createWeddingTable(Long weddingId, WeddingTable weddingTable) {
        Wedding wedding = repository.findById(weddingId).orElse(null);

        if (wedding != null) {
            WeddingTable createdWeddingTable = weddingTableService.save(weddingTable);

            List<WeddingTable> weddingTables = wedding.getTables();
            weddingTables.add(createdWeddingTable);
            repository.save(wedding);
            return createdWeddingTable;
        }
        return null;
    }
}
