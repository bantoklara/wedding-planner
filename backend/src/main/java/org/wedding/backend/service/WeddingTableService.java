package org.wedding.backend.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wedding.backend.model.Guest;
import org.wedding.backend.model.Wedding;
import org.wedding.backend.model.WeddingTable;
import org.wedding.backend.repository.WeddingTableRepository;

import java.util.List;

@Service
public class WeddingTableService {
    @Autowired
    private WeddingTableRepository repository;

    //    CREATE
    public WeddingTable save(WeddingTable table) {
        return repository.save(table);
    }

    //    READ
    public WeddingTable findById(Long id) {
        return repository.findById(id).orElse(null);
    }

    public List<WeddingTable> findByWeddingId(Long weddingId) {
        return repository.findByWeddingId(weddingId);
    }

    public Long findWeddingIdById(Long id) {
        return repository.findWeddingIdById(id);
    }

    public List<WeddingTable> getListOfTableIds(Long weddingId) {
        return repository.findIdByWeddingIdGroupById(weddingId);
    }

    //    UPDATE
    public WeddingTable update(Long id, WeddingTable weddingTable) {
        WeddingTable weddingTableToUpdate = repository.findById(id).orElse(null);
        if (weddingTableToUpdate != null) {
            weddingTable.setId(id);
            weddingTable.setWedding(weddingTableToUpdate.getWedding());
            weddingTable.setGuests(weddingTableToUpdate.getGuests());
            return repository.save(weddingTable);
        }
        return null;
    }

    // DELETE
    public Boolean delete(Long id) {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        }
        return false;
    }
}
