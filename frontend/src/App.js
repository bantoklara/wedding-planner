import './App.css';
import {
  Route,
  Routes,
  BrowserRouter
} from 'react-router-dom';
import GuestList from './Pages/GuestListPage';
import Home from './Pages/Home';
import GuestDetailsPage from './Pages/GuestDetailsPage';
import UserContext from './UserContext';
import { useMemo, useState } from 'react';
import RequireAuth from './Components/RequireAuth';
import { CookiesProvider } from 'react-cookie';
import TodoListPage from './Pages/TodoListPage';
import SeatingPlanPage from './Pages/SeatingPlanPage';
import ServiceCreationPage from './Pages/ServiceCreationPage';
import ServiceListPage from './Pages/ServiceListPage';
import ServiceDetailPage from './Pages/ServiceDetailPage';
import ServiceEditPage from './Pages/ServiceEditPage';
import BookedServiceListPage from './Pages/BookedServiceListPage';
import BookedServiceDetailPage from './Pages/BookedServiceDetailPage';
import Calendar from './Pages/Calendar';
import MainComponent from './Components/MainComponent';

function App() {
  const [user, setUser] = useState(null)
  const value = useMemo(() => ({ user, setUser }), [user, setUser]);

  return (
    <BrowserRouter>
      <CookiesProvider>
        <UserContext.Provider value={value}>
          <Routes>
            <Route path="/" element={<MainComponent />}>
              <Route index element={<Home />} />
              <Route path="/services/:id" element={<ServiceDetailPage />} />
              <Route path="/services" element={<ServiceListPage />} />
              <Route element={<RequireAuth allowedRole={['ROLE_SERVICE_PROVIDER']} />}>
                <Route path="/new-service" element={<ServiceCreationPage />} />
                <Route path="/services/:id/edit" element={<ServiceEditPage />} />
                <Route path="/calendar" element={<Calendar />} />
              </Route>
              <Route element={<RequireAuth allowedRole={['ROLE_COUPLE']} />}>
                <Route path="guests" element={<GuestList />} />
                <Route path="guests/:id" element={<GuestDetailsPage />} />
                <Route path="todos" element={<TodoListPage />} />
                <Route path="seating-plan" element={<SeatingPlanPage />} />
              </Route>
              <Route element={<RequireAuth allowedRole={['ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER']} />}>
                <Route path="/booked-services" element={<BookedServiceListPage />} />
                <Route path="/booked-services/:id/*" element={<BookedServiceDetailPage />} />
              </Route>
            </Route>
          </Routes>
        </UserContext.Provider>
      </CookiesProvider>
    </BrowserRouter>
  );
}

export default App;
