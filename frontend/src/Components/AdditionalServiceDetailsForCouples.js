import React, { useState, useEffect } from 'react'
import { Box, Button, Typography } from '@mui/material';

import HandshakeIcon from '@mui/icons-material/Handshake';
import HourglassTopIcon from '@mui/icons-material/HourglassTop';
import DoneAllIcon from '@mui/icons-material/DoneAll';

import useBookedServiceService from '../Services/BookedServiceService';
import Warning from './Warning';

const AdditionalServiceDetailsForCouples = ({ id }) => {
    const [reserved, setReserved] = useState(null);
    const { isReserved, bookService } = useBookedServiceService();
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(-1);

    const reservationStatus = {
        ACCEPTED: 'Ön már lefoglalta ezt a szolgáltatást',
        PENDING: 'Ön már lefoglalta ezt a szolgáltatást. A szolgáltató válaszára várunk.',
        NOT_FOUND: 'Szolgáltatás igénybevétele'
    };

    useEffect(() => {
        const apiCall = async () => {
            const bookedService = await isReserved(id);
            setReserved(bookedService ? bookedService.status : 'NOT_FOUND');
        };

        apiCall();
    }, []);

    const reserveService = async () => {
        setLoading(1);
        const success = await bookService(id);
        if (success) {
            setReserved('PENDING');
        }
        setLoading(0);
    };

    const triggerPopup = () => {
        setLoading(-1);
        setButtonPopup(true);
    };

    return (
        <>
            {reserved
                ? <>
                    {reserved === 'ACCEPTED' &&
                        <Box display='flex' alignItem='center'>
                            <DoneAllIcon />
                            <Typography>{reservationStatus.ACCEPTED}</Typography>
                        </Box>
                    }
                    {reserved === 'PENDING' &&
                        <Box display='flex' alignItems='center'>
                            <HourglassTopIcon />
                            <Typography>{reservationStatus.PENDING}</Typography>
                        </Box>
                    }
                    {reserved === 'NOT_FOUND' &&
                        <>
                            <Button
                                color='secondary'
                                variant='contained'
                                sx={{ color: '#ffffff', width: 'fit-content' }}
                                startIcon={<HandshakeIcon />}
                                onClick={() => triggerPopup()}>{reservationStatus.NOT_FOUND}</Button>
                            <Warning
                                dialogOpen={buttonPopup}
                                setDialogOpen={setButtonPopup}
                                message='Ha lefoglalja a szolgáltatást az automatikusan megjelenik a szolgáltatónak. Előzőleg vegye fel a szolgáltatóval a kapcsolatot!'
                                cancelMessage='Értettem'
                                continueMessage='Szolgáltatás igénybevétele'
                                action={reserveService}
                                loading={setLoading}
                            />
                        </>}
                </>
                : <div>...</div>
            }
        </>

    )
}

export default AdditionalServiceDetailsForCouples
