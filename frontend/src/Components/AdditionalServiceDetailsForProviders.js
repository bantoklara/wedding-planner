import React from 'react'
import { useNavigate } from 'react-router-dom';

import { Box, Button } from '@mui/material';

import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import { useWeddingServiceService } from '../Services/WeddingServiceService'

const AdditionalServiceDetailsForProviders = ({ service }) => {
    const navigate = useNavigate();
    const { deleteService } = useWeddingServiceService();

    const deleteServiceById = async () => {
        const deleted = deleteService(service.id);
        if (deleted)
            navigate('/');
    };

    return (
        <Box display='flex' gap={2}>
            <Button
                color='secondary'
                variant='contained'
                sx={{ color: '#ffffff' }}
                startIcon={<EditIcon />}
                onClick={() => navigate(`/services/${service.id}/edit`)}
            >Módosítás</Button>
            <Button
                color='error'
                variant='outlined'
                sx={{ color: '#f44336' }}
                startIcon={<DeleteIcon />}
                onClick={deleteServiceById}
            >Törlés</Button>
        </Box>
    )
}

export default AdditionalServiceDetailsForProviders
