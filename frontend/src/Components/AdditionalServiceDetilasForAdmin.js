import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

import { Box, Button } from '@mui/material'

import DeleteIcon from '@mui/icons-material/Delete';
import DoneAllIcon from '@mui/icons-material/DoneAll';

import { useWeddingServiceService } from '../Services/WeddingServiceService';

const AdditionalServiceDetailsForAdmin = ({ service }) => {
    const [enableLoading, setEnableLoading] = useState(false);
    const [deleteLoading, setDeleteLoading] = useState(false);
    const navigate = useNavigate();
    const { enableService, deleteService } = useWeddingServiceService();

    const enableHandler = async () => {
        setEnableLoading(true);
        const success = await enableService(service.id);
        navigate("/services");
        setEnableLoading(false);
    };

    const deleteHandler = async () => {
        setDeleteLoading(true);
        const success = await deleteService(service.id);
        navigate("/sevices");
        setDeleteLoading(false);
    };

    return (
        <Box display='flex' gap={2}>
            {!service.enabled &&
                <Button
                    color='secondary'
                    variant='contained'
                    sx={{ color: '#ffffff' }}
                    startIcon={<DoneAllIcon />}
                    onClick={enableHandler}>Szolgáltatás jóváhagyása {enableLoading && '...'}</Button>
            }
            <Button
                color='error'
                variant='outlined'
                sx={{ color: '#f44336' }}
                startIcon={<DeleteIcon />}
                onClick={deleteHandler}>Törlés {deleteLoading && '...'}</Button>
        </Box>
    )
}

export default AdditionalServiceDetailsForAdmin
