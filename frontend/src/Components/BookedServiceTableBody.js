import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';

import {
    TableRow,
    TableCell,
    Button,
    TextField,
    Box,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    FormLabel
} from '@mui/material'

import DeleteIcon from '@mui/icons-material/Delete';
import DoneAllIcon from '@mui/icons-material/DoneAll';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

import useBookedServiceService from '../Services/BookedServiceService';

const BookedServiceTableBody = ({ bookedServices = [], status, role }) => {
    const [price, setPrice] = useState('');
    const [loading, setLoading] = useState(false);
    const [buttonPopup, setButtonPopup] = useState(false);
    const navigate = useNavigate();
    const { update, deleteBookedService } = useBookedServiceService();

    const setRows = (id, wedding, service) => {
        if (role === 'ROLE_COUPLE') {
            return [
                { align: 'left', content: `${service.user.lastName} ${service.user.firstName}` },
                { align: 'left', content: `${service.user.email}` },
                { align: 'left', content: <Button onClick={() => navigate(`/services/${service.id}`)}>{service.name}</Button> },
                { align: 'left', content: `${service.category.name}` },
                {
                    align: 'right', content:
                        status === 'PENDING'
                            ? ''
                            : <Button
                                color='primary'
                                variant='contained'
                                endIcon={<ChevronRightIcon />}
                                onClick={() => navigate(`/booked-services/${id}`)}
                            >Megtekint</Button>
                }
            ]
        }

        return [
            { align: 'left', content: `${wedding.user.lastName} ${wedding.user.firstName}` },
            { align: 'left', content: `${wedding.user.email}` },
            { align: 'left', content: `${wedding.dateOfWedding}` },
            { align: 'left', content: <Button onClick={() => navigate(`/services/${service.id}`)}>{service.name}</Button> },
            { align: 'left', content: `${service.category.name}` },
            {
                align: 'right',
                content:
                    <>
                        {status === 'PENDING'
                            ? <Box display='flex' gap={1}>
                                <Button
                                    color='secondary'
                                    variant='contained'
                                    sx={{ color: '#ffffff' }}
                                    startIcon={<DoneAllIcon />}
                                    onClick={() => setButtonPopup(true)}>Elfogad</Button>
                                <>{popupForm(id)}</>
                                <Button
                                    color='error'
                                    variant='outlined'
                                    sx={{ color: '#f44336' }}
                                    startIcon={<DeleteIcon />}
                                    onClick={() => deleteBookedService(id)}
                                >Elutasit</Button>
                            </Box>
                            : <Button
                                color='primary'
                                variant='contained'
                                endIcon={<ChevronRightIcon />}
                                onClick={() => navigate(`/booked-services/${id}`)}
                            >Megtekint</Button>
                        }</>
            },
        ]
    }
    const popupForm = (id) => {
        return (
            <Dialog open={buttonPopup} onClose={() => setButtonPopup(false)}>
                <DialogTitle>Kérvényezés elfogadása</DialogTitle>
                <DialogContent>
                    <Box display='flex' alignItems='center'>
                        <FormLabel>Adja meg a szolgáltatásért kért összeget lejben</FormLabel>
                        <TextField
                            type='number'
                            variant='standard'
                            value={price}
                            onChange={(e) => { setPrice(e.target.value) }}
                        />
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button
                        variant='contained'
                        color='secondary'
                        sx={{ color: '#ffffff' }}
                        onClick={(e) => acceptBooking(id)}
                    >Elfogadás {loading && '...'}</Button>
                </DialogActions>
            </Dialog>
        )
    }

    const acceptBooking = async (id) => {
        setLoading(true);
        const success = await update(id, { status: 'ACCEPTED', price: price });
        if (success) {
            setButtonPopup(false);
        }
        setLoading(false);
    };

    return (
        <>
            {
                bookedServices.map(({ id, wedding, service }, index) => {
                    return (
                        <TableRow key={index}>
                            {setRows(id, wedding, service).map(({ align, content }, i) => {
                                return (
                                    <TableCell key={i} align={align}>{content}</TableCell>
                                )
                            })}
                        </TableRow>
                    )
                })
            }
        </>
    )
}

export default BookedServiceTableBody
