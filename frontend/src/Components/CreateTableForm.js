import React, { useState } from 'react'

import { Box, Button, Dialog, DialogContent, DialogTitle, FormLabel, Stack, TextField } from '@mui/material';

import useTableService from '../Services/TableService';

const CreateTableForm = ({ dispatch, dialogOpen, setDialogOpen }) => {
    const [loading, setLoading] = useState(false);
    const [values, setValues] = useState({
        name: '',
        numberOfSeats: ''
    });
    const { createTable } = useTableService();

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setValues({ ...values, [name]: value });
    };

    const submitFormHandler = async (event) => {
        event.preventDefault();
        setLoading(true);
        const table = await createTable(values);
        if (table) {
            dispatch({
                type: "CREATE",
                id: table.id.toString(),
                name: table.name,
                numberOfGuests: 0,
                numberOfSeats: table.numberOfSeats,
                guests: []
            }
            )
            setDialogOpen(false);
        }
        setLoading(false);
    }

    return (
        <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)} fullWidth maxWidth='xs'>
            <DialogTitle textAlign='center'>
                <h3>Assztal létrehozása</h3>
            </DialogTitle>
            <DialogContent sx={{ width: '70%', margin: 'auto', textAlign: 'center', justifyContent: 'center' }}>
                <form onSubmit={submitFormHandler}>
                    <Stack spacing={2}>
                        <TextField
                            required
                            label="Asztal megnevezése"
                            name="name"
                            variant='standard'
                            value={values.name}
                            onChange={inputChangeHandler}
                            fullWidth
                        />
                        <Box display='flex' gap={2} alignItems='center'>
                            <FormLabel>Székek száma</FormLabel>
                            <TextField
                                required
                                type="number"
                                InputProps={{ inputProps: { min: 2, max: 16 } }}
                                name="numberOfSeats"
                                value={values.numberOfSeats}
                                onChange={inputChangeHandler}
                            />
                        </Box>
                        <Button
                            variant='contained'
                            color='secondary'
                            sx={{ color: '#ffffff', width: 'fit-content' }}
                            type='submit'
                        >Asztal létrehozása {loading && '...'}</Button>
                    </Stack>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default CreateTableForm
