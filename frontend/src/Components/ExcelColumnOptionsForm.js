import React, { useState } from 'react'

import {
    Button,
    Checkbox,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    FormControlLabel,
    Grid
} from '@mui/material'

import { useGuestService } from '../Services/GuestService';

const ExcelColumnOptionsForm = ({ dialogOpen, setDialogOpen }) => {
    const [values, setValues] = useState({ columns: new Set() });
    const [loading, setLoading] = useState(false);
    const { getGuestListInExcel } = useGuestService();

    const inputChangeHandler = (event) => {
        const tmp = values.columns;
        const { value, checked } = event.target;
        if (checked) {
            tmp.add(value);
        } else tmp.delete(value);
        setValues({ ...values, columns: tmp });
    };

    const submitFormHandler = async (event) => {
        setLoading(true);
        event.preventDefault();
        const requestParams = [];
        values.columns.forEach((column) => {
            requestParams.push(column);
        });
        const url = await getGuestListInExcel(requestParams);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', 'vendeglista.xlsx');
        document.body.appendChild(link);
        link.click();
        link.remove();
        setDialogOpen(false);
    }

    const checkBoxValues = [
        [{
            value: 'lastName',
            label: 'Vezetéknév'
        },
        {
            value: 'firstName',
            label: 'Keresztnév'
        },
        {
            value: 'feedback',
            label: 'Visszajelzés'
        },
        {
            value: 'numberOfGuests',
            label: 'Személyek száma'
        }],
        [{
            value: 'additionalMessage',
            label: 'További üzenetek'
        },
        {
            value: 'loginCode',
            label: 'Bejelentkező kód'
        },
        {
            value: 'menus',
            label: 'Menük'
        },
        {
            value: 'weddingTable',
            label: 'Asztal'
        }],
    ]
    return (
        <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)} >
            <DialogTitle>Vendéglista letöltése Excel fájlba</DialogTitle>
            <DialogContent>
                <h4>Válassza ki mely információk jelenjenek meg a letöltött fájlba</h4>
                <form onSubmit={submitFormHandler}>
                    <div>
                        <FormControl>
                            <Grid container direction='column'>

                                {checkBoxValues.map((checkboxGroup, index) => (
                                    <Grid item xs={12} md={6} key={index}>
                                        {checkboxGroup.map((checkbox, index2) => (
                                            <FormControlLabel
                                                key={index2}
                                                control={
                                                    <Checkbox
                                                        checked={values.columns.has(checkbox.value)}
                                                        value={checkbox.value}
                                                        onChange={inputChangeHandler}
                                                    />
                                                }
                                                label={checkbox.label}
                                            />
                                        ))}
                                    </Grid>
                                )
                                )}
                            </Grid>
                        </FormControl>
                    </div>
                    <Button
                        variant='contained'
                        color='secondary'
                        sx={{ color: '#ffffff' }}
                        type='submit'>Letöltés {loading && '...'}
                    </Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default ExcelColumnOptionsForm
