import { useState } from "react";

import { Box, Button, MenuItem, Paper, TextField, } from "@mui/material";

import AddIcon from '@mui/icons-material/Add';
import DeleteIcon from '@mui/icons-material/Delete';

import { useFormHandlers } from "../Services/FormHandlers/GuestCreationFormHandler";

const WeddingGuestCreationForm = ({ guestGroups = [], setCount }) => {
    const [numberOfNewRows, setNumberOfNewRows] = useState(5);
    const {
        errors,
        inputFields,
        addNewField,
        removeField,
        inputChangedHandler,
        submitFormHandler
    } = useFormHandlers(guestGroups, setCount)

    const inputFieldValues = [
        {
            label: "Vezetéknév",
            name: "lastName"
        },
        {
            label: "Keresztknév",
            name: "firstName"
        }
    ];

    const selectOptionValues = [
        {
            value: 1,
            text: "és családja"
        },
        {
            value: 2,
            text: "és barátja"
        },
        {
            value: 3,
            text: "és barátnője"
        },
        {
            value: 4,
            text: "egyedül"
        }
    ];

    const createNewFields = () => {
        return (
            <Paper>
                <TextField
                    type="number"
                    name="number-of-new-rows"
                    value={numberOfNewRows}
                    style={{ width: "70px" }}
                    size="small"
                    variant="outlined"

                    onChange={(e) => setNumberOfNewRows(e.target.value)}
                />
                <Button onClick={() => addNewField(numberOfNewRows)}><AddIcon fontSize="large" color="secondary" /></Button>
                <br />
            </Paper>
        );
    };

    return (
        <>
            <form onSubmit={(e) => submitFormHandler(e)}>
                {inputFields.length === 0 && createNewFields()}
                {inputFields.map((values, index) => (
                    < Box key={index} display='flex' alignItems='center'>
                        {
                            inputFieldValues.map((inputFieldValue) => (
                                <TextField
                                    key={inputFieldValue.name}
                                    name={inputFieldValue.name}
                                    value={values[inputFieldValue.name]}
                                    required
                                    label={inputFieldValue.label}
                                    size='small'
                                    variant="outlined"
                                    onChange={(e) => inputChangedHandler(e, index)}
                                    {...(errors[index][inputFieldValue.name] && {
                                        error: true,
                                        helperText: errors[index][inputFieldValue.name]
                                    })}
                                />
                            ))
                        }
                        < TextField
                            select
                            key="invitedWithGuest"
                            name="invitedWithGuest"
                            value={values.invitedWithGuest || ''}
                            label="További meghívott"
                            size='small'
                            style={{ minWidth: "200px" }}
                            variant="outlined"
                            onChange={(e) => inputChangedHandler(e, index)}
                        >
                            {selectOptionValues.map((option) => (
                                <MenuItem
                                    value={option.value}
                                    key={option.value}
                                >
                                    {option.text}
                                </MenuItem>
                            ))}
                        </TextField>
                        <TextField
                            select
                            size='small'
                            style={{ minWidth: "250px" }}
                            label="Vendég csoport"
                            id="select-guest-group"
                            name="guestGroupIndex"
                            key="guestGroupIndex"
                            variant="outlined"
                            defaultValue=''
                            value={values.guestGroupIndex || ''}
                            onChange={(e) => inputChangedHandler(e, index)}
                        >
                            {guestGroups.map((group, i) => (
                                <MenuItem
                                    value={i}
                                    key={i}
                                >
                                    {group.name}
                                </MenuItem>
                            ))}
                        </TextField>
                        <Button
                            size='medium'
                            onClick={() => removeField(index)}>
                            <DeleteIcon color='error' />
                        </Button>
                        {index + 1 === inputFields.length && createNewFields()}
                    </Box>
                ))}

                <Button variant="contained" color="secondary" sx={{ color: '#ffffff' }} type="submit">
                    Vendégek hozzáadása
                </Button>
            </form>
        </>
    )
};

export default WeddingGuestCreationForm;