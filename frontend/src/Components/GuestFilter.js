import React from 'react'

import { FormControlLabel, RadioGroup, Radio, InputAdornment, TextField, Box, Typography } from '@mui/material';

import SearchIcon from '@mui/icons-material/Search';
import PersonIcon from '@mui/icons-material/Person';
import CheckIcon from '@mui/icons-material/Check';
import useStyles from '../Styles/GuestFilterStyle';

const GuestFilter = (props) => {
    const { values, inputChangedHandler } = props;
    const classes = useStyles();

    const setLabel = (text, size) => {
        return (
            <Box sx={{ display: 'flex' }}>
                <Typography>{text} {size}</Typography>
                <PersonIcon color='primary' />
            </Box>
        )
    };

    return (
        <>
            <RadioGroup
                sx={{ marginLeft: '15px' }}
                row
                name="feedback"
                value={values.feedback}
                onChange={inputChangedHandler}
            >
                <FormControlLabel
                    value=''
                    className={`${classes.formControlLabel} ${values.feedback === '' ? classes.active : classes.inactive}`}
                    control={<Radio checkedIcon={<CheckIcon color='primary' />} />}
                    label={setLabel('Összesen', props.numberOfAllGuests)} />
                <FormControlLabel
                    value={1}
                    className={`${classes.formControlLabel} ${values.feedback === '1' ? classes.active : classes.inactive}`}
                    control={<Radio checkedIcon={<CheckIcon color='primary' />} />}
                    label={setLabel('Ott lesz', props.numberOfGuestWithFeedback1)}
                />
                <FormControlLabel
                    value={2}
                    className={`${classes.formControlLabel} ${values.feedback === '2' ? classes.active : classes.inactive}`}
                    control={<Radio checkedIcon={<CheckIcon color='primary' />} />}
                    label={setLabel('Nem lesz ott',
                        props.numberOfGuestWithFeedback2)}
                />
                < FormControlLabel
                    value={3}
                    className={`${classes.formControlLabel} ${values.feedback === '3' ? classes.active : classes.inactive}`}
                    control={<Radio checkedIcon={<CheckIcon color='primary' />} />}
                    label={setLabel('Nem jelzett vissza', props.numberOfGuestWithFeedback3)}
                />
            </RadioGroup>
            <TextField
                InputProps={{
                    startAdornment: (
                        <InputAdornment position="start">
                            <SearchIcon />
                        </InputAdornment>
                    ),
                    endAdornment: (
                        <InputAdornment position='end'>
                        </InputAdornment>
                    )
                }}
                placeholder="keresés név szerint"
                value={values.name}
                size='small'
                sx={{ marginBottom: '10px' }}
                name="name"
                onChange={inputChangedHandler}
            />
        </>
    )
}

export default GuestFilter
