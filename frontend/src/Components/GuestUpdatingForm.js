import { useContext } from "react";

import {
    Box,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    FormLabel,
    MenuItem,
    Paper,
    Radio,
    RadioGroup,
    TextField
} from "@mui/material";
import { useFormHandlers } from "../Services/FormHandlers/GuestUpdatingFormHandler";
import UserContext from "../UserContext";

const GuestUpdatingForm = ({ guest, setGuest, guestGroups, menus, withRedirect = false }) => {
    const { user } = useContext(UserContext);
    const {
        values,
        errors,
        sent,
        inputChangedHandler,
        menuCheckChangeHandler,
        menuAmountChangeHandler,
        submitFormHandler
    } = useFormHandlers(user, guest, setGuest, menus, withRedirect);

    const invitedWithGuestOptions = [
        {
            value: 1,
            text: "és családja"
        },
        {
            value: 2,
            text: "és barátja"
        },
        {
            value: 3,
            text: "és barátnője"
        },
        {
            value: 4,
            text: "egyedül"
        }
    ];

    const formatDate = (date = new Date()) => {
        return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
    }

    return (
        <div>
            {user.role === 'ROLE_COUPLE' &&
                <Paper>
                    <h3>Vendég információi</h3>
                    <TextField
                        label="Vezetéknév"
                        name="lastName"
                        value={values.lastName}
                        onChange={inputChangedHandler}
                        {...(errors.lastName && {
                            error: true,
                            helperText: errors.lastName
                        })}
                    />
                    <TextField
                        label="Keresztnév"
                        name="firstName"
                        value={values.firstName}
                        onChange={inputChangedHandler}
                        {...(errors.firstName && {
                            error: true,
                            helperText: errors.firstName
                        })}
                    />
                    <TextField
                        select
                        type="number"
                        name="invitedWithGuest"
                        label="További meghívott"
                        value={values.invitedWithGuest}
                        onChange={inputChangedHandler}
                    >
                        {invitedWithGuestOptions.map((option) => (
                            <MenuItem
                                value={option.value}
                                key={option.value}
                            >
                                {option.text}
                            </MenuItem>
                        ))}
                    </TextField>
                    <TextField
                        select
                        defaultValue={''}
                        size='small'
                        sx={{ marginRight: "10px", minWidth: "250px" }}
                        label="Vendég csoport"
                        id="select-guest-group"
                        name="guestGroupId"
                        key="guestGroupId"
                        variant="outlined"
                        value={values.guestGroupId}
                        onChange={inputChangedHandler}
                    >
                        {guestGroups.map((group) => (
                            <MenuItem
                                value={group.id}
                                key={group.id}
                            >
                                {group.name}
                            </MenuItem>
                        ))}
                    </TextField>
                </Paper>
            }
            <Box>
                <h2> Meghívó < i > {guest.lastName} {guest.firstName}</i > részére</h2 >
                <h4 style={{ float: 'right' }}>{
                    guest.lastFeedbackDate
                        ? <i>Utolsó visszajelzés: {formatDate(new Date(guest.lastFeedbackDate))}</i>
                        : <i>Még nem jelzett vissza</i>}
                </h4>
                <h3>Visszajelzés</h3>

                <RadioGroup
                    row
                    name="feedback"
                    value={values.feedback}
                    onChange={inputChangedHandler}
                >
                    <FormControlLabel value={1} control={<Radio />} label="Ott leszek" />
                    <FormControlLabel value={2} control={<Radio />} label="Nem leszek ott" />
                    <FormControlLabel value={3} control={<Radio />} label="Még nem tudom" />
                </RadioGroup>
                <Box display='flex' alignItems='center'>
                    <FormLabel sx={{ marginRight: "10px" }}>Személyek száma</FormLabel>
                    <TextField
                        sx={{ width: "70px" }}
                        size="small"
                        type="number"
                        InputProps={{ inputProps: { min: 1 } }}
                        name="numberOfGuests"
                        value={values.numberOfGuests}
                        onChange={inputChangedHandler}
                        disabled={values.feedback != 1}
                    />
                </Box>
                <h4>Menü</h4>
                <FormControl
                >
                    {menus.map((menu) => (
                        <div key={menu.id}>
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={values.guestMenusMap.has(menu.id)}
                                        onChange={(event) => menuCheckChangeHandler(event, menu.id)}
                                        disabled={values.feedback != 1}
                                    />
                                }
                                label={menu.name}
                                sx={{ width: "200px" }}
                            />
                            <TextField
                                size="small"
                                type="number"
                                sx={{ width: "70px", height: "10px" }}
                                value={values.guestMenusMap.has(menu.id) ? values.guestMenusMap.get(menu.id) : 0}
                                onChange={(event) => menuAmountChangeHandler(event, menu.id)}
                                disabled={values.feedback != 1 || !values.guestMenusMap.has(menu.id)}
                                name='guestMenus'
                            />
                            <br />
                        </div>
                    ))}
                </FormControl>
                <br /><br />
                <FormLabel>További megjegyzés</FormLabel>
                <br />
                <TextField
                    name="additionalMessage"
                    value={values.additionalMessage}
                    multiline
                    rows={2}
                    onChange={inputChangedHandler}
                />
                <br />
                <Button
                    variant="contained"
                    color="secondary"
                    sx={{ color: '#ffffff', marginTop: "20px" }}
                    endIcon={sent && '...'}
                    onClick={submitFormHandler}
                >Küldés</Button>
                {errors.menus &&
                    <FormHelperText
                        sx={{ color: 'red' }}
                    >
                        {errors.menus}
                    </FormHelperText>
                }
            </Box >
        </div>
    )
};

export default GuestUpdatingForm;