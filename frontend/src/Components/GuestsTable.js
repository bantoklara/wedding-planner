import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { Button, TableCell, TableRow, Typography } from '@mui/material';

import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';

import { useGuestService } from '../Services/GuestService';
import useStyles from '../Styles/GuestTableStyle';
import SortedTableWithPagination from './SortedTableWithPagination';
import Warning from './Warning';

const WeddingGuestsTable = (props) => {
    const [hover, setHover] = React.useState(false);
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(-1);
    const [guestToDelete, setGuestToDelete] = useState(null);
    const { deleteGuest, updateGuestFeedback } = useGuestService();
    const classes = useStyles();
    const navigate = useNavigate();

    const headers = [
        {
            name: "guestName",
            label: "Meghívott",
            algin: "left",
            width: '40%',
            sortable: true
        },
        {
            name: "feedback",
            label: "Visszajelzés",
            align: "center",
            width: '20%',
            sortable: true
        },
        {
            name: "numberOfGuests",
            label: "Személyek száma",
            align: "center",
            width: '20%',
            sortable: true
        },
        {
            name: "actionButtons",
            label: "",
            align: "right",
            width: '20%',
            sortable: false
        }];

    const updateFeedback = async (id, currentFeedback, newFeedback) => {
        if (currentFeedback !== newFeedback) {
            await updateGuestFeedback(id, { feedback: newFeedback });
            props.setCount(count => count + 1);
        }
    }

    const setFeedbackIcon = (id, feedback, onHover = false) => {
        const classNames = {};
        switch (feedback) {
            case 1:
                if (!onHover)
                    return <CheckCircleOutlineIcon color='secondary' />
                classNames.yes = classes.activeFeedback;
                classNames.no = classes.inactiveFeedback;
                classNames.idk = classes.inactiveFeedback;
                break;
            case 2:
                if (!onHover)
                    return <HighlightOffIcon color='error' />
                classNames.yes = classes.inactiveFeedback;
                classNames.no = classes.activeFeedback;
                classNames.idk = classes.inactiveFeedback;
                break;
            default:
                if (!onHover)
                    return <HelpOutlineIcon sx={{ color: '#bc804d' }} />
                classNames.yes = classes.inactiveFeedback;
                classNames.no = classes.inactiveFeedback;
                classNames.idk = classes.activeFeedback;
                break;
        }
        return <>
            <CheckCircleOutlineIcon color='secondary' className={classNames.yes} onClick={() => updateFeedback(id, feedback, 1)} />
            <HighlightOffIcon color='error' className={classNames.no} onClick={() => updateFeedback(id, feedback, 2)} />
            <HelpOutlineIcon sx={{ color: '#bc804d' }} className={classNames.idk} onClick={() => updateFeedback(id, feedback, 3)} />
        </>
    };

    const setInvitedWithGuest = (id) => {
        switch (parseInt(id, 10)) {
            case 1:
                return "és családja"
            case 2:
                return "és barátja"
            case 3:
                return "és barátnője"
            default:
                return ""
        }
    };

    const deleteGuestHandler = async ({ guestId }) => {
        setLoading(1);
        await deleteGuest(guestId);
        props.setCount(count => count + 1);
        setLoading(0);
    }

    const triggerPopup = (guestId) => {
        setGuestToDelete(guestId);
        setLoading(-1);
        setButtonPopup(true);
    }

    return (
        <>
            <Warning
                dialogOpen={buttonPopup}
                setDialogOpen={setButtonPopup}
                message='Biztosan törli a vendéget?'
                cancelMessage='Mégsem'
                continueMessage='Vendég törlése'
                action={deleteGuestHandler}
                params={{ guestId: guestToDelete }}
                loading={loading}
            />
            <Typography className={classes.guestCounter}>{props.numberOfFilteredGuests}</Typography>
            <SortedTableWithPagination
                headers={headers}
                values={props.values}
                sortByChangeHandler={props.sortByChangeHandler}
                pagingChangedHandler={props.pagingChangedHandler}
                orderHasChanged={props.orderHasChanged}
                size={props.numberOfFilteredGuests}
            >
                {props?.guests?.map((guest) => (
                    <TableRow
                        id={guest.id}
                        className={classes.row}
                        key={guest.id}
                        hover
                    >
                        <TableCell align="left" component="th" >
                            {`${guest.lastName} ${guest.firstName} ${setInvitedWithGuest(guest.invitedWithGuest)}`}
                        </TableCell>
                        <TableCell
                            align="center"
                            component="th"
                            onMouseEnter={() => setHover(true)}
                            onMouseLeave={() => setHover(false)}
                        >
                            {setFeedbackIcon(guest.id, parseInt(guest.feedback), hover)}
                        </TableCell>
                        <TableCell align="center" component="th">{guest.numberOfGuests}</TableCell>
                        <TableCell align="right">
                            <Button onClick={() => { navigate(`/guests/${guest.id}`) }} ><EditIcon color='primary' /></Button>
                            <Button onClick={() => { triggerPopup(guest.id) }} ><DeleteIcon color='error' /></Button>
                        </TableCell>
                    </TableRow>
                ))}
            </SortedTableWithPagination>
        </>

    );
};

export default WeddingGuestsTable;