import { useContext, useEffect, useState } from "react";
import { useCookies } from "react-cookie";
import { Outlet } from "react-router-dom";

import { Container } from "@mui/material";
import { ThemeProvider } from "@mui/material/styles";

import theme from "../CustomTheme";
import AuthenticationHandler from "../Services/AuthenticationService";
import UserContext from "../UserContext";
import LoginForm from "../Pages/Login";
import Register from "../Pages/Register";
import Navbar from "./NavigationBar";

const MainComponent = () => {
    const [cookies] = useCookies(['auth']);
    const { getUserInfoFromToken } = AuthenticationHandler();
    const { user, setUser } = useContext(UserContext);

    const [loginPopup, setLoginPopup] = useState(false);
    const [registerPopup, setRegisterPopup] = useState(false);

    useEffect(() => {
        if (cookies.token) {
            setUser(getUserInfoFromToken(cookies.token));
        }
    }, [])

    return (
        <ThemeProvider theme={theme}>
            <header>
                <nav>
                    <Navbar setLoginPopup={setLoginPopup} setRegisterPopup={setRegisterPopup} user={user} setUser={setUser} />
                </nav>
            </header>
            <main className="App">
                <LoginForm dialogOpen={loginPopup} setDialogOpen={setLoginPopup} />
                <Register dialogOpen={registerPopup} setDialogOpen={setRegisterPopup} />
                <Container sx={{ marginTop: '20px' }}>
                    <Outlet />
                </Container>
            </main>
        </ThemeProvider>
    )
}

export default MainComponent;

