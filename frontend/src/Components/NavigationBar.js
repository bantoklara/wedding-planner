import React, { useState } from 'react'
import { NavLink } from 'react-router-dom';

import {
    AppBar,
    Avatar,
    Box,
    Button,
    Container,
    IconButton,
    Menu,
    MenuItem,
    Toolbar,
    Typography
} from '@mui/material'

import MenuIcon from '@mui/icons-material/Menu';

import { navigationMenus } from '../utils/navigationMenus';
import AuthenticationHandler from '../Services/AuthenticationService';
import NotificationComponent from './NotificationComponent';
import useStyles from '../Styles/NavigationBarStyle';

const Navbar = ({ setLoginPopup, setRegisterPopup, user, setUser }) => {

    const { logout } = AuthenticationHandler();

    const classes = useStyles();

    const [anchorElNav, setAnchorElNav] = useState(null);
    const [anchorElUser, setAnchorElUser] = useState(null);

    const handleOpenNavMenu = (event) => {
        setAnchorElNav(event.currentTarget);
    };

    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloeNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloeUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleLogout = () => {
        setUser(null);
        setAnchorElUser(null);
        logout();
    }

    return (
        <AppBar sx={{ padding: 0 }}>
            <Container maxWidth='xl'>
                <Toolbar variant='dense'>
                    <NavLink to='/' className={classes.navLink}>
                        <Typography
                            variant='h1'
                            noWrap
                            sx={{
                                mr: 2,
                                display: { xs: 'none', md: 'flex' },
                            }}
                        >Esküvő szervező
                        </Typography>
                    </NavLink>

                    {/* For mobile device */}
                    <Box sx={{ flexGrow: 1, display: { xs: 'flex', md: 'none' } }}>
                        <IconButton onClick={handleOpenNavMenu}>
                            <MenuIcon />
                        </IconButton>
                        <Menu
                            anchorEl={anchorElNav}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left'
                            }}
                            open={Boolean(anchorElNav)}
                            onClose={handleCloeNavMenu}
                            sx={{ display: { xs: 'block', md: 'none' } }}
                        >
                            {navigationMenus.filter(navigation => !navigation.private || (navigation.role.includes(user?.role))).map((nav, index) => (
                                <MenuItem key={index}>
                                    <NavLink to={nav.to}>
                                        <Typography vairant='h2'>{nav.label}</Typography>
                                    </NavLink>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                    <Typography
                        variant='h1'
                        noWrap
                        component='a'
                        href='/'
                        sx={{
                            mr: 2,
                            display: { xs: 'flex', md: 'none' },
                            flexGrow: 1
                        }}
                    >
                        Esküvő szervező
                    </Typography>
                    {/* For Desktop device */}
                    <Box sx={{ flexGrow: 1, display: { xs: 'none', md: 'flex' }, height: '100%' }} alignItems='center'>
                        {navigationMenus.filter(navigation => !navigation.private || (navigation.role.includes(user?.role))).map((nav, index) => (
                            <NavLink key={ index } to={nav.to} className={classes.navLinkWithMargin}>
                                <Typography variant="h2">{nav.label}</Typography>
                            </NavLink>
                        ))}
                    </Box>

                    {/* User informations */}
                    {user
                        ?
                        <Box sx={{ flexGrow: 0, display: 'flex', alignItems: 'center' }}>
                            {user.role !== 'ROLE_WEDDING_GUEST' && <NotificationComponent user={user} />}
                            <IconButton onClick={handleOpenUserMenu}>
                                <Avatar sx={{ bgcolor: 'secondary.main', width: '30px', height: '30px', padding: '5px' }}>
                                    {user.name.split(' ')[0].charAt(0)}{user.name.split(' ')[1].charAt(0)}
                                </Avatar>
                            </IconButton>
                            <Menu
                                anchorEl={anchorElUser}
                                anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left'
                                }}
                                open={Boolean(anchorElUser)}
                                onClose={handleCloeUserMenu}
                            >
                                <Typography variant="h2">{user.name}</Typography>
                                <Button onClick={handleLogout}>Kijelentkezés</Button>
                            </Menu>
                        </Box>
                        : <Box sx={{ flexGrow: 0, display: 'flex' }}>
                            <Button onClick={() => setLoginPopup(true)} sx={{ my: 2, display: 'block', color: 'black' }} >
                                <Typography variant='h2'>Bejelentkezés</Typography>
                            </Button>
                            <Button onClick={() => setRegisterPopup(true)}><Typography variant='h2'>Regisztráció</Typography></Button>
                        </Box>
                    }
                </Toolbar>

            </Container>
        </AppBar>
    )
}

export default Navbar
