import React, { useState, useEffect, useRef } from 'react'
import { useNavigate } from 'react-router-dom';
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'

import { Badge, Box, Container, IconButton, Menu, MenuItem, Typography } from '@mui/material';

import NotificationsIcon from '@mui/icons-material/Notifications';

import useNotificationService from '../Services/NotificationService';
import { getWebsocketRoutes } from '../utils/websocket-routes';

const NotificationComponent = ({ user }) => {
    const SOCKET_URL = 'http://localhost:8080/ws';

    const [anchorEl, setAnchorEl] = useState(null);
    const open = useRef(Boolean(anchorEl));
    const stompClient = useRef(null);
    const [notifications, setNotifications] = useState(null);
    const notificationsFromSocket = useRef([]);
    const [isLoading, setLoading] = useState(false);
    const [notificationCounter, setNotificationCounter] = useState(0);

    const navigate = useNavigate();

    const { getNumberOfUnreadNotifications, getAllNotifications } = useNotificationService();

    const callback = (newNotification) => {
        const tmp = [JSON.parse(newNotification.body), ...notificationsFromSocket.current];
        notificationsFromSocket.current = tmp;
        if (!open.current)
            setNotificationCounter(prev => prev + 1);
        if (open.current)
            showNotifications();
    };

    useEffect(() => {
        const socket = new SockJS(SOCKET_URL);
        stompClient.current = Stomp.over(socket);
        stompClient.current.debug = null;
        stompClient.current.connect({}, () => {
            getWebsocketRoutes(user.role, user.username).map((destination) => {
                stompClient.current.subscribe(destination, callback);
            });
        });

        return () => {
            getWebsocketRoutes(user.role, user.username).map((destionation) => {
                stompClient.current.unsubscribe(destionation);
            })
        }
    }, [])

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
        open.current = Boolean(event.currentTarget);

        if (open.current) {
            showNotifications();
        }
    };

    const showNotifications = async () => {
        if (!notifications)
            await getNotifications();
        if (notificationsFromSocket.current.length > 0) {
            const tmp = notificationsFromSocket.current;
            setNotifications(notifications => [...tmp, ...notifications]);
            stompClient.current.send("/app/mark-as-read", {}, user.username);
            notificationsFromSocket.current = [];
        }

        setNotificationCounter(0);
    }

    const handleClose = () => {
        setAnchorEl(null);
        open.current = false;
    };

    const getNotifications = async () => {
        setLoading(true);
        const notificationList = await getAllNotifications();
        setNotifications(notificationList);
        setLoading(false);
    };

    useEffect(() => {
        const apiCall = async () => {
            const count = await getNumberOfUnreadNotifications();
            setNotificationCounter(count);
        };
        apiCall();
    }, []);

    const formatDate = (dateTime) => {
        const date = dateTime.split('T')[0];
        const time = dateTime.split('T')[1];
        return `${date} ${time.split(':')[0]}:${time.split(':')[1]}`
    };

    return (
        <div>
            <IconButton onClick={handleClick}>
                <Badge badgeContent={notificationCounter} color='secondary'>
                    <NotificationsIcon />
                </Badge>
            </IconButton>
            <Menu
                anchorEl={anchorEl}
                elevation={0}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'right',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'right',
                }}
                open={Boolean(anchorEl)}
                onClose={handleClose}
            >
                <Container sx={{ width: '20vw', height: '90vh', padding: '10px 0px 10px 5px' }} maxWidth={false} disableGutters>
                    <Typography variant='h3'>Értesítések</Typography>
                    {isLoading && <MenuItem><p>... </p></MenuItem>}
                    {notifications && notifications.map((notification, index) => {
                        return (
                            <MenuItem
                                key={index}
                                divider
                                sx={{ whiteSpace: 'normal' }}
                                onClick={() => { handleClose(); navigate(notification.link) }
                                }>
                                <Box>
                                    <Typography>{notification.message}</Typography>
                                    <Typography>{formatDate(notification.date)}</Typography>
                                </Box>
                            </MenuItem>
                        )
                    })}
                </Container>
            </Menu>
        </div >
    )
}

export default NotificationComponent
