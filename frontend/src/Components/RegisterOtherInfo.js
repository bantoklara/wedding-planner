import { Box, FormLabel, Stack, TextField, Typography } from "@mui/material";

const RegisterOtherInfo = ({ values, error, weddingInputChangeHandler }) => {
    const inputFields =
        [
            [{
                label: "Menyasszony neve",
                name: "nameOfBride",
                required: true,
                type: 'text'
            },
            {
                label: "Vőlegény neve",
                name: "nameOfFiance",
                required: true,
                type: 'text'
            }],
            [{
                label: "Esküvő dátuma",
                name: "dateOfWedding",
                required: true,
                type: 'date'
            }],
            [{
                label: "Esküvőre való visszajelzés dátuma",
                name: "dateOfFeedbackDeadline",
                required: false,
                type: 'date'
            }],
        ]
    return (
        <Box width='100%'>
            <Stack spacing={4}>
                <Typography variant='h3' textAlign='left'>Esküvőre vonatkozó információk</Typography>
                <Stack spacing={1}>
                    {
                        inputFields.map((inputGroup, index) =>
                            <Box key={index} display='flex' justifyContent='left' alignItems='flex-start'>
                                {inputGroup.map((input, index2) => (
                                    <Box
                                        key={index2}
                                        display='flex'
                                    >
                                        {input.type === 'date' && <FormLabel >{input.label}</FormLabel>}
                                        <TextField
                                            sx={{ padding: 0 }}
                                            label={input.type === 'date' ? '' : input.label}
                                            name={input.name}
                                            value={values[input.name]}
                                            required={input.required}
                                            type={input.type}
                                            variant="standard"
                                            onChange={weddingInputChangeHandler}
                                            {...(error[input.name] && {
                                                error: true,
                                                helperText: error[input.name]
                                            })}
                                        />
                                    </Box>
                                ))}
                            </Box>
                        )}
                </Stack>
            </Stack>
        </Box>
    )
}

export default RegisterOtherInfo;