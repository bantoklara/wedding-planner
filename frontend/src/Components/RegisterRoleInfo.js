import React from 'react';

import { Box, FormControlLabel, Paper, Radio, RadioGroup, Stack, Typography } from "@mui/material";
import { makeStyles } from '@mui/styles';

const PaperStyle = makeStyles(() => ({
    active: {
        border: '3px solid #69837a'
    },
    inactive: {
        border: '0px solid black'
    }
}));

const RegisterRoleInfo = ({ values, inputChangeHandler }) => {
    const classes = PaperStyle();

    return (
        <Box
            sx={{ width: '100%' }}
        >
            <Stack spacing={4}>
                <Typography variant='h3' textAlign='left'>Mire szeretné használni az oldalt?</Typography>
                <Box>
                    <RadioGroup
                        name="role"
                        value={values.role}
                        onChange={inputChangeHandler}
                    >
                        <Stack spacing={2} sx={{ width: '300px' }}>
                            <Paper
                                className={values.role === 'ROLE_COUPLE' ? classes.active : classes.inactive}
                                elevation={3}
                            >
                                <FormControlLabel
                                    value={'ROLE_COUPLE'}
                                    control={<Radio />}
                                    sx={{ width: '100%' }}
                                    label={<Typography textAlign='center' sx={{ width: '100%' }}>Az esküvőm megszervezésére</Typography>}
                                />
                            </Paper>
                            <Paper
                                className={values.role === 'ROLE_SERVICE_PROVIDER' ? classes.active : classes.inactive}
                                elevation={3}
                            >
                                <FormControlLabel
                                    value={'ROLE_SERVICE_PROVIDER'}
                                    control={<Radio />}
                                    sx={{ width: '100%' }}
                                    label={<Typography textAlign='center' sx={{ width: '100%' }}>A szolgáltatásaim hírdetésére</Typography>}
                                />
                            </Paper>
                        </Stack>
                    </RadioGroup>
                </Box>
            </Stack>
        </Box>
    )
}

export default RegisterRoleInfo;