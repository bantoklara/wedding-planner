import { Box, Stack, TextField, Typography } from "@mui/material";

const RegisterUserInfo = ({ values, error, inputChangeHandler }) => {
    const inputFields = [
        [{
            name: 'lastName',
            label: 'Vezetéknév',
            required: true,
            type: 'text'
        },
        {
            name: 'firstName',
            label: 'Keresztnév',
            required: true,
            type: 'text'
        }],
        [{
            name: 'email',
            label: 'Email',
            required: true,
            type: 'text'
        },
        {
            name: 'phoneNumber',
            label: 'Telefonszám',
            required: true,
            type: 'text'
        }],
        [{
            name: 'username',
            label: 'Felhasználónév',
            required: true,
            type: 'text'
        },
        {
            name: 'password',
            label: 'Jelszó',
            required: true,
            type: 'password'
        }],
    ];

    return (
        <Box
            display='flex'
            flexDirection='column'
        >
            <Stack spacing={4}>
                <Typography variant='h3' textAlign='left'>Az ön adatai</Typography>
                <Box>
                    {inputFields.map((inputGroup, index) => (
                        <Box key={index} display='flex' justifyContent='left' alignItems='flex-start'>
                            {inputGroup.map((input, index2) => (
                                <TextField
                                    key={index2}
                                    label={input.label}
                                    name={input.name}
                                    value={values[input.name]}
                                    required={input.required}
                                    type={input.type}
                                    variant="standard"
                                    onChange={inputChangeHandler}
                                    {...(error[input.name] && {
                                        error: true,
                                        helperText: error[input.name]
                                    })}
                                />
                            ))}
                        </Box>
                    )
                    )}
                </Box>
            </Stack>
        </Box>
    )
}

export default RegisterUserInfo;