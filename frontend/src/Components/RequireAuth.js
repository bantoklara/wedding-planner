import { useContext } from "react"
import { Navigate, Outlet } from "react-router-dom";
import UserContext from "../UserContext"

const RequireAuth = ({ allowedRole = [] }) => {
    const { user } = useContext(UserContext);

    return (
        allowedRole.includes(user?.role)
            ? <Outlet />
            : user
                ? <Navigate to="/" />
                : <></>
    );
}

export default RequireAuth;