import React, { useState, useEffect } from 'react'

import {
    Button,
    Box,
    TextField,
    FormHelperText,
    Paper,
    Typography,
    MenuItem,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    FormControlLabel,
    Checkbox
} from '@mui/material'

import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';

import useScheduleService from '../Services/ScheduleService';

const initialValues = {
    start: '',
    end: '',
    title: '',
    serviceId: '',
    bookedServiceId: ''
};

const ScheduleCreation = (props) => {
    const [error, setError] = useState('');
    const [serverError, setServerError] = useState('');
    const [loading, setLoading] = useState(false);
    const [client, setClient] = useState(false);

    const [values, setValues] = useState(initialValues);

    const { createSchedule } = useScheduleService();

    const validate = (fieldValues) => {
        let tmp = error;

        if ('start' in fieldValues) {
            tmp = values.end && fieldValues.start > values.end ? 'A kezdeti időpont előbb kell legyen mint a végső.' : '';
        }

        if ('end' in fieldValues) {
            tmp = values.start && values.start > fieldValues.end ? 'A kezdeti időpont előbb kell legyen mint a végső.' : '';
        }

        setError(tmp);
    };

    useEffect(() => {
        setError('');
        setServerError('');
        setClient(false);
        setValues(initialValues);
        props.inputFields && setValues({ ...values, 'start': props.inputFields.start, 'end': props.inputFields.end });
    }, [props.inputFields, props.dialogOpen])

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setValues({ ...values, [name]: value });
        validate({ [name]: value });
    }

    const serviceChangeHandler = (event) => {
        setValues({ ...values, 'serviceId': event.target.value, 'bookedServiceId': '' })
    };

    const submitFormHandler = async (event) => {
        event.preventDefault();

        setLoading(true);
        const body = { ...values };
        (!body.bookedServiceId || !client) && delete body.bookedServiceId;

        const response = await createSchedule(values);

        if (response) {
            const start = new Date(String(response.start));
            const end = new Date(String(response.end));
            const startTime = start.getHours() + start.getMinutes();
            const endTime = end.getHours() + end.getMinutes();

            response.allDay = startTime === endTime && endTime === 0;

            props.setSchedules(prev => prev.concat(response));
            props.setDialogOpen(false);
        } else {
            setServerError('Az adott intervallum már foglalt');
        }
        setLoading(false);
    }

    return (
        <Dialog open={props.dialogOpen} onClose={() => props.setDialogOpen(false)}>
            <DialogTitle>Esemény létrehozása</DialogTitle>
            <DialogContent>
                {serverError &&
                    <Paper><Typography sx={{ color: 'red' }}>{serverError}</Typography></Paper>
                }
                <Box>
                    <TextField
                        select
                        required
                        label='Szolgáltatás'
                        name='serviceId'
                        value={values.serviceId}
                        fullWidth
                        onChange={serviceChangeHandler}
                    >
                        {props?.services?.map((s) => (
                            <MenuItem key={s.id} value={s.id} >{s.name}</MenuItem>
                        ))}
                    </TextField>
                    {values.serviceId && <Box>
                        <FormControlLabel
                            control={<Checkbox
                                checked={client}
                                onChange={(event) => setClient(event.target.checked)}
                            />}
                            label='Ügyfél hozzárendelése'
                        />
                        <TextField
                            select
                            name='bookedServiceId'
                            value={values.bookedServiceId}
                            onChange={inputChangeHandler}
                            disabled={!client}
                        >
                            {(props.services.find(s => s.id === parseInt(values.serviceId)))?.bookedServices?.map((bookedService) => (
                                <MenuItem key={bookedService.id} value={bookedService.id}>
                                    {bookedService.wedding.nameOfBride}&{bookedService.wedding.nameOfFiance}</MenuItem>
                            ))}
                        </TextField>
                    </Box>
                    }
                </Box>
                <Box display='flex' justifyContent='center' alignItems='center'>
                    <TextField
                        name='start'
                        type='datetime-local'
                        value={values.start}
                        onChange={inputChangeHandler}
                        {...(error && { error: true })}
                    />
                    <HorizontalRuleIcon />
                    <TextField
                        name='end'
                        type='datetime-local'
                        value={values.end}
                        onChange={inputChangeHandler}
                        {...(error && { error: true })}
                    />
                    {error && <FormHelperText style={{ color: 'red' }}>{error}</FormHelperText>}
                </Box>
                <Box>
                    <TextField
                        name='title'
                        label='Cím'
                        value={values.title}
                        onChange={inputChangeHandler}
                    />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button
                    color='secondary'
                    variant='contained'
                    sx={{ color: '#ffffff' }}
                    onClick={submitFormHandler}>Mentés {loading && '...'}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default ScheduleCreation
