import React, { useEffect, useState } from 'react'

import {
    Button,
    Box,
    TextField,
    FormHelperText,
    Paper,
    Typography,
    MenuItem,
    Dialog,
    DialogTitle,
    DialogContent,
    DialogActions,
    Checkbox,
    FormControlLabel
} from '@mui/material'

import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';

import useScheduleService from '../Services/ScheduleService';

const ScheduleModification = (props) => {
    const [values, setValues] = useState({
        start: '',
        end: '',
        title: '',
        serviceId: '',
        bookedServiceId: ''
    });
    const [error, setError] = useState('');
    const [deleteLoading, setDeleteLoading] = useState(false);
    const [updateLoading, setUpdateLoading] = useState(false);
    const [serverError, setServerError] = useState('');
    const [client, setClient] = useState(false);

    const { deleteSchedule, updateSchedule } = useScheduleService();

    useEffect(() => {
        if (props.dialogOpen) {
            setDeleteLoading(false);
            setUpdateLoading(false);
            setServerError(false);
            setError('');
            if (props.schedule)
                setValues({
                    start: props.schedule.start,
                    end: props.schedule.end,
                    title: props.schedule.title,
                    serviceId: props.schedule.service.id,
                    bookedServiceId: props.schedule.bookedService ? props.schedule.bookedService.id : ''
                });
            setClient(props.schedule.bookedService !== null);
        }
    }, [props.dialogOpen]);

    const validate = (fieldValues) => {
        let tmp = error;

        if ('start' in fieldValues) {
            tmp = values.end && fieldValues.start > values.end ? 'A kezdeti időpont előbb kell legyen mint a végső.' : '';
        }

        if ('end' in fieldValues) {
            tmp = values.start && values.start > fieldValues.end ? 'A kezdeti időpont előbb kell legyen mint a végső.' : '';
        }

        setError(tmp);
    };

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setValues({ ...values, [name]: value });
        validate({ [name]: value });
    }

    const serviceChangeHandler = (event) => {
        setValues({ ...values, 'serviceId': event.target.value, 'bookedServiceId': '' });
    }

    const deleteScheduleHandler = async () => {
        setDeleteLoading(true);
        const success = await deleteSchedule(props.schedule.id);
        if (success) {
            props.setSchedules(prev => prev.filter((s) => s.id !== props.schedule.id));
        }
        setDeleteLoading(false);
        props.setDialogOpen(false);
    }

    const updateScheduleHandler = async () => {
        setUpdateLoading(true);

        const body = { ...values };
        (!body.bookedServiceId || !client) && delete body.bookedServiceId;

        const response = await updateSchedule(props.schedule.id, body);
        if (response.error) {
            setServerError(response.error);
        } else {
            const start = new Date(String(response.schedule.start));
            const end = new Date(String(response.schedule.end));
            const startTime = start.getHours() + start.getMinutes();
            const endTime = end.getHours() + end.getMinutes();

            response.schedule.allDay = startTime === endTime && endTime === 0;
            props.setSchedules(prev => {
                const tmp = [...prev];
                const index = tmp.findIndex(s => s.id === props.schedule.id);
                tmp[index] = response.schedule;
                return tmp;
            });
            props.setDialogOpen(false);
        }
        setUpdateLoading(false);
    }

    return (
        <Dialog open={props.dialogOpen} onClose={() => props.setDialogOpen(false)}>
            <DialogTitle>Esemény</DialogTitle>
            <DialogContent>
                {serverError &&
                    <Paper><Typography sx={{ color: 'red' }}>{serverError}</Typography></Paper>
                }
                <Box>
                    <TextField
                        select
                        required
                        label='Szolgáltatás'
                        name='serviceId'
                        value={values.serviceId}
                        onChange={serviceChangeHandler}
                    >
                        {props.services.map((s) => (
                            <MenuItem key={s.id} value={s.id}>{s.name}</MenuItem>
                        ))}
                    </TextField>
                    <Box>
                        {values.serviceId && <Box>
                            <FormControlLabel
                                control={<Checkbox
                                    checked={client}
                                    onChange={(event) => setClient(event.target.checked)}
                                />}
                                label='Ügyfél hozzárendelése'
                            />
                            <TextField
                                select
                                name='bookedServiceId'
                                value={values.bookedServiceId}
                                onChange={inputChangeHandler}
                                disabled={!client}
                            >
                                {(props.services.find(s => s.id === parseInt(values.serviceId)))?.bookedServices?.map((bookedService) => (
                                    <MenuItem key={bookedService.id} value={bookedService.id}>
                                        {bookedService.wedding.nameOfBride}&{bookedService.wedding.nameOfFiance}</MenuItem>
                                ))}
                            </TextField>
                        </Box>
                        }
                    </Box>
                </Box>

                <Box display='flex' justifyContent='center' alignItems='center'>
                    <TextField
                        name='start'
                        type='datetime-local'
                        value={values.start}
                        onChange={inputChangeHandler}
                        {...(error && { error: true })}
                    />
                    <HorizontalRuleIcon />
                    <TextField
                        name='end'
                        type='datetime-local'
                        value={values.end}
                        onChange={inputChangeHandler}
                        {...(error && { error: true })}
                    />
                    {error && <FormHelperText style={{ color: 'red' }}>{error}</FormHelperText>}
                </Box>
                <Box>
                    <TextField
                        name='title'
                        label='Cím'
                        value={values.title}
                        onChange={inputChangeHandler}
                    />
                </Box>
            </DialogContent>
            <DialogActions>
                <Button
                    color='error'
                    variant='outlined'
                    onClick={deleteScheduleHandler}
                >Esemény törlése {deleteLoading && '...'}
                </Button>
                <Button
                    color='secondary'
                    sx={{ color: '#ffffff' }}
                    variant='contained'
                    onClick={updateScheduleHandler}
                >Mentés {updateLoading && '...'}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default ScheduleModification
