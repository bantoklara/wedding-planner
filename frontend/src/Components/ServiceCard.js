import React from 'react'
import { useNavigate } from 'react-router-dom';

import { Card, CardMedia, CardHeader, CardActions, Button, ImageListItemBar, ImageListItem, Box } from '@mui/material'

import priceCategories from '../utils/priceCategories';

const ServiceCard = ({ service }) => {
    const navigate = useNavigate();

    const handleServiceClicked = (id) => {
        navigate(`/services/${id}`);
    };

    const getPriceCategoryIcon = (value) => {
        return (
            priceCategories.filter((priceCategory) => priceCategory.value === value)[0].label
        )
    };
    return (
        <>
            {service.cover &&
                <Card style={{ height: '300px', padding: 0, paddingBottom: '10px' }}>
                    <Box height='60%' >
                        <ImageListItem style={{ height: '100%', width: '100%' }}>
                            <CardMedia
                                height='100%'
                                component='img'
                                image={service.cover.url}
                                title={service.cover.description}
                            />
                            <ImageListItemBar
                                position='top'
                                sx={{ width: 'fit-content', backgroundColor: 'rgba(255,255,255,0.95)', borderRadius: '5px', maring: '5px' }}
                                title={getPriceCategoryIcon(service.priceCategory)}
                            />
                        </ImageListItem>
                    </Box>
                    <CardHeader
                        color="primary"
                        title={service.name}
                        subheader={service.category.name}
                    />
                    <CardActions>
                        <Button onClick={() => handleServiceClicked(service.id)}>Megtekintés</Button>
                    </CardActions>
                </Card>
            }
        </>
    )
}

export default ServiceCard
