import React, { useRef } from 'react'

import {
    Box,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    Input,
    InputAdornment,
    MenuItem,
    Paper,
    Radio,
    RadioGroup,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableRow,
    TextField
} from '@mui/material'

import AddPhotoAlternateIcon from '@mui/icons-material/AddPhotoAlternate';

import { countyList } from "../utils/county";
import { useFormHandlers } from '../Services/FormHandlers/ServiceCreationFormHandler'
import priceCategories from '../utils/priceCategories';

const ServiceCreationForm = ({ categories, serviceDetails }) => {
    const {
        values,
        errors,
        images,
        imagesFromDb,
        coverImage,
        loading,
        inputChangeHandler,
        categoryChangeHandler,
        countyCheckHandler,
        coverImageUploadHandler,
        imageUploadHandler,
        imageDescriptionChangeHandler,
        imageFromDbDescriptionChangedHandler,
        submitFormHandler,
        deleteImage,
        deleteImageFromDb
    } = useFormHandlers(categories, countyList, serviceDetails);

    const coverImageRef = useRef(null);
    const imageRef = useRef(null);

    const handleCoverImageUploadButtonClicked = () => {
        coverImageRef.current.click();
    }

    const handleImageUploadButtonClicked = () => {
        imageRef.current.click();
    }

    return (
        <div>
            <h2>Új szolgáltatás létrehozása</h2>
            <form onSubmit={submitFormHandler} type="multipart/form-data">
                <Box>
                    <Stack spacing={2}>
                        <Paper>
                            <h4>Általános információk</h4>
                            <TextField
                                select
                                required
                                style={{ width: '50vw' }}
                                label="Kategória"
                                name="categoryId"
                                value={values.categoryId === 0 ? '' : values.categoryId}
                            >
                                {categories.map((category) => {
                                    return (
                                        <MenuItem
                                            value={category.id}
                                            key={category.id}
                                            onClick={() => categoryChangeHandler(category)}
                                        >{category.name}
                                        </MenuItem>
                                    )
                                })
                                }
                            </TextField>
                            {values.category.name === 'Étterem' &&
                                <TextField
                                    type="number"
                                    required
                                    label="Maximális férőhely szám"
                                    name="maximalSpaceCapacity"
                                    value={values.maximalSpaceCapacity}
                                    onChange={inputChangeHandler}
                                />
                            }
                            <div>
                                <TextField
                                    type="text"
                                    required
                                    label="Név"
                                    name="name"
                                    value={values.name}
                                    onChange={inputChangeHandler}
                                    {...(errors.name && {
                                        error: true,
                                        helperText: errors.name
                                    })}
                                />
                            </div>
                            <div>
                                <TextField
                                    type="text"
                                    required
                                    multiline
                                    rows={3}
                                    style={{ width: '80%' }}
                                    label="Leírás"
                                    name="description"
                                    value={values.description}
                                    onChange={inputChangeHandler}
                                    {...(errors.description && {
                                        error: true,
                                        helperText: errors.description
                                    })}
                                />
                            </div>
                        </Paper>
                        <Paper>
                            <h4>Szolgáltatási terület</h4>
                            <FormControl>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={values.counties.size === countyList.length}
                                            onChange={(e) => countyCheckHandler(e, 'All')}
                                        />
                                    }
                                    label="Az ország bármely területén"
                                />
                                <Grid container>
                                    {countyList.map((county, index) => {
                                        return (
                                            <Grid item key={index} lg={2}>
                                                <FormControlLabel
                                                    control={
                                                        <Checkbox
                                                            checked={values.counties.has(county)}
                                                            onChange={(e) => countyCheckHandler(e, county)}
                                                        />
                                                    }
                                                    label={county}
                                                />
                                            </Grid>
                                        )
                                    })}
                                </Grid>
                            </FormControl>
                        </Paper>
                        <Paper>
                            <h4>Árkategória</h4>
                            <RadioGroup
                                name="priceCategory"
                                value={values.priceCategory}
                                onChange={inputChangeHandler}
                            >
                                {priceCategories.map((priceCategory, index) => {
                                    return (
                                        <FormControlLabel
                                            key={index}
                                            checked={priceCategory.value === values.priceCategory}
                                            value={priceCategory.value}
                                            control={<Radio />}
                                            label={priceCategory.label}
                                        />
                                    )
                                })}
                            </RadioGroup>
                        </Paper>
                        <Paper>
                            <Box display='flex' alignItems='center' gap={2}>
                                <h4>Képek</h4>
                                <p style={{ color: (errors.totalSize && errors.totalSize !== '') ? 'red' : 'black' }}
                                >{(values.totalSize / 1000).toFixed(2)} KB / 2MB</p>
                            </Box>
                            <Stack spacing={2}>
                                <Box display='flex' alignItems='center' gap={2}>
                                    <Box>
                                        <input
                                            ref={coverImageRef}
                                            style={{ display: 'none' }}
                                            required={!serviceDetails}
                                            type="file"
                                            onChange={coverImageUploadHandler}
                                        />
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            startIcon={<AddPhotoAlternateIcon />}
                                            onClick={handleCoverImageUploadButtonClicked}><h4>Borítókép feltöltése <i>(kötelező)</i></h4>
                                        </Button>
                                    </Box>
                                    <Box>
                                        <input
                                            ref={imageRef}
                                            style={{ display: 'none' }}
                                            multiple
                                            type="file"
                                            onChange={imageUploadHandler}
                                        />
                                        <Button
                                            variant='contained'
                                            color='primary'
                                            startIcon={<AddPhotoAlternateIcon />}
                                            onClick={handleImageUploadButtonClicked}><h4>Képek feltöltése</h4>
                                        </Button>
                                    </Box>
                                </Box>
                                <Box>
                                    {coverImage
                                        ? <>
                                            <h5>Borítókép</h5>
                                            <img src={coverImage} style={{ height: 'auto', width: '200px' }} />
                                            {errors.coverImage &&
                                                <FormHelperText
                                                    style={{ color: 'red' }}>{errors.coverImage}</FormHelperText>
                                            }
                                        </>
                                        : serviceDetails?.coverImage && <img src={coverImage.url} alt={coverImage.description} style={{ width: '65%', height: 'auto' }} />
                                    }
                                </Box>
                                {(images.length > 0 || values.imagesFromDb?.length > 0) &&
                                    <Box>
                                        <h5>Képek</h5>
                                        <Table>
                                            <TableBody>
                                                {images.map((image, index) => {
                                                    return (
                                                        <TableRow key={index}>
                                                            <TableCell align='left' style={{ width: '15%' }}>
                                                                <img src={image} style={{ height: '80px', width: '100px' }} />
                                                                {errors.images[index] &&
                                                                    <FormHelperText
                                                                        style={{ color: 'red' }}
                                                                    >
                                                                        {errors.images[index]}
                                                                    </FormHelperText>
                                                                }
                                                            </TableCell>
                                                            <TableCell align='left' style={{ width: '45%' }}>
                                                                <TextField
                                                                    placeholder="Leírás"
                                                                    name="descriptions"
                                                                    value={values.descriptions[index]}
                                                                    onChange={(e) => { imageDescriptionChangeHandler(e, index) }}
                                                                />
                                                            </TableCell>
                                                            <TableCell align='right' style={{ width: '20%' }}>
                                                            </TableCell>
                                                            <TableCell align='right' style={{ width: '20%' }}>
                                                                <Button variant='contained' onClick={() => deleteImage(index)}>
                                                                    Törlés
                                                                </Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    )
                                                })}
                                                {values.imagesFromDb.map((image, index) => {
                                                    return (
                                                        <TableRow key={index}>
                                                            <TableCell align='left' style={{ width: '15%' }}>
                                                                <img src={image.url} alt={image.description} style={{ height: '80px', width: '100px' }} />
                                                            </TableCell>
                                                            <TableCell align='left' style={{ width: '65%' }}>
                                                                <TextField
                                                                    placeholder="Leírás"
                                                                    name="descriptions"
                                                                    value={values.imagesFromDb[index].description}
                                                                    onChange={(e) => { imageFromDbDescriptionChangedHandler(e, index) }}
                                                                />
                                                            </TableCell>
                                                            <TableCell align='right' style={{ width: '20%' }}>
                                                                <Button variant='contained' onClick={() => deleteImageFromDb(index)}>
                                                                    Törlés
                                                                </Button>
                                                            </TableCell>
                                                        </TableRow>
                                                    )
                                                })}
                                            </TableBody>
                                        </Table>
                                    </Box>
                                }
                            </Stack>
                        </Paper>
                        <div>
                            <Button
                                type="submit"
                                color="secondary"
                                sx={{ color: '#ffffff' }}
                                variant='contained'>Feltöltés {loading && '...'}</Button>
                        </div>
                    </Stack>
                </Box>
            </form >
        </div >
    )
}

export default ServiceCreationForm
