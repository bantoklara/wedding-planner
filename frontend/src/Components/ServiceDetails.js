import React from 'react'

import { Box, Grid, ImageList, ImageListItem, ImageListItemBar, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';

import LocalPhoneIcon from '@mui/icons-material/LocalPhone';
import EmailIcon from '@mui/icons-material/Email';

const ServiceDetails = ({ service }) => {
    return (
        <>
            <Box display='flex' gap={8}>
                <Box>
                    <Box>
                        <ImageListItem>
                            <img
                                src={service.cover.url} alt={service.cover.description}
                                style={{ width: 'auto', height: '60vh', borderRadius: '5px' }}
                            ></img>
                            <ImageListItemBar
                                sx={{ width: 'fit-content' }}
                                title={service.name}
                            />
                        </ImageListItem>
                    </Box>
                    <div>
                        <h4>Rólunk</h4>
                        <p>{service.description}</p>
                    </div>
                </Box>
                <Box>
                    <div>
                        <h4>Elérhetőségek</h4>
                        <List>
                            <ListItem>
                                <ListItemIcon><LocalPhoneIcon /></ListItemIcon>
                                <ListItemText><a href={`tel:${service.user.phoneNumber}`}>{service.user.phoneNumber}</a></ListItemText>
                            </ListItem>
                            <ListItem>
                                <ListItemIcon><EmailIcon /></ListItemIcon>
                                <ListItemText><a href={`mailto:${service.user.email}`}>{service.user.email}</a></ListItemText>
                            </ListItem>
                        </List>
                    </div>
                    <div>
                        <h4>Szolgáltatási terület</h4>
                        <Grid container gap={1}>
                            {service.counties?.split('|')?.map((county, index) => {
                                return (
                                    <Grid item
                                        key={index}
                                        className='county'
                                    >
                                        {county}</Grid>
                                )
                            })}
                        </Grid>
                    </div>
                </Box>
            </Box>
            <h4>Képek</h4>
            <ImageList variant='quited' gap={8} cols={5}>
                {service.images.map((image, index) => {
                    return (
                        <ImageListItem key={index}>
                            <img
                                src={image.url}
                                alt={image.description}
                                loading='lazy' />
                        </ImageListItem>
                    )
                })}
            </ImageList>
        </>
    )
}

export default ServiceDetails
