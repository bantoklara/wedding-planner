import React from 'react'
import {
    Checkbox,
    Autocomplete,
    FormControlLabel,
    Radio,
    RadioGroup,
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
    Typography,
    Stack,
    Paper,
    FormLabel
} from '@mui/material';

import RefreshIcon from '@mui/icons-material/Refresh';
import HorizontalRuleIcon from '@mui/icons-material/HorizontalRule';

import { countyList } from '../utils/county';
import priceCategories from '../utils/priceCategories';

const ServiceFilterForm = (props) => {
    const {
        values,
        inputChangeHandler,
        dateChangeHandler,
        enableCheckHandler,
        myServiceCheckboxChangeHandler,
        clearAllFilters,
        submitFormHandler
    } = props;

    const onSubmit = () => {
        submitFormHandler();
    }

    return (
        <Dialog open={props.dialogOpen} onClose={() => props.setDialogOpen(false)}>
            <DialogTitle>
                <Box
                    display='flex'
                >
                    <Typography variant='h3' flexGrow={1}>Szűrés</Typography>
                    <Button onClick={clearAllFilters} sx={{ padding: 0, margin: 0 }}><RefreshIcon /></Button>
                </Box>
            </DialogTitle>
            <DialogContent>
                <Box display='flex' flexDirection='column'>
                    <Stack spacing={1}>
                        {(props.role === 'ROLE_ADMIN' || props.role === 'ROLE_SERVICE_PROVIDER') &&
                            <Paper>
                                <Box>
                                    <h4>Szolgáltatás állapota</h4>
                                    {props.role === 'ROLE_SERVICE_PROVIDER' &&
                                        <FormControlLabel
                                            control={<Checkbox checked={values.myServices === true} onChange={myServiceCheckboxChangeHandler} />}
                                            label="Saját szolgáltatások"
                                        />
                                    }
                                    <Box sx={{ ml: props.role === 'ROLE_ADMIN' ? 0 : 3 }}>
                                        <RadioGroup
                                            name='enabled'
                                            value={values.enabled}
                                            onChange={enableCheckHandler}
                                        >
                                            <FormControlLabel
                                                disabled={props.role === 'ROLE_SERVICE_PROVIDER' && !values.myServices}
                                                checked={values.enabled}
                                                value={true}
                                                control={<Radio />}
                                                label="Jóváhagyott" />
                                            <FormControlLabel
                                                disabled={props.role === 'ROLE_SERVICE_PROVIDER' && !values.myServices}
                                                checked={!values.enabled}
                                                value={false}
                                                control={<Radio />}
                                                label="Függőben" />
                                        </RadioGroup>
                                    </Box>

                                </Box>
                            </Paper>
                        }
                        <Paper>
                            <h4>Helyszín</h4>
                            <Autocomplete
                                name="county"
                                value={values.county === '' ? null : values.county}
                                onChange={(event, newValue) => inputChangeHandler({ target: { name: 'county', value: newValue ? newValue : '' } })}
                                options={countyList}
                                renderInput={(params) => <TextField {...params} placeholder='Megye szerinti keresés' variant='outlined' />}
                            />
                        </Paper>
                        <Paper>
                            <h4>Kategória</h4>
                            <RadioGroup
                                name='categoryId'
                                value={values.categoryId}
                                onChange={inputChangeHandler}
                            >
                                <FormControlLabel
                                    value=''
                                    checked={values.categoryId == ''}
                                    control={<Radio />}
                                    label='Összes kategória'
                                />
                                {props.categories.map((category) => {
                                    return (
                                        <FormControlLabel
                                            key={category.id}
                                            value={category.id}
                                            checked={category.id == values.categoryId}
                                            control={<Radio />}
                                            label={category.name}
                                        />
                                    )
                                })}
                            </RadioGroup>
                        </Paper>
                        <Paper>
                            <h4>Keresés dátum szerint</h4>
                            <RadioGroup
                                name='byDay'
                                value={values.byDay}
                                onChange={inputChangeHandler}
                            >
                                <Box>
                                    <FormControlLabel
                                        control={
                                            <Radio
                                                checked={values.byDay === 'byDay'}
                                                value='byDay'
                                            />}
                                        label='Ezen a napon'
                                    />
                                    <TextField
                                        type='date'
                                        name='date'
                                        value={values.date}
                                        onChange={dateChangeHandler}
                                        disabled={values.byDay !== 'byDay'}
                                    />
                                </Box>
                                <FormControlLabel
                                    control={
                                        <Radio
                                            value='byInterval'
                                            checked={values.byDay === 'byInterval'}
                                        />}
                                    label='Ebben az intervallumban'
                                />

                                <Box display='flex' justifyContent='center' alignItems='center'>
                                    <TextField
                                        type='datetime-local'
                                        name='start'
                                        value={values.start}
                                        onChange={dateChangeHandler}
                                        disabled={values.byDay !== 'byInterval'}
                                    />
                                    <HorizontalRuleIcon />
                                    <TextField
                                        type='datetime-local'
                                        name='end'
                                        value={values.end}
                                        onChange={dateChangeHandler}
                                        disabled={values.byDay !== 'byInterval'}
                                    />
                                </Box>
                            </RadioGroup>
                        </Paper>
                        <Paper>
                            <h4>Étteremre vonatkozó információk</h4>
                            <Box display='flex' alignItems='center'>
                                <FormLabel>Férőhelyek száma</FormLabel>
                                <TextField
                                    sx={{ width: '80px' }}
                                    type='number'
                                    name='numberOfRequiredSpace'
                                    value={values.numberOfRequiredSpace}
                                    onChange={inputChangeHandler}
                                />
                            </Box>
                        </Paper>
                        <Paper>
                            <h4>Árkategória</h4>
                            <RadioGroup
                                name='priceCategory'
                                value={values.priceCategory}
                                onChange={inputChangeHandler}
                            >
                                <FormControlLabel
                                    value=''
                                    control={<Radio />}
                                    label='Összes'
                                />
                                {priceCategories.map((priceCategory, index) => {
                                    return (
                                        <FormControlLabel
                                            key={index}
                                            value={priceCategory.value}
                                            control={<Radio />}
                                            label={priceCategory.label}
                                        />
                                    )
                                })}

                            </RadioGroup>
                        </Paper>
                    </Stack>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={onSubmit}>Keresés {props.loading && '...'}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default ServiceFilterForm
