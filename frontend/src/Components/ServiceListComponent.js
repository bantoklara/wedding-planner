import React, { useCallback, useRef } from 'react'

import { Box, Grid } from '@mui/material'

import ServiceCard from './ServiceCard';

const ServiceListComponent = ({ services, loading, hasMore, setPage }) => {
    const observer = useRef();
    const lastServiceElementRef = useCallback(node => {
        if (loading) return
        if (observer.current) observer.current.disconnect()
        observer.current = new IntersectionObserver(entries => {
            if (entries[0].isIntersecting && hasMore) {
                setPage(prev => { return prev + 1 })
            }
        })
        if (node) observer.current.observe(node)
    }, [loading, hasMore])

    return (
        <Box marginLeft='30px'>
            <Grid container rowSpacing={2} columnSpacing={4}>
                {services.map((service, index) => {
                    return (
                        <Grid
                            item
                            lg={2} md={3} xs={6}
                            key={service.id}
                            style={{
                                padding: '5px',
                                borderRadius: '5px'
                            }}>
                            {index === services.length - 1
                                ? <div ref={lastServiceElementRef}><ServiceCard service={service} /></div>
                                : <div><ServiceCard service={service} /></div>
                            }
                        </Grid>
                    )
                })}
            </Grid>
        </Box>
    )
}

export default ServiceListComponent
