import React, { useState, useRef } from 'react'
import { Link } from 'react-router-dom'

import { Box, Button, IconButton, TableCell, TableRow } from '@mui/material'

import AttachFileIcon from '@mui/icons-material/AttachFile';
import DownloadIcon from '@mui/icons-material/Download';
import DeleteIcon from '@mui/icons-material/Delete';

import useSharedFileService from '../Services/SharedFileService';
import SortedTableWithPagination from './SortedTableWithPagination';
import Warning from './Warning';

const SharedFiles = (props) => {
    const [file, setFile] = useState('');
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(-1);
    const [params, setParams] = useState({});

    const fileRef = useRef(null);

    const { saveFile, deleteFile } = useSharedFileService(props.bookedServiceId);

    const headers = [
        {
            name: "name",
            label: "Név",
            align: 'left',
            width: '20%',
            sortable: true
        },
        {
            name: "type",
            label: "Típus",
            align: 'left',
            width: '20%',
            sortable: true
        },
        {
            name: "uploadDate",
            label: "Feltöltés dátuma",
            align: 'left',
            width: '20%',
            sortable: true
        },
        {
            name: "actionButton",
            label: "",
            align: 'right',
            width: '20%',
            sortable: false
        },
    ];

    const formatDate = (date = new Date()) => {
        return `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
    }

    const uploadFile = async (e) => {
        e.preventDefault();
        const file = e.target.files[0];
        setFile(file);
        if (file.size / 1000 <= 2000) {
            const formData = new FormData();
            formData.append('file', file);

            const createdFile = await saveFile(formData);
            if (createdFile) {
                setFile('');
                props.setCount(count => count + 1);
            }
        }
    };

    const deleteFileHandler = async ({ fileId }) => {
        setLoading(1);
        await deleteFile(fileId);
        props.setCount(count => count + 1);
        setLoading(0);
    }

    const triggerPopup = (parameters) => {
        setParams(parameters);
        setLoading(-1);
        setButtonPopup(true);
    };

    const handleFileUploadButtonClicked = () => {
        fileRef.current.click();
    }

    return (
        <>
            <Warning
                dialogOpen={buttonPopup}
                setDialogOpen={setButtonPopup}
                message='Biztosan törli a fájlt?'
                cancelMessage='Mégsem'
                continueMessage='Fájl törlése'
                action={deleteFileHandler}
                params={params}
                loading={loading}
            />
            <h3>Megosztott fájlok</h3>
            <Box display='flex' gap={2}>
                <input
                    ref={fileRef}
                    type='file'
                    style={{ display: 'none' }}
                    onChange={uploadFile}
                />
                <Button
                    color='secondary'
                    variant='contained'
                    sx={{ color: '#ffffff' }}
                    startIcon={<AttachFileIcon />}
                    onClick={handleFileUploadButtonClicked}
                >Fájl feltöltése</Button>
                {file && file.size / 1000 > 2000 && <p style={{ color: 'red' }}>Maximális méret: 2MB</p>}
            </Box>


            <SortedTableWithPagination
                headers={headers}
                values={props.values}
                sortByChangeHandler={props.sortByChangeHandler}
                pagingChangedHandler={props.pagingChangedHandler}
                orderHasChanged={props.orderHasChanged}
                size={props.size}
            >
                {props.sharedFiles.map((file) => {
                    return (
                        <TableRow key={file.id}>
                            <TableCell align='left'>{file.name}</TableCell>
                            <TableCell align='left'>{file.type}</TableCell>
                            <TableCell align='left'>{formatDate(new Date(file.uploadDate))}</TableCell>
                            <TableCell align='right'>
                                <IconButton
                                    color='error'
                                    onClick={() => triggerPopup({ fileId: file.id })} >
                                    <DeleteIcon /></IconButton>
                                <Link to={file.url} download={file.name} target='_blank'>
                                    <IconButton color='secondary'><DownloadIcon /></IconButton></Link>
                            </TableCell>
                        </TableRow>
                    )
                })}
            </SortedTableWithPagination>
        </>
    )
}

export default SharedFiles
