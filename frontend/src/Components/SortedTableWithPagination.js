import React, { useState } from 'react'
import { Table, TableBody, TableCell, TableHead, TableRow, Button, TableFooter, TablePagination } from '@mui/material';

import NorthIcon from '@mui/icons-material/North';
import SouthIcon from '@mui/icons-material/South';

import TablePaginationAction from './TablePaginationAction';

const setInitialState = (headers) => {
    const values = new Map();

    headers.map((header) => {
        if (header.sortable)
            values.set(header.name, {
                onHover: header.onHover,
                order: 'ASC'
            })
    });

    return values;
};

const SortedTableWithPagination = (props) => {

    const [sortableHeaders, setSortableHeaders] = useState(setInitialState(props.headers))
    const { values, sortByChangeHandler, orderHasChanged, pagingChangedHandler } = props;
    const emptyRows = Math.max(0, (1 + parseInt(values.page)) * parseInt(values.perPage) - props.size);

    const headerClickedHandler = (name) => {
        if (values.sortBy === name) {
            const newOrder = sortableHeaders.get(name).order === 'ASC' ? 'DESC' : 'ASC';
            setSortProperty(name, 'order', newOrder);
            orderHasChanged(newOrder);
        } else {
            setSortProperty(name, 'order', 'ASC');
            sortByChangeHandler(name);
        }
    };

    const setSortProperty = (name, key, value) => {
        setSortableHeaders(prevMap => {
            const newMap = new Map(prevMap);

            newMap.set(name, { ...prevMap.get(name), [key]: value });

            return newMap;
        })
    };

    return (
        <Table>
            <TableHead>
                <TableRow>
                    {props.headers.map((header, index) => {
                        return (
                            <TableCell
                                key={index}
                                align={header.align}
                                style={{ width: header.width }}
                                onMouseEnter={() => header.sortable && header.name !== values.sortBy && setSortProperty(header.name, 'onHover', true)}
                                onMouseLeave={() => header.sortable && setSortProperty(header.name, 'onHover', false)}
                                onClick={() => headerClickedHandler(header.name)}
                            >
                                {header.sortable
                                    ? <Button
                                        endIcon=
                                        {
                                            (sortableHeaders?.get(header.name).onHover || header.name === values.sortBy) &&
                                            (sortableHeaders?.get(header.name).order === 'ASC'
                                                ? <NorthIcon fontSize='small' />
                                                : <SouthIcon fontSize='small' />
                                            )
                                        }
                                    >
                                        {header.label}
                                    </Button>
                                    : <>{header.label}</>
                                }
                            </TableCell>
                        )
                    })}
                </TableRow>
            </TableHead>
            <TableBody>
                {props.children}
                {emptyRows > 0 && values.page > 0 && (
                    <TableRow style={{ height: 50 * emptyRows }}>
                        <TableCell colSpan={props.headers.length} />
                    </TableRow>
                )}
            </TableBody>
            <TableFooter>
                <TableRow>
                    <TablePagination
                        rowsPerPageOptions={[5, 10, 50, 100, { label: 'Összes', value: -1 }]}
                        colSpan={props.headers.length}
                        labelDisplayedRows={({ from, to, count }) => `${from}-${to}/${count}`}
                        count={props.size}
                        rowsPerPage={parseInt(values.perPage)}
                        page={parseInt(values.page)}
                        labelRowsPerPage="Sorok száma:"
                        onPageChange={(event, newPage) => pagingChangedHandler(newPage)}
                        onRowsPerPageChange={(event) => pagingChangedHandler(0, parseInt(event.target.value, 10))}
                        ActionsComponent={TablePaginationAction}
                    />
                </TableRow>
            </TableFooter>
        </Table>
    )
}

export default SortedTableWithPagination
