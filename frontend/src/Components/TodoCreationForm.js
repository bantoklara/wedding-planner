import React from 'react'

import { Box, Button, MenuItem, TextField } from '@mui/material'

import { useFormHandlers } from '../Services/FormHandlers/TodoFormHandler';

const TodoCreationForm = ({ update, groupedTodos, setGroupedTodos, categories }) => {
    const {
        values,
        errors,
        loading,
        inputCHangeHandler,
        submitFormHandler
    } = useFormHandlers(update, groupedTodos, setGroupedTodos, categories);

    return (
        <form
            onSubmit={submitFormHandler}
        >
            <Box display='flex' gap={1} alignItems='center'>
                <TextField
                    required
                    multiline
                    size="small"
                    label="Leírás"
                    name="description"
                    style={{ width: '40%' }}
                    value={values.description}
                    onChange={inputCHangeHandler}
                    {...(errors.description && {
                        error: true,
                        helperText: errors.description
                    })}
                />
                <TextField
                    required
                    size="small"
                    select
                    label="Határidő"
                    name="deadline"
                    value={values.deadline}
                    onChange={inputCHangeHandler}
                    style={{ width: '25%' }}
                >
                    {groupedTodos.map(({ deadline, deadlineText }) => (
                        <MenuItem
                            key={deadline}
                            value={deadline}
                        >
                            {deadlineText}
                        </MenuItem>
                    ))}
                </TextField>
                <TextField
                    required
                    size="small"
                    select
                    label="Kategória"
                    name="categoryId"
                    value={values.category.id}
                    style={{ width: '25%' }}
                    onChange={inputCHangeHandler}
                >
                    {categories.map(({ id, name }) => (
                        <MenuItem key={id} value={id}>
                            {name}
                        </MenuItem>
                    ))}
                </TextField>
                <Button
                    size='medium'
                    type="submit"
                    color="secondary"
                    sx={{ color: '#ffffff' }}
                    variant='contained'
                    style={{ width: '10%', height: '40px' }}>

                    Mentés {loading && '...'}
                </Button>
            </Box>
        </form>
    )
}

export default TodoCreationForm
