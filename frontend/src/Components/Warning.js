import React, { useEffect } from 'react'

import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material'

const Warning = (props) => {
    useEffect(() => {
        if (props.loading === 0)
            props.setDialogOpen(false);
    }, [props.loading]);
    return (
        <Dialog open={props.dialogOpen} onClose={() => props.setDialogOpen(false)} fullWidth maxWidth='xs'>
            <DialogTitle>
                {props.message}
            </DialogTitle>
            <DialogActions>
                <Button
                    onClick={() => props.setDialogOpen(false)}
                >{props.cancelMessage}</Button>
                <Button
                    onClick={() => props.action(props.params ? props.params : {})}>{props.continueMessage} {props.loading === 1 && '...'}</Button>
            </DialogActions>
        </Dialog>
    )
}

export default Warning
