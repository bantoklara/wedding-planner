import React, { useState, useContext } from 'react'
import { useNavigate } from 'react-router-dom';

import { Alert, Box, Button, Dialog, DialogContent, DialogTitle, Stack, Typography } from '@mui/material'

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import useStyles from '../Styles/CodeInputStyle';
import AuthenticationHandler from '../Services/AuthenticationService';
import UserContext from '../UserContext';

const WeddingGuestLoginForm = ({ dialogOpen, setDialogOpen }) => {
    const [loginCode, setLoginCode] = useState(new Array(8).fill(''));
    const [pending, setPending] = useState(false);
    const [error, setError] = useState('');
    const classes = useStyles();
    const { loginAsGuest, getUserInfoFromToken } = AuthenticationHandler();
    const navigate = useNavigate();
    const { user, setUser } = useContext(UserContext);

    const inputChangeHandler = (event, index) => {
        const { value, nextSibling } = event.target;
        setLoginCode([...loginCode.map((c, idx) => idx === index ? value : c)]);
        if (nextSibling)
            nextSibling.focus();
    }

    const submitFormHandler = async (event) => {
        event.preventDefault();
        setPending(true);
        let code = '';
        loginCode.forEach(c => code += c)
        const token = await loginAsGuest({ "loginCode": code });
        if (token) {
            setUser(getUserInfoFromToken(token));
            navigate("/");
        }
        setError('A megadott azonosito helytelen!');
        setPending(false);
    }

    return (
        <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
            <DialogTitle justifyContent='center'>Ha azért van itt, hogy visszajelezzen egy esküvőre, ne kattintson el</DialogTitle>
            <DialogContent>
                {error && <Alert severity='error'>{error}</Alert>}
                <form onSubmit={submitFormHandler}>
                    <Stack spacing={2}>
                        <Typography>Ide írja be az azonosítót, melyet a meghívóban kapott</Typography>
                        <Box display='flex' gap='2px'>
                            {loginCode.map((character, index) => (
                                <input
                                    value={character}
                                    key={index}
                                    onChange={e => inputChangeHandler(e, index)}
                                    onFocus={e => e.target.select()}
                                    maxLength='1'
                                    className={classes.input}
                                />
                            ))}
                        </Box>
                        <Button
                            type="submit"
                            color="secondary"
                            variant='contained'
                            sx={{ color: 'white', width: 'fit-content' }}
                            endIcon={<ArrowForwardIcon />}
                        >
                            Tovább {pending && '...'}</Button>
                    </Stack>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default WeddingGuestLoginForm
