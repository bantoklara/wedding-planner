import React from 'react'
import { Draggable } from 'react-beautiful-dnd'

import { Box, Typography } from '@mui/material';

import PersonIcon from '@mui/icons-material/Person';

const DraggableList = (props) => {
    return (
        <div>
            {Object.entries(props.guests).map(([guestId, { guest }], index) => {
                return (
                    <Draggable
                        draggableId={`${guestId.toString()}-0`}
                        key={guestId}
                        index={index}
                    >
                        {(provided) => {
                            return (
                                <div
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                >
                                    <Box display='flex' alignItems='center'>
                                        <Typography>{guest.lastName} {guest.firstName} </Typography>
                                        <PersonIcon color='primary' />
                                        <Typography> {guest.numberOfGuests}</Typography>
                                    </Box>
                                </div>
                            )
                        }}
                    </Draggable>
                )
            })}
            {props.children}
        </div>
    )
}

export default DraggableList
