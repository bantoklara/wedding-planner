import React, { useState } from 'react'
import { Draggable, Droppable } from 'react-beautiful-dnd';

import { Box, IconButton, Tooltip, Typography } from '@mui/material';

import DeleteIcon from '@mui/icons-material/Delete';
import CloseIcon from '@mui/icons-material/Close';
import PersonIcon from '@mui/icons-material/Person';

import useStyles from '../../Styles/TableStyle';
import useTableService from '../../Services/TableService';
import Warning from '../Warning';
import theme from '../../CustomTheme'

const DraggableTable = (props) => {

    const containerWith = 200;
    const r = 100;
    const [onHover, setOnHover] = useState(-1);
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(-1);
    const [tableOnHover, setTableOnHover] = useState(false);
    const classes = useStyles();

    const { deleteTable } = useTableService();

    const coordinate = (angle) => {
        return { x: r * Math.cos(angle), y: r * Math.sin(angle) };
    };

    const marginTop = (index, seatHeight) => {
        const angle = 2 * Math.PI - (index + 1) * (2 * Math.PI) / props.numberOfSeats;
        const { y } = coordinate(angle);
        return (y + r - seatHeight / 2) * 100 / containerWith;
    }

    const margintLeft = (index, seatWidth) => {
        const angle = 2 * Math.PI - (index + 1) * (2 * Math.PI) / props.numberOfSeats;
        const { x } = coordinate(angle);
        return (x + r - seatWidth / 2) * 100 / containerWith;
    }

    const seatHovered = (hovered, tableId, guestId, seatIndex, index, width) => {
        setOnHover(hovered ? seatIndex : -1);
    }

    const removeGuestFromTable = (guestId) => {
        props.dispatch({
            type: 'MOVE',
            from: props.tableId,
            to: '0',
            fromIndex: guestId
        });
        props.saveGuestsTable(guestId, 0);
    }

    const deleteTableHandler = () => {
        setLoading(1);
        deleteTable(parseInt(props.tableId));
        props.dispatch({
            type: 'DELETE_TABLE',
            tableId: props.tableId
        })
        setLoading(0);
    }

    const triggerPopup = () => {
        setLoading(-1);
        setButtonPopup(true);
    };

    return (
        <>
            <Warning
                dialogOpen={buttonPopup}
                setDialogOpen={setButtonPopup}
                message='Biztosan törli az asztalt?'
                cancelMessage='Mégsem'
                continueMessage='Asztal törlése'
                action={deleteTableHandler}
                loading={loading}
            />
            <div
                className={classes.container}
                onMouseEnter={() => setTableOnHover(true)}
                onMouseLeave={() => setTableOnHover(false)}
            >
                {tableOnHover &&
                    <IconButton
                        onClick={triggerPopup}
                        className={classes.deleteTableButton}
                    ><DeleteIcon color='error' /></IconButton>
                }
                <div
                    className={classes.table}
                    style={{
                        borderWidth: props.tableOnHover === props.tableId ? '3px' : '1px',
                        borderColor: props.tableOnHover === props.tableId ? theme.palette.secondary.main : 'black',
                        width: containerWith + 'px',
                        height: containerWith + 'px',
                    }}>
                    <div className={classes.tableName}>
                        <b>{props.name}</b></div>
                    {Object.entries(props.guests).map(([guestId, { guest, seats }]) => {
                        return (
                            <div
                                key={guestId}
                            >
                                {seats.map(({ index, width, height }, ind) => {
                                    return (
                                        <Droppable
                                            isDropDisabled={true}
                                            droppableId={`${props.tableId}-${index}`}
                                            key={`${guestId}-${index}`}
                                            type='GUEST'
                                        >
                                            {(provided) => {
                                                return (
                                                    <div
                                                        ref={provided.innerRef}
                                                        {...provided.droppableProps}
                                                        className={classes.seats}
                                                        style={{
                                                            width: width,
                                                            height: height,
                                                            top: marginTop(index, height) + '%',
                                                            left: margintLeft(index, width) + '%',
                                                        }}
                                                    >{guestId != -1
                                                        && <Draggable
                                                            draggableId={`${guestId}-${index}`}
                                                            index={index}
                                                        >
                                                            {(provided) => {
                                                                return (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div
                                                                            onMouseEnter={() => { guestId != -1 && seatHovered(true, props.tableId, guestId, index, ind, 150) }}
                                                                            onMouseLeave={() => { guestId != -1 && seatHovered(false, props.tableId, guestId, index, ind, 30) }}
                                                                            style={{
                                                                                margin: '-40px 0 -30px 0',
                                                                                display: 'inline-block',
                                                                                whiteSpace: 'no-wrap',
                                                                                height: height,
                                                                                width: width,
                                                                                textAlign: 'center'
                                                                            }}
                                                                        >{((props.onDrag.guestId != guestId) || (props.onDrag.guestId == guestId && props.onDrag.seatIndex == index))
                                                                            && <div className={classes.nameContainer}>
                                                                                <Tooltip
                                                                                    className={classes.tooltip}
                                                                                    title={
                                                                                        <Box display='flex' alignContent='center'>
                                                                                            <Typography>{`${guest.lastName} ${guest.firstName}`}</Typography>
                                                                                            <PersonIcon />
                                                                                            <Typography>{guest.numberOfGuests}</Typography>
                                                                                        </Box>
                                                                                    }>
                                                                                    <Typography className={classes.guestNameAbbr}>{guest.lastName.charAt(0)} {guest.firstName.charAt(0)}</Typography>
                                                                                </Tooltip>
                                                                                {onHover === index &&
                                                                                    <IconButton

                                                                                        size='small'
                                                                                        style={{
                                                                                            position: 'absolute',
                                                                                            top: '-20px',
                                                                                            right: '-15px',
                                                                                            borderRadius: '50%'
                                                                                        }}
                                                                                        onClick={() => removeGuestFromTable(guestId)}><CloseIcon fontSize='inherit' color='error' />
                                                                                    </IconButton>
                                                                                }
                                                                            </div>
                                                                            }
                                                                        </div>

                                                                    </div>
                                                                )
                                                            }}
                                                        </Draggable>
                                                        }
                                                        {provided.placeholder}
                                                        {props.children}
                                                    </div>
                                                )
                                            }}
                                        </Droppable>
                                    )
                                })}
                            </div>
                        )
                    })}
                </div >
            </div >
        </>
    )
}

export default DraggableTable
