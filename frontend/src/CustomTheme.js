import { createTheme } from "@mui/material/styles";
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';

import CheckIcon from '@mui/icons-material/Check';
import CheckBoxOutlinedIcon from '@mui/icons-material/CheckBoxOutlined';

const theme = createTheme({
    components: {
        MuiButton: {
            styleOverrides: {
                root: {
                    textTransform: 'none',
                    color: "black",
                }
            }
        },
        MuiTooltip: {
            styleOverrides: {
                tooltip: {
                    backgroundColor: '#faded4',
                    color: 'black'
                }
            }
        },
        MuiRadio: {
            defaultProps: {
                icon: < RadioButtonUncheckedIcon sx={{ opacity: '0' }} />,
                checkedIcon: < CheckIcon color='secondary' />
            },
        },
        MuiCheckbox: {
            defaultProps: {
                checkedIcon: <CheckBoxOutlinedIcon color="secondary" />
            }
        },
        MuiContainer: {
            defaultProps: {
                disableGutters: 'true',
                maxWidth: 'false'
            },
        },
        MuiPaper: {
            styleOverrides: {
                root: {
                    padding: '10px'
                }
            }
        },
        MuiToolbar: {
            styleOverrides: {
                dense: {
                    height: '50px',
                    minHeight: '50px'
                }
            }
        },
        MuiTextField: {
            styleOverrides: {
                root: {
                    margin: '5px'
                }
            },
            defaultProps: {
                size: 'small',
                color: 'info'
            }
        },
        MuiFormLabel: {
            styleOverrides: {
                root: {
                    margin: '5px',
                    color: 'black'
                }
            }
        }
    },
    palette: {
        primary: {
            main: '#faded4'
        },
        secondary: {
            main: '#69837a'
        },
        info: {
            main: '#000000'
        }
    },
    typography: {
        fontFamily: [
            'Aleo Light'
        ].join(','),
        color: 'black',
        h1: {
            fontWeight: 500,
            fontSize: "20px",
            lineHeight: 1.429
        },
        h2: {
            fontWeight: 500,
            fontSize: "18px",
            lineHeight: 1.429
        },
        h3: {
            fontWeight: 'bold',
            fontSize: "18px",
            lineHeight: 1.429
        },
    }
});

export default theme;