import React, { useEffect, useState } from 'react'
import { useParams, useSearchParams } from 'react-router-dom';

import { Box, Container, Grid, Paper, Stack } from '@mui/material';

import QueryBuilderIcon from '@mui/icons-material/QueryBuilder';
import PermIdentityIcon from '@mui/icons-material/PermIdentity';
import MailOutlineIcon from '@mui/icons-material/MailOutline';
import LocalPhoneOutlinedIcon from '@mui/icons-material/LocalPhoneOutlined';

import useBookedServiceService from '../Services/BookedServiceService';
import useSharedFileService from '../Services/SharedFileService';
import useFilterPagingAndSortHandler from '../Services/FilterAndSortHandler/useFilterPagingAndSortHandler';
import SharedFiles from '../Components/SharedFiles';

const BookedServiceDetailPage = () => {
    const { id } = useParams();
    const [bookedService, setBookedService] = useState(null);
    const [sharedFiles, setSharedFiles] = useState(null);
    const [size, setSize] = useState(null);
    const [count, setCount] = useState(0);

    const [searchParams] = useSearchParams();

    const { getShareFiles } = useSharedFileService(id);
    const { getById } = useBookedServiceService();

    const {
        values,
        getRequestParams,
        orderHasChanged,
        sortByChangeHandler,
        pagingChangedHandler }
        = useFilterPagingAndSortHandler(`/booked-services/${id}/shared-files`, searchParams, {});

    useEffect(() => {
        const apiCall = async () => {
            const response = await getShareFiles(getRequestParams());
            setSharedFiles(response.sharedFiles);
            setSize(response.size);
        };

        apiCall();
    }, [searchParams, count]);

    useEffect(() => {
        const apiCall = async () => {
            const bookedServiceDetails = await getById(id);
            setBookedService(bookedServiceDetails);
        };

        apiCall();
    }, [bookedService]);

    const getInfo = (role) => {
        if (role === 'couple')
            return [
                {
                    icon: <QueryBuilderIcon />,
                    title: 'Esküvő dátuma',
                    text: bookedService.wedding.dateOfWedding
                },
                {
                    icon: <PermIdentityIcon />,
                    title: 'Név',
                    text: `${bookedService.wedding.user.lastName} ${bookedService.wedding.user.firstName}`
                },
                {
                    icon: <MailOutlineIcon />,
                    title: 'Email',
                    text: bookedService.wedding.user.email
                },
                {
                    icon: <LocalPhoneOutlinedIcon />,
                    title: 'Telefonszám',
                    text: bookedService.wedding.user.phoneNumber
                },
            ];
        if (role === 'provider')
            return [
                {
                    icon: <PermIdentityIcon />,
                    title: 'Név',
                    text: `${bookedService.service.user.lastName} ${bookedService.service.user.firstName}`
                },
                {
                    icon: <MailOutlineIcon />,
                    title: 'Email',
                    text: bookedService.service.user.email
                },
                {
                    icon: <LocalPhoneOutlinedIcon />,
                    title: 'Telefonszám',
                    text: bookedService.service.user.phoneNumber
                },
            ]

        return [
            {
                icon: <></>,
                title: 'Név',
                text: bookedService.service.name,
            },
            {
                icon: <></>,
                title: 'Kategória',
                text: bookedService.service.category.name,
            },
            {
                icon: <></>,
                title: 'Ár',
                text: `${bookedService.price} lej`,
            },
        ]
    }

    return (
        <Container>
            {bookedService
                ?
                <Box>
                    <Stack spacing={4}>
                        <Paper>
                            <Grid container justifyContent='center' alignItems='center' >
                                <Grid item xs={12} md={12} lg={4}>
                                    <h3>Ügyfél</h3>
                                    <Box paddingLeft='15px'>
                                        {getInfo('couple').map(({ icon, title, text }, index) => (
                                            <Box display='flex' gap={1} alignItems='center' key={index} >
                                                {icon}
                                                <p><b>{title}:</b> {text}</p>
                                            </Box>
                                        ))}
                                    </Box>
                                </Grid>
                                <Grid item xs={12} md={12} lg={4}>
                                    <h3>Szolgáltató</h3>
                                    <Box paddingLeft='15px'>
                                        {getInfo('provider').map(({ icon, title, text }, index) => (
                                            <Box display='flex' gap={1} alignItems='center' key={index} >
                                                {icon}
                                                <p><b>{title}:</b> {text}</p>
                                            </Box>
                                        ))}
                                    </Box>
                                </Grid>
                                <Grid item xs={12} md={12} lg={4}>
                                    <h3>Szolgáltatás</h3>
                                    <Box paddingLeft='15px'>
                                        {getInfo('service').map(({ icon, title, text }, index) => (
                                            <Box display='flex' gap={1} alignItems='center' key={index} >
                                                {icon}
                                                <p><b>{title}:</b> {text}</p>
                                            </Box>
                                        ))}
                                    </Box>
                                </Grid>
                            </Grid>
                        </Paper>
                        <Paper>
                            {sharedFiles != null && size != null
                                ? <SharedFiles
                                    bookedServiceId={bookedService.id}
                                    sharedFiles={sharedFiles}
                                    values={values}
                                    orderHasChanged={orderHasChanged}
                                    sortByChangeHandler={sortByChangeHandler}
                                    pagingChangedHandler={pagingChangedHandler}
                                    size={size}
                                    setCount={setCount}
                                />
                                : <div>Loading...</div>
                            }
                        </Paper>
                    </Stack>
                </Box>
                : <div>Loading...</div>
            }
        </Container >
    )
}

export default BookedServiceDetailPage
