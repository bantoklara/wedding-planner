import React, { useEffect, useState, useContext } from 'react'
import { useSearchParams } from 'react-router-dom';

import { FormControlLabel, Paper, Radio, RadioGroup } from '@mui/material';
import { makeStyles } from '@mui/styles';

import useBookedServiceService from '../Services/BookedServiceService';
import useFilterPagingAndSortHandler from '../Services/FilterAndSortHandler/useFilterPagingAndSortHandler';
import UserContext from '../UserContext';
import BookedServiceTableBody from '../Components/BookedServiceTableBody';
import SortedTableWithPagination from '../Components/SortedTableWithPagination';

const useStyles = makeStyles(() => ({
    formControlLabel: {
        padding: '3px',
        borderRadius: '10px',
    },
    active: {
        border: '3px solid #69837a'
    },
    inactive: {
        border: 'none'
    }
}));

const headers = {
    ROLE_COUPLE: [
        {
            name: 'providerName',
            label: 'Szolgáltató neve',
            align: 'left',
            width: '20%',
            sortable: true,
        },
        {
            name: 'email',
            label: 'Emailcím',
            align: 'left',
            width: '20%',
            sortable: false,
        },
        {
            name: 'serviceName',
            label: 'Szolgáltatás neve',
            align: 'left',
            width: '20%',
            sortable: true,
        },
        {
            name: 'category',
            label: 'Kategória',
            align: 'left',
            width: '20%',
            sortable: true,
        },
        {
            name: 'actionButtons',
            label: '',
            align: 'right',
            width: '20%',
            sortable: false,
        },
    ],
    ROLE_SERVICE_PROVIDER: [
        {
            name: 'coupleName',
            label: 'Név',
            align: 'left',
            width: '17%',
            sortable: true,
        },
        {
            name: 'email',
            label: 'Email',
            align: 'left',
            width: '17%',
            sortable: false,
        },
        {
            name: 'dateOfWedding',
            label: 'Esküvő dátuma',
            align: 'left',
            width: '17%',
            sortable: true,
        },
        {
            name: 'serviceName',
            label: 'Szolgáltatás neve',
            align: 'left',
            width: '17%',
            sortable: true,
        },
        {
            name: 'category',
            label: 'Kategória',
            align: 'left',
            width: '17%',
            sortable: true,
        },
        {
            name: 'actionButtons',
            label: '',
            align: 'right',
            width: '15%',
            sortable: false,
        },
    ]
};

const initialFilterValues = {
    status: 'ACCEPTED',
};

const BookedServiceListPage = () => {
    const { user } = useContext(UserContext);

    const [bookedServices, setBokedServices] = useState(null);
    const [size, setSize] = useState(null);
    const [searchParams] = useSearchParams();

    const {
        values,
        getRequestParams,
        orderHasChanged,
        filterCHangeHandler,
        sortByChangeHandler,
        pagingChangedHandler }
        = useFilterPagingAndSortHandler("/booked-services", searchParams, initialFilterValues);

    const classes = useStyles();
    const { findBookedServicesOfUser } = useBookedServiceService();

    useEffect(() => {
        const apiCall = async () => {
            const resp = await findBookedServicesOfUser(getRequestParams());
            setBokedServices(resp.bookedServices);
            setSize(resp.size);
        };

        apiCall();
    }, [searchParams]);

    return (
        <Paper sx={{ padding: '20px' }}>
            {bookedServices !== null && size !== null
                ? <>
                    <RadioGroup
                        row
                        name="status"
                        value={values.status}
                        onChange={filterCHangeHandler}
                    >
                        <FormControlLabel
                            className={`${classes.formControlLabel} ${values.status === 'ACCEPTED' ? classes.active : classes.inactive}`}
                            value='ACCEPTED'
                            control={<Radio />}
                            label='Elfogadva' />
                        <FormControlLabel
                            className={`${classes.formControlLabel} ${values.status === 'PENDING' ? classes.active : classes.inactive}`}
                            value='PENDING'
                            control={<Radio />}
                            label='Függőben' />
                    </RadioGroup>
                    <SortedTableWithPagination
                        headers={headers[user.role]}
                        values={values}
                        sortByChangeHandler={sortByChangeHandler}
                        pagingChangedHandler={pagingChangedHandler}
                        orderHasChanged={orderHasChanged}
                        size={size}
                    >
                        <BookedServiceTableBody bookedServices={bookedServices} status={values.status} role={user.role} />

                    </SortedTableWithPagination>
                </>
                : <div>Loading...</div>
            }
        </Paper >
    )
}

export default BookedServiceListPage
