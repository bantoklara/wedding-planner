import React, { useState, useEffect } from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import timeGridPlugin from '@fullcalendar/timegrid'
import interactionPlugin from '@fullcalendar/interaction'
import useScheduleService from '../Services/ScheduleService'

import { Badge, Box, IconButton, Tooltip, Typography } from '@mui/material'

import CalendarMonthIcon from '@mui/icons-material/CalendarMonth';
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';

import { useWeddingServiceService } from '../Services/WeddingServiceService'
import useStyles from '../Styles/IconButtonStyle'
import ScheduleCreation from '../Components/ScheduleCreation'
import ScheduleModification from '../Components/ScheduleModification'

const Calendar = () => {
    const [schedules, setSchedules] = useState(null);
    const [services, setServices] = useState(null);
    const [inputFields, setInputFields] = useState(null);
    const [selectedEvent, setSelectedEvent] = useState(null);

    const [createButtonClicked, setCreateButtonClicked] = useState(false);
    const [eventCliked, setEventCliked] = useState(false);

    const classes = useStyles();

    const { getAllServicesOfUser } = useWeddingServiceService();
    const { getAllSchedulesOfUser } = useScheduleService();

    useEffect(() => {
        const apiCall = async () => {
            const scheduleList = await getAllSchedulesOfUser();

            scheduleList.forEach((schedule) => {
                const start = new Date(String(schedule.start));
                const end = new Date(String(schedule.end));
                const startTime = start.getHours() + start.getMinutes();
                const endTime = end.getHours() + end.getMinutes();

                schedule.allDay = startTime === endTime && endTime === 0;

            });

            setSchedules(scheduleList);
            const serviceList = await getAllServicesOfUser();
            setServices(serviceList);
        };
        apiCall();
    }, [])

    const handleDateSelected = (info) => {
        const offset = new Date(info.start).getTimezoneOffset();
        const start = new Date(info.start);
        const end = new Date(info.end);

        const startStr = new Date(start.getTime() - (offset * 60 * 1000)).toISOString().split('.')[0];
        const endStr = new Date(end.getTime() - (offset * 60 * 1000)).toISOString().split('.')[0];

        setInputFields({ start: startStr, end: endStr });
        setCreateButtonClicked(true);
    };

    const handleEventClicked = (info) => {
        const scheduleId = info.event._def.publicId;
        setSelectedEvent(schedules.filter((schedule) => schedule.id === parseInt(scheduleId))[0]);
        setEventCliked(true);
    };

    return (
        <>{schedules && services
            ? <>
                <Box marginBottom='20px'>
                    <Tooltip title='Új időpont megadása'>
                        <IconButton onClick={() => setCreateButtonClicked(true)} className={classes.addTableButton}>
                            <Badge badgeContent={<AddCircleRoundedIcon color='primary' />} sx={{ padding: '0' }}>
                                <CalendarMonthIcon fontSize='large' color='primary' />
                            </Badge>
                        </IconButton>
                    </Tooltip>
                </Box>
                <ScheduleCreation
                    dialogOpen={createButtonClicked}
                    setDialogOpen={setCreateButtonClicked}
                    services={services}
                    setSchedules={setSchedules}
                    setButtonPopup={setCreateButtonClicked}
                    inputFields={inputFields}
                />
                <ScheduleModification
                    dialogOpen={eventCliked}
                    setDialogOpen={setEventCliked}
                    services={services}
                    setSchedules={setSchedules}
                    schedule={selectedEvent}
                />

                <FullCalendar
                    plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                    initialView='dayGridMonth'
                    events={schedules}
                    eventClick={handleEventClicked}
                    selectable
                    select={handleDateSelected}
                    headerToolbar={{
                        start: 'prev,next',
                        center: 'title',
                        end: 'dayGridMonth,timeGridWeek,timeGridDay',
                    }}
                    eventTimeFormat={{
                        hour: '2-digit',
                        minute: '2-digit',
                        meridiem: 'false'
                    }}
                    eventBackgroundColor='#69837a'
                    eventTextColor='#000000'
                    height='90vh'
                    locale='hu'
                    buttonText={{
                        today: 'ma',
                        month: 'Hónap',
                        week: 'Hét',
                        day: 'Nap'
                    }}
                    displayEventEnd='true'
                />
            </>
            : <Typography>Loading...</Typography>
        }
        </>
    )
}

export default Calendar
