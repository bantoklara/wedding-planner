import { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";

import { useMenuService } from "../Services/MenuService";
import { useGuestService } from "../Services/GuestService";
import { useGuestGroupService } from "../Services/GuestGroupService";
import GuestUpdatingForm from "../Components/GuestUpdatingForm";

const GuestDetailsPage = () => {
    const { id } = useParams()
    const [guest, setGuest] = useState(null);
    const [menus, setMenus] = useState(null);
    const [guestGroups, setGuestGroups] = useState(null);

    const { getGuestDetail } = useGuestService();
    const { getAllGuestGroups } = useGuestGroupService();
    const { getAllMenus } = useMenuService();

    useEffect(() => {
        const apiCall = async () => {
            const guestDetails = await getGuestDetail(id);
            setGuest(guestDetails);


            const menuList = await getAllMenus();
            setMenus(menuList);
        }
        apiCall();
    }, []);

    useEffect(() => {
        const apiCall = async () => {
            const guestGroupsList = await getAllGuestGroups();
            setGuestGroups(guestGroupsList);
        }
        apiCall();
    }, [])

    return (
        <div>
            {guest && menus && guestGroups
                ? <GuestUpdatingForm
                    guest={guest}
                    setGuest={setGuest}
                    guestGroups={guestGroups}
                    menus={menus}
                    withRedirect={true} />
                : <div>Loading...</div>}
        </div>
    )
}

export default GuestDetailsPage;