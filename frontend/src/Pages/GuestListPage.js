import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";

import { Box, Button, Container, Paper, Stack, Typography } from "@mui/material";

import DownloadIcon from '@mui/icons-material/Download';

import { useGuestGroupService } from "../Services/GuestGroupService";
import { useGuestService } from "../Services/GuestService";
import WeddingGuestCreationForm from "../Components/GuestCreationForm";
import WeddingGuestsTable from "../Components/GuestsTable";
import ExcelColumnOptionsForm from "../Components/ExcelColumnOptionsForm";
import GuestFilter from "../Components/GuestFilter";
import useFilterPagingAndSortHandler from "../Services/FilterAndSortHandler/useFilterPagingAndSortHandler";

const initialFilterValues = {
    name: '',
    feedback: '',
};

const GuestList = () => {
    const [guests, setGuests] = useState(null);
    const [count, setCount] = useState(0);
    const [guestGroups, setGuestGroups] = useState(null);
    const [loading, setLoading] = useState(false);
    const [buttonPopup, setButtonPopup] = useState(false);
    const { filterGuests, generateCodes } = useGuestService();
    const { getAllGuestGroups } = useGuestGroupService();
    const [searchParams] = useSearchParams();
    const [numberOfAllGuests, setNumberOfAllGuests] = useState('');
    const [numberOfFilteredGuests, setNumberOfFilteredGuests] = useState('');
    const [numberOfGuestWithFeedback1, setNumberOfGuestWithFeedback1] = useState('');
    const [numberOfGuestWithFeedback2, setNumberOfGuestWithFeedback2] = useState('');
    const [numberOfGuestWithFeedback3, setNumberOfGuestWithFeedback3] = useState('');

    const {
        values,
        getRequestParams,
        orderHasChanged,
        filterCHangeHandler,
        sortByChangeHandler,
        pagingChangedHandler }
        = useFilterPagingAndSortHandler("/guests", searchParams, initialFilterValues);

    const generateLoginCodes = async () => {
        setLoading(true);
        const success = await generateCodes();
        setLoading(false);
    };

    const setValues = (response) => {
        setGuests(response.guests);
        setNumberOfAllGuests(response.numberOfAllGuests);
        setNumberOfFilteredGuests(response.numberOfFilteredGuests);
        setNumberOfGuestWithFeedback1(response.numberOfGuestWithFeedback1);
        setNumberOfGuestWithFeedback2(response.numberOfGuestWithFeedback2);
        setNumberOfGuestWithFeedback3(response.numberOfGuestWithFeedback3);
    };

    useEffect(() => {
        console.log('HERE in useffect');
        const apiCall = async () => {
            const response = await filterGuests(getRequestParams());
            setValues(response);
        };

        apiCall();
    }, [searchParams, count])


    useEffect(() => {
        const apiCall = async () => {
            const guestGroupsList = await getAllGuestGroups();
            setGuestGroups(guestGroupsList);
        };

        apiCall();
    }, [])

    return (
        <>
            {guests && guestGroups ?
                <Stack spacing={4}>
                    <Paper>
                        <GuestFilter
                            values={values}
                            inputChangedHandler={filterCHangeHandler}
                            numberOfGuestWithFeedback1={numberOfGuestWithFeedback1}
                            numberOfGuestWithFeedback2={numberOfGuestWithFeedback2}
                            numberOfGuestWithFeedback3={numberOfGuestWithFeedback3}
                            numberOfAllGuests={numberOfAllGuests}
                        />
                        {/* wedding guests */}
                        <WeddingGuestsTable
                            guests={guests}
                            numberOfFilteredGuests={numberOfFilteredGuests}
                            values={values}
                            orderHasChanged={orderHasChanged}
                            sortByChangeHandler={sortByChangeHandler}
                            pagingChangedHandler={pagingChangedHandler}
                            setCount={setCount}
                        />
                        <Box display='flex' gap={2}>
                            <Button
                                variant='contained'
                                onClick={generateLoginCodes}
                            >
                                Bejelentkező kódok generálása {loading && '...'}
                            </Button>
                            <Button
                                variant='contained'
                                startIcon={<DownloadIcon />}
                                color='secondary'
                                sx={{ color: '#ffffff' }}
                                onClick={() => setButtonPopup(true)}
                            >
                                Vendéglista letöltése
                            </Button>
                            <ExcelColumnOptionsForm dialogOpen={buttonPopup} setDialogOpen={setButtonPopup} />
                        </Box>
                    </Paper>
                    <Paper>
                        <Typography variant='h3'>Új vendég hozzáadása</Typography>
                        {/* creating new guest */}
                        <WeddingGuestCreationForm
                            guestGroups={guestGroups}
                            setCount={setCount}
                        />
                    </Paper>
                </Stack> : <div>Loading...</div>

            }
        </>
    )
}

export default GuestList;