import { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import { Container, Grid, List, ListItem, ListItemIcon, ListItemText } from "@mui/material";

import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import useStyles from "../Styles/HomeStyle";
import coupleImage from '../img/couple.png'
import providerImage from '../img/provider.png';
import UserContext from "../UserContext";
import HomePageForCouple from "./HomePageForCouple";
import HomePageForWeddingGuest from "./HomePageForWeddingGuest";
import WeddingGuestLoginForm from "../Components/WeddingGuestLoginForm";

const paragraphs = {
    wedding: [
        'Az oldalon lehetősége van összeírni a vendéglistát, így minden vendég adatai egy helyen lesznek és ezt bármikor módosíthatja.',
        'A vendéglistába bevitt vendégek vissza tudnak jelezni részvételüket illetően, így önnek nem kell a sok visszaigazoló hívást fogadnia. Sőt, a vendégek azt is megadhatják, hogy milyen menüt szeretnének az esküvőn fogyasztani és, hogy hány fővel érkeznek.',
        'Az összeállított vendéglista alapján az oldal lehetőséget nyújt az ültetési rend megtervezésében is.',
        'Ezenkívül az elvégezendő feladatok listáját is nyomonkövetheti az alkalmazásban.',
        'Ha még nem találta meg a megfelelő éttermet, fotóst vagy más esküvői szolgáltatót, oldalunkon böngészhet az elérhető szolgáltatások között, akár azt is megnézheti, hogy mely szolgáltatók szabadok az ön nagy napján.'
    ],
    provider: [
        'Szolgáltatóként regisztrálva feltöltheti szolgáltatásait az oldalunkra.',
        'Mielőtt a szolgáltatása megjelenne más felhasználóknak, az adminisztrátor ezt jóvá kell hagyja.',
        'Nyomonkövetheti, hogy kik foglalták le szolgáltatásait.',
        'Az oldalon lehetősége van egy naptárban bejelölni a foglalások időpontjait.'
    ]
}
const Home = () => {
    const { user } = useContext(UserContext);
    const [dialogOpen, setDialogOpen] = useState(false);
    const navigate = useNavigate();
    const classes = useStyles();

    useEffect(() => {
        if (user?.role === 'ROLE_SERVICE_PROVIDER')
            navigate('/calendar');

        if (!user)
            setTimeout(() => {
                setDialogOpen(true);
            }, 2000)
    }, [user])

    const ListItemContent = ({ text }) => {
        return (
            <ListItem>
                <ListItemIcon ><ArrowForwardIcon color='secondary' /></ListItemIcon>
                <ListItemText>{text}</ListItemText>
            </ListItem>
        )
    }
    return (
        <>
            {!user &&
                <Container>
                    <WeddingGuestLoginForm dialogOpen={dialogOpen} setDialogOpen={setDialogOpen} />
                    <Grid container>
                        <Grid item lg={4}>
                            <img src={coupleImage} height='70%' />
                        </Grid>
                        <Grid item lg={8}>
                            <h3>Épp az esküvőjét szervezi?</h3>
                            <List>
                                {paragraphs.wedding.map((text, index) => (
                                    <ListItemContent text={text} key={index} />
                                ))}
                            </List>
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item lg={7}>

                            <h3>Ön egy esküvői szolgáltató?</h3>
                            <List>
                                {paragraphs.provider.map((text, index) => (
                                    <ListItemContent text={text} key={index} />
                                ))}
                            </List>
                        </Grid>
                        <Grid item lg={5}>
                            <img src={providerImage} width='80%' height='90%' />
                        </Grid>
                    </Grid>
                </Container>
            }
            {user?.role === "ROLE_COUPLE" && <HomePageForCouple />}
            {user?.role === "ROLE_WEDDING_GUEST" && <HomePageForWeddingGuest />}
        </>
    )
}

export default Home;