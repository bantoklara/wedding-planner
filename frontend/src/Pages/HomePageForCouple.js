import { useEffect, useState, useContext } from "react";
import { Box, Button, CircularProgress, Container, Grid, Paper, Stack, Typography } from "@mui/material";

import QueryBuilderIcon from '@mui/icons-material/QueryBuilder';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';

import { useWeddingService } from "../Services/WeddingService";
import UserContext from "../UserContext";
import { useNavigate } from "react-router-dom";

const HomePageForCouple = () => {
    const { user, setUser } = useContext(UserContext);
    const navigate = useNavigate();
    const [wedding, setWedding] = useState(null);
    const { getWeddingDetailsOfUser } = useWeddingService();

    useEffect(() => {
        const apiCall = async () => {
            const weddingDetails = await getWeddingDetailsOfUser(user.weddingId);
            setWedding(weddingDetails);
        }

        apiCall();
    }, [])

    const getPercentage = (all, part) => {
        return (part * 100) / all;
    }

    const Progress = ({ all, part, text, color }) => {
        return (
            <Box position='relative' width='fit-content'>
                <CircularProgress
                    sx={{ color: 'rgba(128,128,128,0.3)' }}
                    value={100}
                    variant="determinate"
                    size={200}
                    thickness={2}
                />
                <Box
                    position='absolute'
                    top='0'
                    left='0'
                    right='0'
                    bottom='0'
                    display='flex'
                    flexDirection='column'
                    alignItems='center'
                    justifyContent='center'
                >
                    <Typography fontWeight='bold'>{text}</Typography>
                    <Typography>{part}/{all}</Typography>
                </Box>
                <CircularProgress
                    style={{
                        transform: 'rotate(-270deg)',
                        position: 'absolute',
                        left: 0,
                    }}
                    color={color}
                    thickness={2}
                    size={200}
                    variant='determinate'
                    value={(getPercentage(all, part))} />
            </Box>
        )
    }

    return (
        <>
            {wedding
                ? <Container>
                    <h3>{wedding.nameOfBride}  & {wedding.nameOfFiance} esküvője</h3>
                    <Stack spacing={4}>
                        <Box display='flex' alignItems='center'>
                            <QueryBuilderIcon />
                            <h4>Esküvő dátuma: <b>{wedding.dateOfWedding}</b></h4>
                        </Box>
                        <Paper>
                            <h4>Vendégek</h4>
                            <Grid container gap={1} justifyContent='center'>
                                <Grid item xs={3}>
                                    <Progress
                                        all={wedding.numberOfInvitations}
                                        part={wedding.numberOfInvitationsWithPositiveFeedback}
                                        text='Ott lesz'
                                        color='secondary'
                                    />
                                </Grid>
                                <Grid item xs={3}>
                                    <Progress
                                        all={wedding.numberOfInvitations}
                                        part={wedding.numberOfInvitationsWithNegativeFeedback}
                                        text='Nem lesz ott'
                                        color='error'
                                    />
                                </Grid>
                                <Grid item xs={3}>
                                    <Progress
                                        all={wedding.numberOfInvitations}
                                        part={wedding.numberOfInvitationsWithPendingFeedback}
                                        text='Még nem jelzett vissza'
                                        color='primary'
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                color='primary'
                                variant='contained'
                                endIcon={<ChevronRightIcon />}
                                sx={{ float: 'right' }}
                                onClick={() => navigate('/guests')}
                            > Vendéglista megtekintése</Button>
                        </Paper>
                        <Paper>
                            <h4>Elvégezendő feladatok</h4>
                            <Grid container gap={1} justifyContent='center'>
                                <Grid item xs={3}>
                                    <Progress
                                        all={wedding.numberOfCompletedTodos + wedding.numberOfUncompletedTodos}
                                        part={wedding.numberOfCompletedTodos}
                                        text='Elvégezve'
                                        color='secondary'
                                    />
                                </Grid>
                                <Grid item xs={3}>
                                    <Progress
                                        all={wedding.numberOfCompletedTodos + wedding.numberOfUncompletedTodos}
                                        part={wedding.numberOfUncompletedTodos}
                                        text='Függőben'
                                        color='primary'
                                    />
                                </Grid>
                            </Grid>
                            <Button
                                color='primary'
                                variant='contained'
                                endIcon={<ChevronRightIcon />}
                                sx={{ float: 'right' }}
                                onClick={() => navigate('/todos')}
                            > Ugrás a teendőkhöz</Button>
                        </Paper>
                    </Stack>
                </Container >
                : <div>Loading...</div>
            }
        </>
    )
}

export default HomePageForCouple;