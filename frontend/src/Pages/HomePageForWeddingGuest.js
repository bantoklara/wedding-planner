import React from 'react'

import GuestUpdatingForm from '../Components/GuestUpdatingForm'
import { useState } from 'react'
import { useGuestService } from '../Services/GuestService'
import { useMenuService } from '../Services/MenuService'
import { useEffect } from 'react'

const HomePageForWeddingGuest = () => {
    const [guest, setGuest] = useState(null);
    const [menus, setMenus] = useState(null);
    const { getAuthenticatedGuestInfo } = useGuestService();
    const { getAllMenus } = useMenuService();

    useEffect(() => {
        const apiCall = async () => {
            const guestDetails = await getAuthenticatedGuestInfo();
            setGuest(guestDetails);

            const menuList = await getAllMenus();
            setMenus(menuList);
        };

        apiCall();
    }, []);

    return (
        <>
            {guest && menus
                ? <GuestUpdatingForm guest={guest} setGuest={setGuest} menus={menus} />
                : <div>Loading...</div>
            }
        </>
    )
}

export default HomePageForWeddingGuest
