import { useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

import { Button, TextField, InputAdornment, Dialog, Alert, DialogTitle, DialogContent } from "@mui/material";

import PersonIcon from '@mui/icons-material/Person';
import LockIcon from '@mui/icons-material/Lock';

import { useWeddingService } from "../Services/WeddingService";
import AuthenticationHandler from "../Services/AuthenticationService";
import LoginStyle from "../Styles/LoginStyle";
import UserContext from "../UserContext";

const LoginForm = ({ dialogOpen, setDialogOpen }) => {
    const { setUser } = useContext(UserContext);
    const [error, setError] = useState('')
    const [processing, setProcessing] = useState(false);
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const { login, getUserInfoFromToken } = AuthenticationHandler();
    const navigate = useNavigate();

    const submit = async (event) => {
        event.preventDefault();
        setProcessing(true);
        setError('');
        const token = await login({ username, password });
        if (token) {
            setUser(getUserInfoFromToken(token));
            setDialogOpen(false);
            navigate('/');
        }
        else {
            setError('Helytelen felhasználónév vagy jelszó!');
        }
        setProcessing(false);
    };

    return (
        <Dialog open={dialogOpen} onClose={() => setDialogOpen(false)}>
            <DialogTitle>Bejelentkezés </DialogTitle>
            <DialogContent>
                {error !== '' && <Alert severity="error">{error}</Alert>}
                <form onSubmit={submit}>
                    <br />
                    <TextField
                        type="text"
                        variant="outlined"
                        placeholder="Felhasználónév"
                        name="username"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <PersonIcon />
                                </InputAdornment>
                            ),
                        }}
                        onChange={(e) => { setUsername(e.target.value); }}
                        size="small"
                        required
                    />
                    <br /><br />
                    <TextField
                        type="password"
                        variant="outlined"
                        placeholder="Jelszó"
                        name="password"
                        InputProps={{
                            startAdornment: (
                                <InputAdornment position="start">
                                    <LockIcon />
                                </InputAdornment>
                            ),
                        }}
                        onChange={(e) => { setPassword(e.target.value) }}
                        size="small"
                        required
                    />
                    <br /><br />
                    <Button
                        color="secondary"
                        variant="contained"
                        sx={{ color: '#ffffff' }}
                        type="submit">Bejelentkezés {processing && '...'}</Button>
                </form>
            </DialogContent>
        </Dialog>
    )
}

export default LoginForm;