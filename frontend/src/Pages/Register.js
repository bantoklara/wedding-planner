import { useState } from "react";

import { Alert, Button, Container, Dialog, DialogActions, DialogContent, DialogTitle, Typography } from "@mui/material";

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';

import { useFormHandlers } from "../Services/FormHandlers/UserRegisterFormHandler";
import RegisterOtherInfo from "../Components/RegisterOtherInfo";
import RegisterRoleInfo from "../Components/RegisterRoleInfo";
import RegisterUserInfo from "../Components/RegisterUserInfo";

const Register = ({ dialogOpen, setDialogOpen }) => {
    const {
        processing,
        values,
        error,
        errorFromServer,
        inputChangeHandler,
        weddingInputChangeHandler,
        submitFormHandler
    } = useFormHandlers();
    const [page, setPage] = useState(0);

    const isTheLastPage = () => {
        return (values.role === 'ROLE_COUPLE' && page === 2) || (values.role === 'ROLE_SERVICE_PROVIDER' && page === 1);
    }

    const FormData = () => {
        switch (page) {
            case 0: return <RegisterRoleInfo values={values} inputChangeHandler={inputChangeHandler} />
            case 1: return <RegisterUserInfo values={values} error={error} inputChangeHandler={inputChangeHandler} />
            default: return <RegisterOtherInfo values={values} error={error} weddingInputChangeHandler={weddingInputChangeHandler} />
        }
    }

    return (
        <Dialog
            open={dialogOpen}
            onClose={() => setDialogOpen(false)}
            fullWidth
            maxWidth='xs'
        >
            <DialogTitle>
                Regisztráció {processing && "..."}
            </DialogTitle>

            <DialogContent>
                {errorFromServer !== '' && <Alert severity="error">{errorFromServer}</Alert>}
                <Container
                    className="register-form-container"
                    style={{ height: "50vh", justifyContent: 'center', display: 'flex', flexDirection: 'column' }}
                >
                    {FormData()}
                </Container>
            </DialogContent>
            <DialogActions sx={{ maring: '15px', justifyContent: 'space-between' }}>
                <Button
                    startIcon={<ArrowBackIcon />}
                    variant="text"
                    disabled={page === 0}
                    onClick={() => { setPage((currentPage) => currentPage - 1) }}
                >Vissza
                </Button>
                <Button
                    variant={isTheLastPage() ? "contained" : "text"}
                    color="primary"
                    endIcon={!isTheLastPage() ? <ArrowForwardIcon /> : ""}
                    onClick={(e) => {
                        if (!isTheLastPage())
                            setPage((currentPage) => currentPage + 1)
                        else submitFormHandler(e)
                    }}
                >
                    {isTheLastPage()
                        ? "Regisztrálás"
                        : "Következő"}
                </Button>
            </DialogActions>
        </Dialog >
    )
}

export default Register;