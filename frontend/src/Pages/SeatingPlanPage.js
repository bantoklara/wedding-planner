import produce from 'immer';
import React, { useEffect, useReducer, useState } from 'react'
import { DragDropContext, Droppable } from 'react-beautiful-dnd';

import { Badge, Box, Grid, IconButton, Paper, Stack, Tooltip } from '@mui/material';

import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import TableRestaurantRoundedIcon from '@mui/icons-material/TableRestaurantRounded';
import AddCircleRoundedIcon from '@mui/icons-material/AddCircleRounded';

import { useGuestService } from '../Services/GuestService';
import DraggableTable from '../Components/drag-and-drop/DraggableTable';
import DraggableList from '../Components/drag-and-drop/DraggableList';
import CreateTableForm from '../Components/CreateTableForm';
import useStyles from '../Styles/IconButtonStyle';

const dragReducer = produce((state, action) => {
    switch (action.type) {
        case 'MOVE': {
            const fromTable = action.from;
            const toTable = action.to;
            const guestId = action.fromIndex;
            const guest = state[fromTable].guests[guestId].guest;
            const seats = state[fromTable].guests[guestId].seats;

            const newSeats = [];

            if (fromTable !== '0') {
                for (let i = 0; i < seats.length; i++) {
                    state[fromTable].guests[-1].seats.push({ index: seats[i].index, height: 10, width: 10 });
                }
            }
            delete state[fromTable].guests[guestId];

            if (toTable !== '0') {
                for (let i = 0; i < guest.numberOfGuests; i++) {
                    const seat = state[toTable].guests[-1].seats.shift();
                    newSeats.push({ index: seat.index, height: 25, width: 35 });
                }
                state[toTable].guests[guestId] = { guest: guest, seats: newSeats };
            } else {
                state[toTable].guests[guestId] = { guest: guest };
            }

            break;
        }
        case 'CREATE': {
            state[action.id] = {
                name: action.name,
                numberOfSeats: action.numberOfSeats,
                guests: { '-1': { seats: Array.from({ length: action.numberOfSeats }, (_, i) => ({ index: i, height: 10, width: 10 })) } }
            };
            break;
        }

        case 'DELETE_TABLE': {
            const guestsFromTable = state[action.tableId].guests;
            delete state[action.tableId];
            Object.entries(guestsFromTable).forEach(([guestId, value]) => delete guestsFromTable[guestId].seats);
            delete guestsFromTable['-1'];
            state['0'].guests = { ...state['0'].guests, ...guestsFromTable };
        }
    }
});


const SeatingPlanPage = () => {
    const { updateGuestInformation, getGuestsGroupedByTable } = useGuestService();
    const [state, dispatch] = useReducer(dragReducer, {});
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(true);
    const [visible, setVisible] = useState(true);
    const [onDrag, setOnDrag] = useState({ guestId: -1, seatIndex: -1 });
    const [tableOnHover, setTableOnHover] = useState('');
    const withoutSeatId = 0;

    const classes = useStyles();

    const buildGuestWithSeats = (guests, numberOfSeats) => {
        const guestsWithSeats = {};
        let numberOfGuests = 0;
        let seatIndex = 0;

        for (const guest of guests) {
            const seats = [];
            for (let i = 0; i < guest.numberOfGuests; i++) {
                seats.push({ index: seatIndex, height: 25, width: 35 });
                seatIndex += 1;
            }
            guestsWithSeats[parseInt(guest.id)] = { guest: guest, seats: seats };
        }
        numberOfGuests = seatIndex;

        const seats = [];
        while (seatIndex < numberOfSeats) {
            seats.push({ index: seatIndex, height: 10, width: 10 });
            seatIndex += 1;
        }
        guestsWithSeats[parseInt(-1)] = { seats: seats };

        return { guestsWithSeats: guestsWithSeats, numberOfGuests: numberOfGuests };
    };

    const updateState = (groupedGuests = new Map()) => {
        for (const [table, guests] of groupedGuests) {
            const id = table.id !== null ? table.id.toString() : withoutSeatId.toString();
            const { guestsWithSeats, numberOfGuests } = table.id !== null && buildGuestWithSeats(guests, table.numberOfSeats);
            const value = table.id !== null
                ? {
                    name: table.name,
                    numberOfSeats: table.numberOfSeats,
                    guests: guestsWithSeats,
                    numberOfGuests: numberOfGuests
                }
                : {
                    name: "Le nem ültetett vendégek",
                    guests: guests.reduce((obj, guest) => ({ ...obj, [guest.id]: { guest: guest } }), {})
                }
            state[id] = value;
        }
    };

    const saveGuestsTable = async (guestId, tableId) => {
        await updateGuestInformation(guestId, { weddingTableId: tableId });
    };

    const onDragStart = (result) => {
        setOnDrag({ guestId: parseInt(result.draggableId.split('-')[0]), seatIndex: result.draggableId.split('-')[1] });
    }

    const onDragUpdate = (result) => {
        if (!result.source?.droppableId || !result.destination?.droppableId || !result.draggableId)
            return;
        const from = result.source.droppableId.split('-')[0];
        const to = result.destination.droppableId;
        const guestId = result.draggableId.split('-')[0];

        if (to !== '0' && (from === to || state[to].guests[-1].seats.length >= state[from].guests[guestId].guest.numberOfGuests))
            setTableOnHover(to);
        else setTableOnHover(-1);
    }

    const onDragEnd = (result) => {
        if (result.reason === 'DROP') {
            setOnDrag({ guestId: -1, seatIndex: -1 });
            setTableOnHover(-1);
            if (!result.destination)
                return;

            const from = result.source.droppableId.split('-')[0];
            const to = result.destination.droppableId;

            // Check if the two tables are not the same
            if (from === to)
                return;

            const guestId = result.draggableId.split('-')[0];
            // Check if the destination table has enough seat
            if (to !== '0' && state[to].guests[-1].seats.length < state[from].guests[guestId].guest.numberOfGuests)
                return;

            dispatch({
                type: 'MOVE',
                from: from,
                to: to,
                fromIndex: guestId
            });

            saveGuestsTable(guestId, parseInt(result.destination.droppableId));
        }
    };

    useEffect(() => {
        const apiCall = async () => {
            const groupedGuests = await getGuestsGroupedByTable();
            updateState(groupedGuests);
            setLoading(false);
        };

        apiCall();
    }, []);

    return (
        <div>
            {!loading
                ? <DragDropContext
                    onDragStart={onDragStart}
                    onDragEnd={onDragEnd}
                    onDragUpdate={onDragUpdate}>
                    {/* Guests without seat */}

                    {/* Inserting new table */}
                    <Box>
                        <IconButton
                            onClick={() => setVisible(prev => !prev)}
                        >{visible ? <ArrowBackIcon color='secondary' /> : <ArrowForwardIcon color='secondary' />}</IconButton>
                    </Box>
                    <Grid container style={{ height: '100%' }}>
                        <Grid item lg={visible ? 2 : 0}>
                            {visible &&
                                <Paper sx={{ height: '100%' }}>
                                    <Stack spacing={2}>
                                        <Box>
                                            <Tooltip title='Új asztal létrehozása'>
                                                <IconButton onClick={() => setButtonPopup(true)} className={classes.addTableButton}>
                                                    <Badge badgeContent={<AddCircleRoundedIcon color='primary' />} sx={{ padding: '0' }}>
                                                        <TableRestaurantRoundedIcon fontSize='large' color='primary' />
                                                    </Badge>
                                                </IconButton>
                                            </Tooltip>
                                            <CreateTableForm
                                                dispatch={dispatch}
                                                dialogOpen={buttonPopup}
                                                setDialogOpen={setButtonPopup} />
                                        </Box>
                                        <Box>
                                            <h3>Le nem ültetett vendégek</h3>
                                            <Droppable
                                                droppableId={withoutSeatId.toString()}
                                                type="GUEST"
                                            >
                                                {(provided) => {
                                                    return (
                                                        <div
                                                            ref={provided.innerRef}
                                                            {...provided.droppableProps}
                                                        >
                                                            <DraggableList
                                                                guests={state[0].guests}
                                                            >
                                                                {provided.placeholder}
                                                            </DraggableList>
                                                        </div>
                                                    )
                                                }}
                                            </Droppable>
                                        </Box>
                                    </Stack>
                                </Paper>
                            }
                        </Grid>
                        <Grid item lg={visible ? 10 : 12} >
                            <Grid
                                container
                                justifyContent='center'
                            >
                                {
                                    Object.entries(state).slice(1).map(([key, { name, numberOfSeats, guests }], index) => {
                                        return (
                                            <Droppable
                                                droppableId={key}
                                                type="GUEST"
                                                key={key}
                                            >
                                                {(provided) => {
                                                    return (
                                                        <Grid item
                                                            ref={provided.innerRef}
                                                            {...provided.droppableProps}
                                                        >
                                                            <DraggableTable
                                                                state={state}
                                                                tableId={key}
                                                                dispatch={dispatch}
                                                                name={name}
                                                                numberOfSeats={numberOfSeats}
                                                                guests={guests}
                                                                saveGuestsTable={saveGuestsTable}
                                                                onDrag={onDrag}
                                                                tableOnHover={tableOnHover}
                                                            >
                                                                {provided.placeholder}
                                                            </DraggableTable>
                                                        </Grid>
                                                    )
                                                }}
                                            </Droppable>
                                        )
                                    })
                                }
                            </Grid>
                        </Grid>
                    </Grid>
                </DragDropContext>
                : <div>Loading...</div>
            }
        </div >
    )
}

export default SeatingPlanPage
