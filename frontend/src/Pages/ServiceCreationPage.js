import React, { useState, useEffect } from 'react'

import useCategoryService from '../Services/CategoryService';
import ServiceCreationForm from '../Components/ServiceCreationForm';

const ServiceCreationPage = () => {
    const [categories, setCategories] = useState(null);

    const { getAllCategories } = useCategoryService();

    useEffect(() => {
        const apiCall = async () => {
            const categoryList = await getAllCategories();
            setCategories(categoryList);
        };

        apiCall();
    }, []);


    return (
        <div>
            {categories
                ?
                <>
                    <ServiceCreationForm categories={categories} />
                </>
                : <div>Loading...</div>
            }
        </div>
    )
}

export default ServiceCreationPage
