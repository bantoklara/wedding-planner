import React, { useEffect, useState, useContext } from 'react'
import { useParams } from 'react-router-dom';

import { Box, Stack } from '@mui/material';

import { useWeddingServiceService } from '../Services/WeddingServiceService';
import UserContext from '../UserContext';
import AdditionalServiceDetailsForCouples from '../Components/AdditionalServiceDetailsForCouples';
import ServiceDetails from '../Components/ServiceDetails';
import AdditionalServiceDetailsForProviders from '../Components/AdditionalServiceDetailsForProviders';
import AdditionalServiceDetailsForAdmin from '../Components/AdditionalServiceDetilasForAdmin';

const ServiceDetailPage = () => {
    const { id } = useParams();
    const { user } = useContext(UserContext);
    const [service, setService] = useState(null);
    const { getServiceDetails } = useWeddingServiceService();

    useEffect(() => {
        const apiCall = async () => {
            const serviceDetails = await getServiceDetails(parseInt(id));
            setService(serviceDetails);
        };

        apiCall();
    }, []);

    return (
        <Box>
            {service
                ?
                <Stack spacing={3}>
                    {user?.role === 'ROLE_ADMIN' && <AdditionalServiceDetailsForAdmin service={service} />}
                    {user?.role === 'ROLE_SERVICE_PROVIDER' && user.username === service.user.username &&
                        <AdditionalServiceDetailsForProviders service={service} />
                    }
                    {user?.role === 'ROLE_COUPLE' && <AdditionalServiceDetailsForCouples id={id} />}
                    <ServiceDetails service={service} />
                </Stack>
                : <div>Loading...</div>
            }
        </Box >
    )
}

export default ServiceDetailPage
