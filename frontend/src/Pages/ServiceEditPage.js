import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom';

import { useWeddingServiceService } from '../Services/WeddingServiceService';
import useCategoryService from '../Services/CategoryService';
import ServiceCreationForm from '../Components/ServiceCreationForm';

const ServiceEditPage = () => {
    const { id } = useParams();
    const [service, setService] = useState(null);
    const [categories, setCategories] = useState(null);
    const { getServiceDetails } = useWeddingServiceService();
    const { getAllCategories } = useCategoryService();

    useEffect(() => {
        const apiCall = async () => {
            const serviceDetails = await getServiceDetails(parseInt(id));
            setService(serviceDetails);

            const categoryList = await getAllCategories();
            setCategories(categoryList);
        };

        apiCall();
    }, []);

    return (
        <div>
            {service && categories
                ? <ServiceCreationForm categories={categories} serviceDetails={service} />
                : <div>Loading...</div>
            }
        </div>
    )
}

export default ServiceEditPage
