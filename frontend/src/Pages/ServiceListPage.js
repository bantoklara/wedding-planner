import React, { useContext, useState, useEffect } from 'react'
import { useSearchParams } from 'react-router-dom';

import { Box, Button, Checkbox, Container, FormControlLabel, Radio, RadioGroup, Stack, Switch, Typography } from '@mui/material'

import CloseIcon from '@mui/icons-material/Close';
import FilterListIcon from '@mui/icons-material/FilterList';

import { useWeddingServiceService } from '../Services/WeddingServiceService'
import { useFilterHandler } from '../Services/FilterAndSortHandler/ServiceFilterHandler';
import UserContext from '../UserContext';
import useCategoryService from '../Services/CategoryService';
import ServiceFilterForm from '../Components/ServiceFilterForm';
import ServiceListComponent from '../Components/ServiceListComponent';

const ServiceListPage = () => {
    const { filter } = useWeddingServiceService();
    const { user } = useContext(UserContext);
    const { getAllCategories } = useCategoryService();

    const [services, setServices] = useState([]);
    const [categories, setCategories] = useState(null);
    const [loading, setLoading] = useState(false);
    const [hasMore, setHasMore] = useState(true);
    const [page, setPage] = useState(0);
    const [dialogOpen, setDialogOpen] = useState(false);

    const [searchParams] = useSearchParams()

    const {
        values,
        getRequestParams,
        inputChangeHandler,
        dateChangeHandler,
        enableCheckHandler,
        myServiceCheckboxChangeHandler,
        clearAllFilters,
        submitFormHandler
    } = useFilterHandler(searchParams, setPage);

    useEffect(() => {
        const apiCall = async () => {
            setLoading(true);
            const serviceList = await filter(getRequestParams(), page);
            setHasMore(serviceList.length > 0);
            setServices(prev => { return page === 0 ? serviceList : [...prev, ...serviceList] });
            setLoading(false);
            setDialogOpen(false);
        };

        apiCall();
    }, [searchParams, page]);

    useEffect(() => {
        const apiCall = async () => {
            const categoryList = await getAllCategories();
            setCategories(categoryList);
        }
        apiCall();
    }, [])

    return (
        <Container>
            <Stack spacing={4}>
                <Box display='flex' gap={2}>
                    <Button
                        color='secondary'
                        variant='contained'
                        sx={{ color: '#ffffff' }}
                        startIcon={<FilterListIcon />}
                        onClick={() => setDialogOpen(true)}
                    >Szűrés</Button>
                    {getRequestParams().length > 0 &&
                        <Button
                            color='error'
                            variant='outlined'
                            onClick={clearAllFilters}
                            endIcon={<CloseIcon />}
                        >Szűrőfeltételek törlése
                        </Button>
                    }
                    {categories &&
                        <ServiceFilterForm
                            loading={loading}
                            values={values}
                            inputChangeHandler={inputChangeHandler}
                            enableCheckHandler={enableCheckHandler}
                            dateChangeHandler={dateChangeHandler}
                            myServiceCheckboxChangeHandler={myServiceCheckboxChangeHandler}
                            clearAllFilters={clearAllFilters}
                            categories={categories}
                            role={user?.role}
                            dialogOpen={dialogOpen}
                            setDialogOpen={setDialogOpen}
                            submitFormHandler={submitFormHandler}
                        />
                    }
                </Box>
                {!loading && services.length === 0
                    ? <Typography variant='h2' sx={{ fontStyle: 'italic' }}>Nincs találat</Typography>
                    : <Box margin='30px'><ServiceListComponent
                        services={services}
                        loading={loading}
                        hasMore={hasMore}
                        setPage={setPage} /></Box>
                }
                {loading && <div>Loading...</div>}
            </Stack>
        </Container>
    )
}

export default ServiceListPage
