import React, { useState, useEffect } from 'react'
import {
    Button,
    Checkbox,
    Dialog,
    FormControlLabel,
    Paper,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableRow
} from '@mui/material';

import AddCircleIcon from '@mui/icons-material/AddCircle';
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';

import { groupBy } from "core-js";
import { useTodoService } from '../Services/TodoService';
import useCategoryService from '../Services/CategoryService';
import TodoStyle from '../Styles/TodoStyle';
import TodoCreationForm from '../Components/TodoCreationForm';
import Warning from '../Components/Warning';

const TodoListPage = () => {
    const [groupedTodos, setGroupedTodos] = useState(null);
    const [categories, setCategories] = useState(null);
    const [buttonPopup, setButtonPopup] = useState(false);
    const [loading, setLoading] = useState(-1);
    const [params, setParams] = useState({});

    const { getAllTodos, updateTodoState, deleteTodo } = useTodoService();
    const { getAllCategories } = useCategoryService();
    const classes = TodoStyle();

    useEffect(() => {
        const apiCall1 = async () => {
            const todoList = await getAllTodos({});
            const groupedTodoList = todoList.groupBy(todo => { return todo.deadline });
            const x = [];
            Object.keys(groupedTodoList).map((key) => {
                x.push({
                    deadline: key,
                    deadlineText: setDeadlineText(parseInt(key)),
                    expanded: false,
                    todos: (groupedTodoList[key]).map((todo) => ({ editMode: false, todo: todo }))
                })
            });
            setGroupedTodos(x);
        };

        const apiCall2 = async () => {
            const categoryList = await getAllCategories();
            setCategories(categoryList);
        }

        apiCall1();
        apiCall2();
    }, []);

    const todoCheckedHandler = (rindex, cindex, id, checked) => {
        updateTodoState(id, checked);
        const tmp = [...groupedTodos];
        tmp[rindex].todos[cindex].todo = { ...tmp[rindex].todos[cindex].todo, done: checked };
        setGroupedTodos(tmp);
    };

    const setDeadlineText = (deadline) => {
        switch (deadline) {
            case 1:
                return "12 hónappal az esküvő előtt";
            case 2:
                return "7-9 hónappal az esküvő előtt";
            case 3:
                return "4-6 hónappal az esküvő előtt";
            case 4:
                return "2-3 hónappal az esküvő előtt";
            case 5:
                return "1 hónappal az esküvő előtt";
            case 6:
                return "2 héttel az esküvő előtt";
            case 7:
                return "néhány nappal az esküvő előtt";
            case 8:
                return "az esküvő napján";
            case 9:
                return "az esküvő után";
        }
    };

    const expandGroup = (index) => {
        const tmp = [...groupedTodos];
        tmp[index].expanded = !tmp[index].expanded;
        setGroupedTodos(tmp);
    }

    const deleteTodoHandler = ({ rindex, cindex, todoId }) => {
        setLoading(1);
        deleteTodo(todoId);
        const tmp = [...groupedTodos];
        delete tmp[rindex].todos[cindex];
        setGroupedTodos(tmp);
        setLoading(0);
    }

    const setEditMode = (rindex, cindex) => {
        const tmp = [...groupedTodos];
        tmp[rindex].todos[cindex].editMode = !tmp[rindex].todos[cindex].editMode;
        setGroupedTodos(tmp);
    }

    const triggerPopup = (paramss) => {
        setParams(paramss);
        setLoading(-1);
        setButtonPopup(true);
    };

    return (
        <>
            {groupedTodos && categories
                ? <>
                    <Warning
                        dialogOpen={buttonPopup}
                        setDialogOpen={setButtonPopup}
                        message='Biztosan törli a teendőt?'
                        cancelMessage='Mégsem'
                        continueMessage='Teendő törlése'
                        action={deleteTodoHandler}
                        params={params}
                        loading={loading}
                    />
                    <Stack spacing={4}>
                        <Paper>
                            <h3>Új teendő létrehozása</h3>
                            <TodoCreationForm groupedTodos={groupedTodos} setGroupedTodos={setGroupedTodos} categories={categories} />
                        </Paper>
                        <Paper>
                            {
                                groupedTodos.map(({ deadline, deadlineText, expanded, todos }, rindex) => (
                                    <div key={deadline} style={{ margin: '10px' }}>
                                        <Button
                                            color={groupedTodos[rindex].expanded ? 'secondary' : 'primary'}
                                            className={classes.groupButton}
                                            variant="contained"
                                            size="large"
                                            sx={{ color: groupedTodos[rindex].expanded ? '#ffffff' : '#000000' }}
                                            startIcon={expanded ? <RemoveCircleIcon /> : <AddCircleIcon />}
                                            onClick={() => expandGroup(rindex)}
                                        >{deadlineText}
                                        </Button>
                                        {expanded &&
                                            <Table>
                                                <TableBody>
                                                    {todos.map(({ todo, editMode }, cindex) => (
                                                        <TableRow key={todo.id} style={{ height: '20' }}>
                                                            {editMode
                                                                ? <>
                                                                    <TableCell
                                                                        colSpan={3}
                                                                        align='left'
                                                                    >
                                                                        <TodoCreationForm
                                                                            update={{ info: { rindex: rindex, cindex: cindex, id: todo.id } }}
                                                                            groupedTodos={groupedTodos}
                                                                            setGroupedTodos={setGroupedTodos}
                                                                            categories={categories}
                                                                            setCategories={setCategories}
                                                                        />
                                                                    </TableCell>
                                                                </>
                                                                : <><TableCell align='left' style={{ width: '55%' }}>
                                                                    <FormControlLabel
                                                                        control={
                                                                            <Checkbox
                                                                                checked={todo.done}
                                                                                onClick={(event) => { todoCheckedHandler(rindex, cindex, parseInt(todo.id), event.target.checked) }}
                                                                            />}
                                                                        label={todo.description}

                                                                    />
                                                                </TableCell>
                                                                    <TableCell align='left' style={{ width: '30%' }}>{todo.category.name}</TableCell>
                                                                    <TableCell align="right" style={{ width: '15%' }}>
                                                                        <Button
                                                                            size='small'
                                                                            onClick={() => { setEditMode(rindex, cindex) }}
                                                                        ><EditIcon color='primary' /></Button>
                                                                        <Button
                                                                            size='small'
                                                                            onClick={(e) => { triggerPopup({ rindex: rindex, cindex: cindex, todoId: todo.id }) }}
                                                                        ><DeleteIcon color='error' /></Button>
                                                                    </TableCell>
                                                                </>
                                                            }
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table>}
                                    </div>
                                ))
                            }</Paper>
                    </Stack>
                </>
                : <div>Loading...</div>
            }
        </>
    )
}

export default TodoListPage
