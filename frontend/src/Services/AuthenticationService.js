import jwtDecode from "jwt-decode";
import { useCookies } from 'react-cookie';
import { useNavigate } from "react-router-dom";

const AuthenticationHandler = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['token'])
    const navigate = useNavigate();

    const getUserInfoFromToken = (token) => {
        let userInfo = {};
        if (token.startsWith("CustomAuth")) {
            userInfo = jwtDecode(token.substring("CustomAuth".length));
        }
        else userInfo = jwtDecode(token);
        if (userInfo.role === 'ROLE_COUPLE') {
            return {
                username: userInfo.sub,
                role: userInfo.role,
                name: userInfo.name,
                weddingId: userInfo.weddingId
            };
        }

        return {
            username: userInfo.sub,
            name: userInfo.name,
            role: userInfo.role
        };
    }

    const login = async (authRequest) => {
        const response = await fetch('/api/auth/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(authRequest)
        });
        if (response.status === 200) {
            const token = response.headers.get('Authorization');
            setCookie('token', token, { path: '/' });
            return token;
        }

        if (response.status === 401) {
            return null;
        }
    }

    const register = async (body) => {
        const response = await fetch('/api/auth/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        });

        return response.status;
    }

    const loginAsGuest = async (loginCode) => {
        const response = await fetch("/api/auth/login-as-guest", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(loginCode)
        });

        if (response.status === 200) {
            const { token } = await response.json();
            setCookie('token', token, { path: '/' });
            return token;
        }

        if (response.status === 401) {
            return null;
        }
    }

    const logout = () => {
        removeCookie('token');
        navigate("/");
    };

    return {
        getUserInfoFromToken,
        login,
        loginAsGuest,
        register,
        logout
    };
}

export default AuthenticationHandler;