import { useFetchWrapper } from "../fetchWrapper";

function useBookedServiceService() {
    const path = "/api/booked-services";
    const fetchWrapper = useFetchWrapper();

    // CREATE
    async function bookService(serviceId) {
        const response = await fetchWrapper.post(path, JSON.stringify(serviceId));

        if (response.status === 200) {
            const createdReservation = await response.json();
            return true;
        }
        return false;
    }

    // READ
    async function getById(id) {
        const response = await fetchWrapper.get(`${path}/${id}`);

        if (response.status === 200) {
            const bookedService = await response.json();
            return bookedService;
        }
        return null;
    };

    async function isReserved(serviceId) {
        const response = await fetchWrapper.get(`${path}/${serviceId}/is-reserved`);

        if (response.status === 200) {
            const bookedService = await response.json();
            return bookedService;
        }
        else if (response.status === 204) {
            return null;
        }
    };

    async function findBookedServicesOfUser(filters) {
        const endpoint = filters === "" ? path : `${path}?${filters}`;
        const response = await fetchWrapper.get(endpoint);

        if (response.status === 200) {
            const bookedServiceList = await response.json();
            return bookedServiceList;
        }
        return null;
    };

    // UPDATE
    async function update(id, body) {
        const response = await fetchWrapper.put(`${path}/${id}`, JSON.stringify(body));

        if (response.status === 200) {
            const updatedBookedService = await response.json();
            return updatedBookedService;
        }
        return null;
    }

    // DELETE
    async function deleteBookedService(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);

        if (response.status === 200) {
            return true;
        }
        return false;
    };

    return {
        bookService,
        getById,
        isReserved,
        findBookedServicesOfUser,
        update,
        deleteBookedService
    };
}

export default useBookedServiceService;