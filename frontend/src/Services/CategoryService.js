import { useFetchWrapper } from '../fetchWrapper';

function useCategoryService() {
    const path = `/api/categories`;
    const fetchWrapper = useFetchWrapper();

    async function getAllCategories() {
        const response = await fetchWrapper.get(path);

        if (response.status === 200) {
            const categoryList = await response.json();
            return categoryList;
        }

        // error handling
    }
    
    return {
        getAllCategories
    };
}

export default useCategoryService
