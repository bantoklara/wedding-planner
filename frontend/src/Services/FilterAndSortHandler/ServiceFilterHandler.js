import { useEffect } from "react";
import { useRef, useState } from "react"
import { useNavigate } from "react-router-dom";

const initialFormValues = {
    myServices: false,
    enabled: true,
    serviceType: '',
    categoryId: '',
    county: '',
    numberOfRequiredSpace: '',
    priceCategory: '',
    date: '',
    start: '',
    end: '',
    byDay: 'byDay'
};

export const useFilterHandler = (searchParams, setPage) => {
    const requestParams = useRef('');
    const [values, setValues] = useState({ ...initialFormValues });
    const filters = useRef(new Map());
    const navigate = useNavigate();

    useEffect(() => {
        const filterInObcjet = Object.entries(Object.fromEntries([...searchParams]));
        let query = "";
        const tmp = { ...values };
        if (filterInObcjet.length > 0) {
            for (const [key, value] of filterInObcjet) {
                query += `${key}=${value}&`;
                filters.current.set(key, value);
                tmp[key] = value === 'false' ? false : value === 'true' ? true : value;
            };
            setValues(tmp);
        }

        requestParams.current = query !== '' ? (query.substring(0, query.length - 1)) : '';
    }, [searchParams]);

    const getRequestParams = () => { return requestParams.current; }

    const clearAllFilters = () => {
        filters.current = new Map();
        setValues({ ...initialFormValues });
        setPage(0);
        navigate('/services');
    };

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setValues({ ...values, [name]: value });
        if (value === '')
            filters.current.delete(name);
        else
            filters.current.set(name, value);
    }

    const dateChangeHandler = (event) => {
        const { name, value } = event.target;

        if (values.byDay === 'byDay') {
            const start = new Date(value);
            const end = new Date(value);
            end.setDate(end.getDate() + 1);

            setValues({ ...values, [name]: value });
            filters.current.set('start', start.toISOString().split('.')[0]);
            filters.current.set('end', end.toISOString().split('.')[0]);
        }
        if (values.byDay === 'byInterval') {
            setValues({ ...values, [name]: value });
            if (name === 'start' && values.end) {
                filters.current.set('start', value);
                filters.current.set('end', values.end);
            }
            if (name === 'end' && values.start) {
                filters.current.set('start', values.start);
                filters.current.set('end', value);
            }
        }
    }

    const enableCheckHandler = (event) => {
        const { value } = event.target;
        const enabled = value == 'true' ? true : false;
        setValues({ ...values, 'enabled': enabled });
        filters.current.set('enabled', enabled);
    };

    const myServiceCheckboxChangeHandler = (event) => {
        const { checked } = event.target;
        setValues({ ...values, myServices: checked });
        if (!checked) {
            filters.current.delete('myServices');
            filters.current.delete('enabled');
        }
        else filters.current.set('myServices', checked);
    }

    const submitFormHandler = async () => {
        let requestParams = "";
        for (const [key, value] of filters.current.entries()) {
            requestParams += `${key}=${value}&`;
        }

        if (requestParams !== "")
            requestParams = requestParams.substring(0, requestParams.length - 1)

        setPage(0);
        navigate(requestParams === '' ? '/services' : `/services?${requestParams}`);
    }

    return {
        values,
        getRequestParams,
        inputChangeHandler,
        dateChangeHandler,
        enableCheckHandler,
        myServiceCheckboxChangeHandler,
        clearAllFilters,
        submitFormHandler
    };

}