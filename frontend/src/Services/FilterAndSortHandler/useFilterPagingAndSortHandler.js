import { useRef } from "react";
import { useEffect } from "react";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

const sortingAndPaginInitialValues = {
    sortBy: '',
    order: 'ASC',
    page: 0,
    perPage: 10,
};

const useFilterPagingAndSortHandler = (path, searchParams, filterInitialValues) => {
    const [values, setValues] = useState({ ...sortingAndPaginInitialValues, ...filterInitialValues });
    const navigate = useNavigate();
    const filters = useRef(new Map());
    const requestParams = useRef("")

    useEffect(() => {
        const filterInObcjet = Object.entries(Object.fromEntries([...searchParams]));
        let query = "";
        const tmp = {...values};
        if (filterInObcjet.length > 0) {

            for (const [key, value] of filterInObcjet) {
                query += `${key}=${value}&`;
                filters.current.set(key, value);
                tmp[key] = value;
            };
            setValues(tmp);
        }
        
        requestParams.current = query !== "" ? (query.substring(0, query.length - 1)) : "";
    }, [searchParams]);

    const getRequestParams = () => {
        return requestParams.current;
    };

    const filterCHangeHandler = (event) => {
        const { name, value } = event.target;

        setValues({ ...values, [name]: value });
        if (value !== '') {
            filters.current.set(name, value);
        } else {
            filters.current.delete(name);
        }
        filters.current.set('page', 0);
        filterAndSort();
    };

    const sortByChangeHandler = (sortBy) => {
        setValues({ ...values, sortBy: sortBy, order: 'ASC' });
        filters.current.set('sortBy', sortBy);
        filters.current.set('order', 'ASC');
        filterAndSort();
    };

    const pagingChangedHandler = (newPage, newPerPage = null) => {
        if (newPerPage) {
            setValues({ ...values, page: newPage, perPage: newPerPage });
        }
        else setValues({ ...values, page: newPage });
        filters.current.set('page', newPage);
        filters.current.set('perPage', newPerPage ? newPerPage : values.perPage);
        filterAndSort();
    }

    const orderHasChanged = (order) => {
        setValues({ ...values, order: order });
        filters.current.set('order', order);
        filterAndSort();
    };

    const filterAndSort = () => {
        let requestParams = "";
        for (const [key, value] of filters.current.entries()) {
            requestParams += `${key}=${value}&`
        }
        if (requestParams !== "")
            requestParams = requestParams.substring(0, requestParams.length - 1);
        navigate(requestParams === "" ? path : `${path}?${requestParams}`);
    };

    return {
        values,
        getRequestParams,
        filterCHangeHandler,
        sortByChangeHandler,
        pagingChangedHandler,
        orderHasChanged
    }
}

export default useFilterPagingAndSortHandler;