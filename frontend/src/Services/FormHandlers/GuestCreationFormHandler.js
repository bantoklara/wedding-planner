import { useState } from "react";
import { useGuestService } from "../GuestService";

const initialFormValues = {
    firstName: "",
    lastName: "",
    invitedWithGuest: '',
    guestGroupIndex: '',
}

export const useFormHandlers = (guestGroups, setCount) => {
    const [inputFields, setInputFields] = useState(Array(3).fill(initialFormValues))
    const [errors, setErrors] = useState(Array(3).fill({}))
    const { submitGuestCreationForm } = useGuestService();

    const validate = (fieldValues, index) => {
        const err = [...errors];
        const temp = { ...err[index] };
        if ("firstName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.firstName = regex.test(fieldValues.firstName) ? "" : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.firstName.length > 30) {
                temp.firstName += "\n A mező maximum 30 karaktert tartalmazhat."
            }

        }
        if ("lastName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.lastName = regex.test(fieldValues.lastName) ? "" : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.lastName.length > 30) {
                temp.lastName += "\n A mező maximum 30 karakter hosszú kell legyen."
            }
        }
        err[index] = { ...temp };
        setErrors([...err]);
    };

    const addNewField = (size) => {
        const newFields = Array(parseInt(size)).fill(initialFormValues);
        setInputFields([...inputFields, ...newFields]);
        setErrors([...errors, ...Array(parseInt(size)).fill({})]);
    }

    const removeField = (index) => {
        const tmpInputFields = [...inputFields];
        const tmpErrors = [...errors];

        tmpInputFields.splice(index, 1);
        tmpErrors.splice(index, 1);

        setInputFields(tmpInputFields);
        setErrors(tmpErrors);
    }

    const inputChangedHandler = (event, index) => {
        const { name, value } = event.target;
        let data = [...inputFields];
        data[index] = { ...data[index], [name]: value };
        setInputFields(data);
        validate({ [name]: value }, index);
    };

    const isFormValid = () => {
        return errors.every((errorInFields) => (
            Object.values(errorInFields).every((x) => x === "")))
    };

    const submitFormHandler = async (event) => {
        event.preventDefault();
        const requests = [...inputFields]
        requests.forEach((request) => {
            if (request.guestGroupIndex !== '')
                request.guestGroup = { id: guestGroups[request.guestGroupIndex].id };
            if (request.invitedWithGuest === '')
                delete request.invitedWithGuest;
            delete request.guestGroupIndex;
        });

        if (isFormValid()) {
            const newGuests = await submitGuestCreationForm(requests);
            setCount(prev => prev + 1);
            setInputFields(Array(inputFields.length).fill(initialFormValues));
            setErrors(Array(inputFields.length).fill({}));
        }
    };

    return {
        errors,
        inputFields,
        addNewField,
        removeField,
        inputChangedHandler,
        submitFormHandler
    }
}