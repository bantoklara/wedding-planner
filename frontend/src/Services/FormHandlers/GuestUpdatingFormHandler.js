import { useState } from "react"
import { useNavigate } from "react-router-dom";
import { useGuestService } from "../GuestService";

export const useFormHandlers = (user, guest, setGuest, menus = [], withRedirect) => {
    const { updateGuestInformation } = useGuestService();
    function createGuestMenusMap() {
        const guestMenuMap = new Map();
        guest.guestMenus.forEach((menu) => guestMenuMap.set(menu.id.menuId, menu.amount))
        return guestMenuMap;
    };

    const initialFormValues = {
        lastName: guest.lastName != null ? guest.lastName : '',
        firstName: guest.firstName != null ? guest.firstName : '',
        invitedWithGuest: guest.invitedWithGuest != null ? guest.invitedWithGuest : '',
        guestGroupId: guest.guestGroup?.id != null ? guest.guestGroup.id : '',
        feedback: guest.feedback != null ? guest.feedback : 3,
        numberOfGuests: guest.numberOfGuests != null ? guest.numberOfGuests : 0,
        additionalMessage: guest.additionalMessage != null ? guest.additionalMessage : '',
        guestMenusMap: createGuestMenusMap()
    }

    const [values, setValues] = useState(initialFormValues);
    const [errors, setErrors] = useState({})
    const [sent, setSent] = useState(false);
    const navigate = useNavigate();

    const validate = (fieldValues, index) => {
        const temp = { ...errors };
        if ("firstName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.firstName = regex.test(fieldValues.firstName) ? "" : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.firstName.length > 30) {
                temp.firstName += "\n A mező maximum 30 karakter hosszú kell legyen."
            }

        }
        if ("lastName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.lastName = regex.test(fieldValues.lastName) ? "" : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.lastName.length > 30) {
                temp.lastName += "\n A mező maximum 30 karakter hosszú kell legyen."
            }
        }
        setErrors({ ...temp });
    };

    const isEqual = (menus, numberOfGuests) => {
        let sum = 0;
        menus.forEach((value, key) => sum += parseInt(value));
        return sum === numberOfGuests;
    };

    const numberOfMenusAreCorrect = () => {
        const temp = { ...errors };
        temp.menus = parseInt(values.feedback) === 1 && !isEqual(values.guestMenusMap, values.numberOfGuests)
            ? 'A személyek száma és a megadott menük száma meg kell egyezzen.'
            : '';
        setErrors({ ...temp });
    };

    const inputChangedHandler = (event) => {
        const { name, value, type } = event.target;
        if (type === 'number' || type === 'radio')
            setValues({ ...values, [name]: parseInt(value) });
        else
            setValues({ ...values, [name]: value });
        validate({ [name]: value });
    };

    const menuCheckChangeHandler = (event, menuId) => {
        const { checked } = event.target;
        const updatedGuestMenus = values.guestMenusMap;

        if (checked === false)
            updatedGuestMenus.delete(menuId);
        else
            updatedGuestMenus.set(menuId, 1);
        setValues({ ...values, guestMenusMap: updatedGuestMenus });
    };

    const menuAmountChangeHandler = (event, menuId) => {
        const { value } = event.target;

        const updatedGuestMenus = values.guestMenusMap;
        updatedGuestMenus.set(menuId, parseInt(value));
        setValues({ ...values, guestMenusMap: updatedGuestMenus });
    };

    const isFormValid = () => {
        const temp = { ...errors };
        temp.menus = parseInt(values.feedback) === 1 && !isEqual(values.guestMenusMap, values.numberOfGuests)
            ? 'A személyek száma és a megadott menük száma meg kell egyezzen.'
            : '';
        setErrors({ ...temp });
        return Object.values(temp).every((x) => x === "");
    };

    const submitFormHandler = async (event) => {
        event.preventDefault();
        if (isFormValid()) {
            // building the request body
            setSent(true);
            let guestMenus = [];
            for (const [key, value] of (values.guestMenusMap).entries()) {
                if (value > 0) {
                    guestMenus.push(
                        {
                            menuId: key,
                            amount: value
                        });
                }
            };

            const requestBody = {}
            if (guestMenus !== guest.guestMenus)
                requestBody.guestMenus = guestMenus;

            Object.entries(values).map(([key, value]) => {
                if (value && value != guest[key])
                    requestBody[key] = value;
            })
            delete requestBody.guestMenuMap;

            const updatedGuest = await updateGuestInformation(user.role, guest.id, requestBody);
            setGuest(updatedGuest);
            if (withRedirect) {
                navigate(-1);
            }
            setSent(false);
        }
    };

    return {
        values,
        errors,
        sent,
        inputChangedHandler,
        menuCheckChangeHandler,
        menuAmountChangeHandler,
        submitFormHandler
    }
}