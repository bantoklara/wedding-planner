import { useRef, useState } from "react";
import { useWeddingServiceService } from "../WeddingServiceService";
import { useNavigate } from "react-router-dom";
import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

export const useFormHandlers = (categories, countyList, serviceDetails) => {
    // fetch functions
    const { createService, updateService } = useWeddingServiceService();
    const navigate = useNavigate();

    const createSet = (counties = '') => {
        const set = new Set();
        counties.split('|').forEach((county) => {
            set.add(county);
        })
        return set;
    };

    const initialFormValues = {
        name: serviceDetails?.name ? serviceDetails.name : '',
        description: serviceDetails?.description ? serviceDetails.description : '',
        priceCategory: serviceDetails?.priceCategory ? serviceDetails.priceCategory : 'CHEAP',
        categoryId: serviceDetails?.category?.id ? serviceDetails.category.id : '',
        category: serviceDetails?.category ? serviceDetails.category : {},
        imagesFromDb: serviceDetails?.images ? serviceDetails?.images : [],
        descriptions: [],
        coverImage: null,
        images: [],
        counties: serviceDetails?.counties ? createSet(serviceDetails.counties) : new Set(),
        maximalSpaceCapacity: serviceDetails?.maximalSpaceCapacity ? serviceDetails.maximalSpaceCapacity : '',
        totalSize: 0,
    };

    const [values, setValues] = useState(initialFormValues);
    const [errors, setErrors] = useState({});
    const [images, setImages] = useState([]);
    const [coverImage, setCoverImage] = useState(serviceDetails?.cover ? serviceDetails.cover.url : null);
    const [loading, setLoading] = useState(false);
    const coverImageSize = useRef(0);
    const imageSize = useRef(0);

    const validate = (fieldValues) => {
        const temp = { ...errors };
        if ("name" in fieldValues) {
            const regex = /^[\p{L} &-]*$/gu;
            temp.name = regex.test(fieldValues.name) ? "" : "A mező csak betüket, szóközt és -,& karaktereket tartalmazhat.";

            if (fieldValues.name.length > 30) {
                temp.name += "A mező maximum 30 karakter hosszú lehet.";
            }
        }
        if ("description" in fieldValues) {
            temp.description = fieldValues.description.length > 10000 ? "A mező maximum 10000 karakter hosszú lehet." : "";
        }

        if ('coverImage' in fieldValues) {
            temp.coverImage = fieldValues.coverImage.type.startsWith('image/') ? '' : 'Csak kép feltöltése megengedett.'
        };

        if ('images' in fieldValues) {
            temp.images = {};
            fieldValues.images.forEach((image, index) => {
                temp.images[index] = image.type.startsWith('image/') ? '' : 'Csak kép feltöltése megengedett.';
            })
        };

        if ('totalSize' in fieldValues) {
            temp.totalSize = (fieldValues.totalSize / 1000 > 2000) ? 'A feltöltött kép összmérete maximum 2 MB lehet.' : '';
        }
        setErrors({ ...temp });
    };

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;

        setValues({ ...values, [name]: value });
        validate({ [name]: value });
    };

    const categoryChangeHandler = (category) => {
        setValues({ ...values, categoryId: category.id, category: category });
    };

    const coverImageUploadHandler = (event) => {
        const { files } = event.target;
        coverImageSize.current = files[0].size;
        setValues({ ...values, coverImage: files[0], totalSize: coverImageSize.current + imageSize.current });
        setCoverImage(URL.createObjectURL(files[0]));
        validate({ totalSize: coverImageSize.current + imageSize.current, coverImage: files[0] });

    };

    const imageUploadHandler = (event) => {
        const tmpV = [...values.images];
        const tmpI = [...images]

        for (let i = 0; i < event.target.files.length; i++) {
            imageSize.current += event.target.files[i].size;
            tmpV.push(event.target.files[i])
            tmpI.push(URL.createObjectURL(event.target.files[i]))
        };
        setImages(tmpI);
        setValues({
            ...values,
            images: tmpV,
            descriptions: [...values.descriptions, ...Array(event.target.files.length).fill('')],
            totalSize: coverImageSize.current + imageSize.current
        });
        validate({ totalSize: coverImageSize.current + imageSize.current, images: tmpV });
    };

    const imageDescriptionChangeHandler = (event, index) => {
        const tmp = [...values.descriptions];
        tmp[index] = event.target.value;
        setValues({ ...values, descriptions: tmp });
    };

    const imageFromDbDescriptionChangedHandler = (event, index) => {
        const tmp = [...values.images];
        tmp[index] = { ...tmp[index], description: event.target.value };
        setValues({ ...values, images: tmp });
    }

    const countyCheckHandler = (event, name) => {
        const { checked } = event.target;
        const updatedCountyList = values.counties;
        if (checked) {
            if (name === 'All') {
                countyList.forEach((county) => !updatedCountyList.has(county) && updatedCountyList.add(county));
            } else {
                updatedCountyList.add(name);
            }
        }
        else {
            if (name === 'All') {
                countyList.forEach((county) => updatedCountyList.has(county) && updatedCountyList.delete(county));
            } else {

                updatedCountyList.delete(name);
            }
        }
        setValues({ ...values, counties: updatedCountyList });
    };

    const deleteImage = (index) => {
        const tmpV = [...values.images];
        const tmpD = [...values.descriptions];
        const tmpI = [...images]
        imageSize.current -= tmpV[index].size;
        tmpV.splice(index, 1);
        tmpD.splice(index, 1)
        tmpI.splice(index, 1);

        setValues({ ...values, images: tmpV, description: tmpD, totalSize: coverImageSize.current + imageSize.current });
        setImages(tmpI);
        validate({ totalSize: coverImageSize.current + imageSize.current });
    };

    const deleteImageFromDb = (index) => {
        const tmp = [...values.imagesFromDb];
        tmp.splice(index, 1);
        setValues({ ...values, imagesFromDb: tmp });
    }

    const buildRequest = (editMode) => {
        const buildCountyList = (counties) => {
            let countyList = "";
            counties.values().forEach((county) => countyList += county + "|");
            return countyList.substring(0, countyList.length - 1);
        };

        let formData = new FormData();
        let weddingServiceDto =
            `{
            "name": "${values.name}", 
            "description": "${values.description}", 
            "priceCategory": "${values.priceCategory}",
            "counties": "${buildCountyList(values.counties)}",
            "maximalSpaceCapacity": ${values.maximalSpaceCapacity === '' ? null : values.maximalSpaceCapacity},
            "category": { "id":${values.category.id}, "name": "${values.category.name}" }`;
        if (editMode)
            weddingServiceDto += `,\n"images": [${values.imagesFromDb.map((image) =>
                `{ "id": ${image.id}, "url": "${image.url}", "description": "${image.description}"}`
            )}]\n`;
        weddingServiceDto += '}\n';

        formData.append('service', weddingServiceDto);
        if (values.coverImage)
            formData.append('coverImage', values.coverImage);
        if (values.images.length > 0) {
            formData.append('descriptions', `[${values.descriptions.map((description) => "\"" + description + "\"")}]`);
            values.images.forEach((image) => {
                formData.append("images", image);
            })
        };

        return formData;
    };

    const isValid = () => {
        Object.entries(errors).forEach(([key, value]) => {
            if (key === 'images') {
                for (const imageErrors of Object.values(value)) {
                    if (imageErrors !== '')
                        return false;
                }
            } else {
                if (value !== '')
                    return false;
            }
        });

        return true;
    };

    const submitFormHandler = async (event) => {
        event.preventDefault();
        if (isValid()) {
            setLoading(true);
            const id = serviceDetails ? await updateService(serviceDetails.id, buildRequest(true)) : await createService(buildRequest(false));
            if (id !== null) {
                navigate(`/services/${id}`);
            }

            setLoading(false);
        }
    };

    return {
        values,
        errors,
        images,
        coverImage,
        loading,
        inputChangeHandler,
        categoryChangeHandler,
        countyCheckHandler,
        coverImageUploadHandler,
        imageUploadHandler,
        imageDescriptionChangeHandler,
        imageFromDbDescriptionChangedHandler,
        submitFormHandler,
        deleteImage,
        deleteImageFromDb,
    };
}