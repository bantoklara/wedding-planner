import { useState } from "react";
import { useTodoService } from "../TodoService";

export const useFormHandlers = (update, groupedTodos, setGroupedTodos, categories) => {
    const initialFormValues = () => {
        if (update) {
            const { rindex, cindex } = update.info;
            const { todo, editMode } = groupedTodos[rindex].todos[cindex];
            return {
                description: todo.description,
                deadline: todo.deadline,
                category: {
                    id: todo.category.id,
                    name: todo.category.name
                }
            }
        }

        return {
            description: "",
            deadline: 1,
            category: {
                id: categories[0].id,
                name: categories[0].name
            }
        }
    }

    const [values, setValues] = useState(initialFormValues);
    const [errors, setErrors] = useState({});
    const [loading, setLoading] = useState(false);
    const { createTodo, updateTodo } = useTodoService();

    const validate = (fieldValues) => {
        const temp = { ...errors };
        if ("description" in fieldValues) {
            temp.description = fieldValues.description.length > 255 ? 'A mező hossza maximum 255 karakter lehet!' : '';
        }
        setErrors({ ...temp });
    };

    const isValid = () => {
        return Object.values(errors).every(x => x === '');
    }

    const inputCHangeHandler = (event) => {
        const { name, value } = event.target;
        if (name === "categoryId") {
            setValues({ ...values, category: { id: value, name: "" } })
        } else {
            setValues({ ...values, [name]: value });
        }
        validate({ [name]: value });
    }

    const submitFormHandler = async (e) => {
        e.preventDefault();
        if (isValid()) {
            setLoading(true);
            if (update) {
                const { rindex, cindex, id } = update.info;
                values.done = groupedTodos[rindex].todos[cindex].todo.done;
                const updatedTodo = await updateTodo(id, values);
                const tmp = [...groupedTodos];
                if (tmp[rindex].deadline != updatedTodo.deadline) {
                    delete tmp[rindex].todos[cindex];
                    const index = tmp.findIndex((group) => group.deadline == updatedTodo.deadline);
                    tmp[index].todos.push({ editMode: false, todo: updatedTodo });
                } else {
                    tmp[update.info.rindex].todos[update.info.cindex] = { editMode: false, todo: updatedTodo };
                }
                setGroupedTodos(tmp);
            } else {
                setLoading(true);
                const createdTodo = await createTodo(values);
                const tmp = [...groupedTodos];
                const index = tmp.findIndex((group) => group.deadline == createdTodo.deadline);
                tmp[index].todos.push({ editMode: false, todo: createdTodo });
                tmp[index] = { ...tmp[index], expanded: true };
                setGroupedTodos(tmp);
                setLoading(false);
                setValues(initialFormValues);
            }
            setLoading(false);
        }
    };

    return {
        values,
        errors,
        loading,
        inputCHangeHandler,
        submitFormHandler
    };

}