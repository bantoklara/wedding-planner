import { useState } from "react";
import { useNavigate } from "react-router-dom";
import AuthenticationHandler from "../AuthenticationService";

const initialFormValues = {
    lastName: '',
    firstName: '',
    username: '',
    email: '',
    phoneNumber: '',
    password: '',
    role: 'ROLE_COUPLE',
    wedding: {
        nameOfBride: '',
        nameOfFiance: '',
        dateOfWedding: '',
        dateOfFeedbackDeadlin: ''
    }
};

export const useFormHandlers = () => {
    const [error, setError] = useState({});
    const [errorFromServer, setErrorFromServer] = useState('');
    const [inputField, setInputField] = useState(initialFormValues);
    const [processing, setProcessing] = useState(false);
    const navigate = useNavigate();
    const { register } = AuthenticationHandler();

    const validate = (fieldValues) => {
        const temp = { ...error };

        if ("lastName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.lastName = regex.test(fieldValues.lastName) ? '' : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.lastName.length > 30) {
                temp.lastName += "A mező maximum 30 karaktert tartalmazhat.";
            }
        }

        if ("firstName" in fieldValues) {
            const regex = /^[\p{L} -]*$/gu;
            temp.firstName = regex.test(fieldValues.firstName) ? '' : "A mező csak betüket, szóközt és kötőjelet tartalmazhat.";

            if (fieldValues.firstName.length > 30) {
                temp.firstName += "A mező maximum 30 karaktert tartalmazhat.";
            }
        }

        if ("username" in fieldValues) {
            const regex = /^[\p{L}0-9_]*$/gu;
            temp.username = regex.test(fieldValues.username) ? '' : "A mező csak betüket, számokat és alulvonást tartalmazhat.";

            if (fieldValues.username.length > 15) {
                temp.username += "A mező maximum 15 karaktert tartalmazhat.";
            }
        }

        if ("email" in fieldValues) {
            const regex = /^.+@.+\..+$/gu;

            temp.email = regex.test(fieldValues.email) ? '' : 'A megadott email helytelen.';

            if (fieldValues.email.length > 20) {
                temp.email += 'A mező maximum 20 karakter lehet';
            }
        }

        if ("phoneNumber" in fieldValues) {
            const regex = /^[0-9]{10}$/gu;

            temp.phoneNumber = regex.test(fieldValues.phoneNumber) ? '' : 'A megadott telefonszám helytelen';
        }

        if ("password" in fieldValues) {
            const regex = /^(?=.{8,}$)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).*$/gu;

            temp.password = regex.test(fieldValues.password) ? '' : 'Gyenge jelszó.';
        }

        setError(temp);
    }

    const inputChangeHandler = (event) => {
        const { name, value } = event.target;
        setInputField({ ...inputField, [name]: value });
        validate({ [name]: value });
    }

    const weddingInputChangeHandler = (event) => {
        const { name, value } = event.target;
        setInputField(prev => ({ ...prev, "wedding": { ...prev.wedding, [name]: value } }));
    }

    const isFormValid = () => {
        return Object.values(error).every((x) => x === "");
    }

    const submitFormHandler = async (event) => {
        event.preventDefault();

        if (isFormValid()) {
            setProcessing(true);
            setErrorFromServer('');
            // Building the requestbody
            const requestbody = { ...inputField };
            if (inputField.role === 'ROLE_SERVICE_PROVIDER') {
                delete requestbody.wedding;
            } else {
                Object.entries(requestbody.wedding).forEach(([key, value]) => {
                    if (value === '')
                        delete requestbody.wedding[key];
                })
            }

            const status = await register(requestbody)
            if (status === 201) {
                setInputField({ ...initialFormValues });
                navigate('/');
            }

            if (status === 406) {
                setErrorFromServer('A felhasználónév már létezik. Kérjük válasszon másat.');
            }

            setProcessing(false);
        }
    };

    return {
        processing,
        values: inputField,
        error,
        errorFromServer,
        inputChangeHandler,
        weddingInputChangeHandler,
        submitFormHandler,
    };
};