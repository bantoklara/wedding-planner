import { useFetchWrapper } from "../fetchWrapper";
export function useGuestGroupService() {
    const path = '/api/guestGroups';
    const fetchWrapper = useFetchWrapper();

    async function getAllGuestGroups() {
        const response = await fetchWrapper.get(path);
        if (response.status === 200) {
            const guestGroups = await response.json();
            return guestGroups;
        }

        // error handling
    }

    return {
        getAllGuestGroups
    };
}