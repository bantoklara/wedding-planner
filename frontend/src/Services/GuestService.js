import { useFetchWrapper } from "../fetchWrapper";

export function useGuestService() {
    const path = `/api/guests`;
    const fetchWrapper = useFetchWrapper();

    async function getAllGuests() {
        const response = await fetchWrapper.get(path)

        if (response.status === 200) {
            const guestList = await response.json();
            return guestList;
        }
        // error handling
    }

    async function getGuestsGroupedByTable() {
        const response = await fetchWrapper.get(`${path}/by-table`);

        if (response.status === 200) {
            const groupedGuests = await response.json();

            const resp = new Map();
            Object.entries(groupedGuests).forEach(([key, value]) => {
                const trimmedKey = key.replace('WeddingTableDto(', '').replace(')', '');
                const [id, name, numberOfSeats] = trimmedKey.split(',').map(part => part.trim());
                const obj = {
                    id: id.split('=')[1] === 'null' ? null : parseInt(id.split('=')[1]),
                    name: name.split('=')[1],
                    numberOfSeats: parseInt(numberOfSeats.split('=')[1])
                };
                resp.set(obj, value);
            });
            return resp;
        }
        return null;
    }

    async function filterGuests(requestParams = "") {
        const endpoint = requestParams === "" ? path : `${path}?${requestParams}`;
        const response = await fetchWrapper.get(endpoint);

        if (response.status === 200) {
            const guestList = await response.json();
            return guestList;
        }
    }

    async function getGuestDetail(id) {
        const response = await fetchWrapper.get(`${path}/${id}`);

        if (response.status === 200) {
            const guesDetails = await response.json();
            return guesDetails;
        }

        // error handling
    }

    async function getGuestListInExcel(requestParams) {
        const response = await fetchWrapper.get(`${path}/generate-excel?columns=${requestParams}`);
        const inputstream = await response.body.getReader().read();
        if (response.status === 200) {
            return URL.createObjectURL(new Blob([inputstream.value], { type: 'application/vnd.ms-excel' }));
        };
    }

    async function submitGuestCreationForm(newGuest) {
        const response = await fetchWrapper.post(path, JSON.stringify(newGuest));

        if (response.status === 200) {
            const responseBody = await response.json();
            return responseBody;
        }
        // error handling
    }

    async function generateCodes() {
        const response = await fetchWrapper.post(`${path}/codes`);

        if (response.status === 200) {
            const respBody = await response.json();
            return true;
        }
        return false;
    }

    async function updateGuestTables(body) {
        const response = await fetchWrapper.put(`${path}/wedding-tables`, JSON.stringify(body));
        if (response.status === 200) {
            return true;
        }
        return false;
    }

    async function deleteGuest(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);
        // error handling
    }

    async function updateGuestInformation(role, id, updatedGuest) {

        const response = await fetchWrapper.patch(
            role === 'ROLE_COUPLE' ? `${path}/${id}` : `${path}/my-invitation`,
            JSON.stringify(updatedGuest));

        if (response.status === 200) {
            const responseBody = await response.json()
            return responseBody
        }
        return null;

        //error handling
    }

    async function updateGuestFeedback(id, feedback) {
        const response = await fetchWrapper.patch(`${path}/${id}`, JSON.stringify(feedback));
        if (response.status === 200) {
            const respBody = await response.json();
            return respBody.feedback;
        }
    }

    async function getAuthenticatedGuestInfo() {
        const response = await fetchWrapper.get(`${path}/my-invitation`);
        if (response.status === 200) {
            const guestInfo = await response.json();
            return guestInfo;
        }
    }

    return {
        getAllGuests,
        getGuestsGroupedByTable,
        getGuestDetail,
        getAuthenticatedGuestInfo,
        getGuestListInExcel,
        submitGuestCreationForm,
        generateCodes,
        deleteGuest,
        updateGuestInformation,
        updateGuestFeedback,
        updateGuestTables,
        filterGuests
    }
}