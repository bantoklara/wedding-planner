import { useFetchWrapper } from "../fetchWrapper";

export const useMenuService = () => {
    const fetchWrapper = useFetchWrapper();
    
    const getAllMenus = async () => {
        const response = await fetchWrapper.get('/api/menus');
        if (response.status === 200) {
            const menus = await response.json();
            return menus;
        }
    }

    return {
        getAllMenus
    };
}
