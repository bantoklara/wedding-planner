import { useFetchWrapper } from "../fetchWrapper"

const useNotificationService = () => {
    const fetchWrapper = useFetchWrapper();
    const path = "/api/notifications";

    async function getNumberOfUnreadNotifications() {
        const response = await fetchWrapper.get(`${path}/count`);

        if (response.status === 200) {
            const count = await response.json();
            return count;
        }
    };

    async function getAllNotifications(unread = null) {
        const endpoint = unread ? `${path}?unread=${unread}` : path;

        const response = await fetchWrapper.get(endpoint);

        if (response.status === 200) {
            const notificationList = await response.json();
            return notificationList;
        }
    };

    return {
        getNumberOfUnreadNotifications,
        getAllNotifications
    };
};

export default useNotificationService;