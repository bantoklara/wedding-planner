import { useFetchWrapper } from "../fetchWrapper"

const useScheduleService = () => {
    const fetchWrapper = useFetchWrapper();
    const path = 'api/schedules';

    async function getAllSchedulesOfUser() {
        const response = await fetchWrapper.get(path);

        if (response.status === 200) {
            return await response.json();
        }
    };

    async function createSchedule(body) {
        const response = await fetchWrapper.post(path, JSON.stringify(body));

        if (response.status === 200) {
            return await response.json();
        }

        if (response.status === 400) {
            return null;
        };

    };

    async function updateSchedule(id, body) {
        const response = await fetchWrapper.put(`${path}/${id}`, JSON.stringify(body));

        if (response.status === 200) {
            return { schedule: await response.json() };
        }
        else if (response.status === 400) {
            return { error: 'A szolgáltatás már foglalt ebben az intervallumban!' };
        };
    };

    async function deleteSchedule(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);

        if (response.status === 204) {
            return true;
        } else {
            return false;
        }
    }

    return {
        getAllSchedulesOfUser,
        createSchedule,
        updateSchedule,
        deleteSchedule
    }
};

export default useScheduleService;