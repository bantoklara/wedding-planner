import { useFetchWrapper } from "../fetchWrapper";

function useSharedFileService(bookedServiceId) {
    const path = `/api/booked-services/${bookedServiceId}/shared-files`;
    const fetchWrapper = useFetchWrapper();

    async function getShareFiles(requests) {
        const endpoint = requests === "" ? path : `${path}?${requests}`;

        const response = await fetchWrapper.get(endpoint);

        if (response.status === 200) {
            const sharedFileList = await response.json();
            return sharedFileList;
        }
    };

    async function saveFile(formData) {
        const response = await fetchWrapper.post(path, formData, 'multipart/from-data');

        if (response.status === 200) {
            const createdFile = await response.json();
            return createdFile;
        }
        return null;
    };

    async function deleteFile(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);

        if (response.status === 200) {
            return true;
        }
        return false;
    };

    return {
        getShareFiles,
        saveFile,
        deleteFile
    };
}

export default useSharedFileService;