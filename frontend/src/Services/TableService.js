import { useFetchWrapper } from "../fetchWrapper";

const useTableService = () => {
    const fetchWrapper = useFetchWrapper();
    const path = "/api/wedding-tables";

    async function getAllWeddingTables() {
        const response = await fetchWrapper.get(path);
        if (response.status === 200) {
            const tableList = await response.json();
            return tableList;
        } else return null;
    };

    async function createTable(body) {
        const response = await fetchWrapper.post(path, JSON.stringify(body));
        if (response.status === 200) {
            const table = await response.json();
            return table;
        }
        return null;
    }

    async function deleteTable(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);
        if (response.status === 200) {
            return true;
        }
        return false;
    }

    return {
        getAllWeddingTables,
        createTable,
        deleteTable
    };
}

export default useTableService;