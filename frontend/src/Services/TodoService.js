import { useFetchWrapper } from "../fetchWrapper";

export function useTodoService() {
    const path = `/api/todos`;
    const fetchWrapper = useFetchWrapper();

    async function getAllTodos() {
        const response = await fetchWrapper.get(path);

        if (response.status === 200) {
            const todoList = await response.json();
            return todoList;
        }

        // error handling
    };

    async function updateTodoState(id, done) {
        await fetchWrapper.patch(`${path}/${id}`, JSON.stringify(done));
    }

    async function createTodo(body) {
        const response = await fetchWrapper.post(path, JSON.stringify(body));

        if (response.status === 200) {
            const createdTodo = await response.json();
            return createdTodo;
        }

        // error handling
    }

    async function deleteTodo(id) {
        await fetchWrapper.delete(`${path}/${id}`);
    }

    async function updateTodo(id, body) {
        const response = await fetchWrapper.put(`${path}/${id}`, JSON.stringify(body));

        if (response.status === 200) {
            const updatedTodo = await response.json();
            return updatedTodo;
        }
    }

    return {
        getAllTodos,
        createTodo,
        updateTodo,
        updateTodoState,
        deleteTodo
    };
}