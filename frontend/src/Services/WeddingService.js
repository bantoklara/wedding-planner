import { useFetchWrapper } from "../fetchWrapper";

export function useWeddingService() {
    const fetchWrapper = useFetchWrapper();

    async function getWeddingDetailsOfUser(weddingId) {
        const response = await fetchWrapper.get(`/api/wedding/${weddingId}`);

        if (response.status === 200) {
            const weddingDetails = await response.json();
            return weddingDetails;
        }

        if (response.status === 204) {
            return null;
        }
    };

    return {
        getWeddingDetailsOfUser
    };
}