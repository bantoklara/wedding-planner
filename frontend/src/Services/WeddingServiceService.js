import { useFetchWrapper } from '../fetchWrapper';

export function useWeddingServiceService() {
    const path = "/api/services";
    const fetchWrapper = useFetchWrapper();

    async function getAllServices() {
        const response = await fetchWrapper.get(`${path}`);

        if (response.status === 200) {
            const serviceList = await response.json();
            return serviceList;
        }
        return null;
    };

    async function getAllServicesOfUser() {
        const response = await fetchWrapper.get(`${path}/my-services`);

        if (response.status === 200) {
            return await response.json();
        }
    };

    async function getServiceDetails(id) {
        const response = await fetchWrapper.get(`${path}/${id}`);

        if (response.status === 200) {
            const serviceDetails = await response.json();
            return serviceDetails;
        }
        return null;
    };

    async function createService(formData) {
        const response = await fetchWrapper.post(path, formData, 'multipart/form-data');

        if (response.status === 200) {
            const createdService = await response.json();
            return createdService.id;
        }
        return null;
    }

    async function updateService(id, formData = new FormData()) {
        const response = await fetchWrapper.put(`${path}/${id}`, formData, 'multipart/from-data');

        if (response.status === 200) {
            const updatedService = await response.json();
            return updatedService.id;
        }
        return null;
    };

    async function enableService(id) {
        const response = await fetchWrapper.patch(`${path}/${id}`);

        if (response === 200) {
            return true;
        }
        return false;
    }

    async function filter(requestParams = "", paging) {
        let endpoint = requestParams === ""
            ? paging ? `${path}?page=${paging}` : path
            : paging ? `${path}?${requestParams}&page=${paging}` : `${path}?${requestParams}`;

        const response = await fetchWrapper.get(endpoint);
        if (response.status === 200) {
            const serviceList = await response.json();
            return serviceList;
        }
    }

    async function deleteService(id) {
        const response = await fetchWrapper.delete(`${path}/${id}`);

        if (response.status === 204) {
            return true;
        }
        return false;
    };

    return {
        getAllServices,
        getAllServicesOfUser,
        getServiceDetails,
        filter,
        createService,
        updateService,
        enableService,
        deleteService
    }
}
