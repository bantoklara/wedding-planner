import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    input: {
        width: '25px',
        height: '45px',
        textAlign: 'center',

        '&:focus': {
            border: `1px solid ${theme.palette.primary.main}`
        }
    }
}))

export default useStyles;