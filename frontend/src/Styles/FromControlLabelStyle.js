import { FormControlLabel } from "@mui/material";
import { styled } from "@mui/material/styles"
const StyledFormControlLabel = styled(FormControlLabel)(({ theme }) => ({
    boxShadow: '0 3px 5px rgb(0 0 0 / 0.2)',
    padding: '5px',
    borderRadius: '10px',
    width: 'fit-content'
}));

export default StyledFormControlLabel;