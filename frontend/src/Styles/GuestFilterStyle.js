import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
    formControlLabel: {
        padding: '3px',
        borderRadius: '10px',
    },
    active: {
        border: '3px solid #faded4'
    },
    inactive: {
        border: 'none'
    }
}));

export default useStyles;