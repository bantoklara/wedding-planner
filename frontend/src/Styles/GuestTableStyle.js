import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    form: {
        marginBottom: 10
    },

    table: {
        marginBottom: 100,
        minHeight: "80vh",
        width: '80vw',
        border: '1px solid gray'
    },
    tableHeader: {
        fontWeight: "bolder",
    },
    tableCell: {
        textAlign: "center",
        alignContent: "center",
        alignItems: "center"
    },
    navLink: {
        border: "2px solid",
        borderColor: theme.palette.secondary.main,
        padding: "10px",
        borderRadius: "5px",
        bottom: 0,
        position: "absolute"
    },
    activeFeedback: {
        opacity: '1'
    },
    inactiveFeedback: {
        opacity: '0.2',
        '&:hover': {
            opacity: '1',
            cursor: 'pointer'
        }
    },
    guestCounter: {
        textAlign: 'center',
        width: "50px",
        padding: '5px',
        marginBottom: '5px',
        backgroundColor: theme.palette.primary.main,
        float: 'right'
    },
    row: {
        height: 50
    },
}));

export default useStyles;