import { makeStyles } from "@mui/styles";

const useStyles = makeStyles((theme) => ({
    addTableButton: {
        backgroundColor: theme.palette.secondary.main,
        padding: '10px',
        '&:hover': {
            backgroundColor: theme.palette.secondary.main
        }
    }
}))

export default useStyles;