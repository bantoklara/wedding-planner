import { makeStyles } from '@mui/styles';

const LoginStyle = makeStyles((theme) => ({
    errorContainerVisible: {
        display: 'block',
        border: "2px solid red",
        backgroundColor: "rgba(0,0,0,0.2)",
        padding: "15px"
    },
    errorContainerHidden: {
        display: 'none'
    },
    submitButton: {
        color: theme.palette.primary
    }
}));

export default LoginStyle;