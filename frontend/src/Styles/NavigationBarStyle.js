import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    navLink: {
        height: '75%',
        paddingTop: '12px'
    },
    navLinkWithMargin: {
        marginLeft: '10px',
        height: '75%',
        paddingTop: '12px'
    }
}));

export default useStyles
