import styled from "@emotion/styled";

import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

const StyledMoneyIcon = styled(AttachMoneyIcon)(() => ({
    fontSize: 'large',
    color: '#FFB833'
}));

export default StyledMoneyIcon;