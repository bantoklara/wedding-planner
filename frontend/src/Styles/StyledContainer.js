import { Container } from "@mui/material";
import { styled } from "@mui/material/styles"
const ContainerWithShadow = styled(Container)(({ theme }) => ({
    boxShadow: ' 0 3px 5px rgb(0 0 0 / 0.2)',
    padding: '10px',
    margin: '3px',
    borderRadius: '10px',
    backgroundColor: 'rgba(255, 255, 255, 0.95)',
}));

export default ContainerWithShadow;