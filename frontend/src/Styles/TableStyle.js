import { makeStyles } from '@mui/styles';

const useStyles = makeStyles((theme) => ({
    container: {
        marginLeft: 'auto',
        marginRight: 'auto',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
    table: {
        margin: '30px',
        position: 'relative',
        borderRadius: '50%',
        border: '1px solid black',
    },
    tableName: {
        position: 'absolute',
        textAlign: 'center',
        width: '50px',
        height: '50px',
        margin: '-25px 0 0 -25px',
        top: '50%',
        left: '50%'
    },
    seats: {
        zIndex: 1,
        borderRadius: '50%',
        backgroundColor: theme.palette.primary.main,
        position: 'absolute',
        border: '1px solid black',
    },
    guestNameAbbr: {
        fontSize: '12px',
        fontWeight: 'bold',
        margin: '3px'
    },
    deleteTableButton: {
        float: 'right',
        top: '-20px'
    },
    nameContainer: {
        position: 'relative'
    }
}));

export default useStyles;