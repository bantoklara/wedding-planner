import { makeStyles } from "@mui/styles";

const TodoStyle = makeStyles((theme) => ({
    groupButton: {
        width: "100%",
        justifyContent: "flex-start",
        boxShadow: '1px',
        '&:hover': {
            color: '#ffffff',
            backgroundColor: theme.palette.secondary.main
        }
    }
}));

export default TodoStyle;