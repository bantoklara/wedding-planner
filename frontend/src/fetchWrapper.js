import { useCookies } from 'react-cookie';

const useFetchWrapper = () => {
    const [cookies] = useCookies(['token'])
    const authHeader = () => {
        return cookies.token ? { Authorization: `Bearer ${cookies.token}` } : {}
    }

    const request = (method) => {
        return (url, body, contentType) => {
            const requestOptions = {
                method,
                headers: authHeader()
            };

            if (body) {
                if (!contentType) {
                    requestOptions.headers.Accept = "application/json";
                    requestOptions.headers["Content-Type"] = "application/json";
                }
                requestOptions.body = body;
            }
            return fetch(url, requestOptions);
        };
    }

    return {
        get: request("GET"),
        post: request("POST"),
        put: request("PUT"),
        delete: request("DELETE"),
        patch: request('PATCH')
    };
}
export { useFetchWrapper };