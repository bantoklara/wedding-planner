export const navigationMenus = [
    { to: "/services", label: 'Szolgáltatások', private: false, role: [] },
    { to: "/guests", label: 'Vendéglista', role: ['ROLE_COUPLE'], private: true },
    { to: "todos", label: 'Teendők', role: ['ROLE_COUPLE'], private: true },
    { to: "seating-plan", label: 'Ültetési rend', role: ['ROLE_COUPLE'], private: true },
    { to: "/booked-services", label: 'Foglalások', role: ['ROLE_COUPLE', 'ROLE_SERVICE_PROVIDER'], private: true },
    { to: "/new-service", label: 'Új szolgáltatás', role: ['ROLE_SERVICE_PROVIDER'], private: true },
    { to: "/calendar", label: 'Naptár', role: ['ROLE_SERVICE_PROVIDER'], private: true },
]

