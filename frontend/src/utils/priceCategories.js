import { Tooltip } from "@mui/material";
import styled from "@emotion/styled";

import AttachMoneyIcon from '@mui/icons-material/AttachMoney';

const StyledMoneyIcon = styled(AttachMoneyIcon)(() => ({
    fontSize: 'medium',
    color: '#FFB833'
}));

const priceCategories = [
    {
        value: 'CHEAP',
        label: <Tooltip title='Olcsó árkategória'>
            <div style={{ width: 'fit-content' }}>
                <StyledMoneyIcon />
                <StyledMoneyIcon className="inactive-money" />
                <StyledMoneyIcon className="inactive-money" />
            </div>
        </Tooltip>
    },
    {
        value: 'MIDDLE',
        label: <Tooltip title='Közép árkategória'>
            <div style={{ width: 'fit-content' }}>
                <StyledMoneyIcon />
                <StyledMoneyIcon />
                <StyledMoneyIcon className="inactive-money" />
            </div>
        </Tooltip>
    },
    {
        value: 'EXPENSIVE',
        label: <Tooltip title='Drága árkategória'>
            <div style={{ width: 'fit-content' }}>
                <StyledMoneyIcon />
                <StyledMoneyIcon />
                <StyledMoneyIcon />
            </div></Tooltip>
    },
]

export default priceCategories;