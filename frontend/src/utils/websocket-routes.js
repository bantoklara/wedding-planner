export const getWebsocketRoutes = (role, username) => {
    switch (role) {
        case 'ROLE_COUPLE':
            return [
                `/user/${username}/notifications/couple/feedback`,
                `/user/${username}/notifications/couple/service`
            ];
        case 'ROLE_SERVICE_PROVIDER':
            return [
                `/user/${username}/notifications/provider/request`,
                `/user/${username}/notifications/provider/service-feedback`
            ];
        default:
            return [`/user/${username}/notifications/admin/request`];
    }
}