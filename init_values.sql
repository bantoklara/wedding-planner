-- This script needs to be run in mysql after the tables have been created.

-- Inserting guestgroups  
INSERT into guest_group (name)
VALUES
('Menyasszony családja'),
('Vőlegény családja'),
('Menyasszony barátja'),
('Vőlegény barátja'),
('Szolgáltató'),
('Násznagy'),
('Koszorús');

-- Inserting menus 
INSERT into menu (name)
values
('Alapmenü'),
('Gyerekmenü'),
('Menü nélkül'),
('Vegetáriánus'),
('Vegán'),
('Gluténmentes'),
('Laktózmentes');


-- Inserting categories
 INSERT into category (id, name)
 values
 (1, 'Autóbérlés'),
 (2, 'Catering'),
 (3, 'Ceremóniamester/Vőfély'),
 (4, 'Cukrász'),
 (5, 'Dekoráció'),
 (6, 'Ékszerek'),
 (7, 'Esküvőszervező'),
 (8, 'Étterem'),
 (9, 'Hivatalos'),
 (10, 'Meghívók'),
 (11, 'Sminkes, Fodrász'),
 (12, 'Zenész/DJ'),
 (13, 'Fotós, Videós'),
 (14, 'Esküvői ruha, kiegészítő'),
 (15, 'Szállás'),
 (16, 'Nászút');
